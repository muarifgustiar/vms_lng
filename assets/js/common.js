moment.locale("id");
Date.parseDate = function( input, format ){
  return moment(input,format).toDate();
};
Date.prototype.dateFormat = function( format ){
  return moment(this).format(format);
};
function editButton(url, id, value){
	if(value==null) value = "Edit";
	html = "<a href='"+url+"' class='btn btn-white tableButton buttonEdit' data-role='modal' data-header='Ubah Data'><i class='icon'></i><span class='icon-text'>"+value+"</span></a>";
	return html;
}

function deleteButton(url, id, value){
	if(value==null) value = "Hapus";
	html = "<a href='"+url+"' class='btn btn-danger tableButton buttonDelete'><i class='icon'></i><span class='icon-text'>"+value+"</span></a>";
	return html;
}

function insertButton(url, id, value){
	if(value==null) value = "Tambah";
	html = "<a href='"+url+"' class='btn btn-primary tableButton buttonAdd' data-role='modal' data-header='Tambah Data'><i class='icon'></i><span class='icon-text'>"+value+"</span></a>";
	return html;
}
function is_empty(data){

	if(data == null || data == '' || typeof data == 'undefined') {
		return true;
	}else{
		return false;
	}
}
function uniqId() {
  	return Math.round(new Date().getTime() + (Math.random() * 100));
}
function generateDropdown(el, data){
	
	var html = '<select name="'+data.name+'" class="form-control dropdownSmall">';
		$.each(data.option, function(key, value){
			var _select = '';
			if(parseInt(data.value) == key) _select = 'selected'; 
			
			html += '<option value="'+key+'" '+_select+'>'+value+'</option>'; 
		})
		html += '</select>'
	$(el).html(html);
}

function round(value, decimals){
	return Number(Math.round(value+'e'+decimals)+'e-'+decimals)
}
var getObjectSize = function(obj) {
    var len = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) len++;
    }
    return len;
};
function randomColor(){
	_color = [	'#C91F37',
				'#DC3023',
				'#9D2933',
				'#CF000F',
				'#E68364',
				'#F22613',
				'#CF3A24',
				'#C3272B',
				'#8F1D21',
				'#D24D57',
				'#F08F07',
				'#F47983',
				'#DB5A6B',
				'#C93756',
				'#FCC9B9',
				'#FFB3A7',
				'#F62459',
				'#F58F84',
				'#875F9A',
				'#5D3F6A',
				'#89729E',
				'#763568',
				'#8D608C',
				'#A87CA0',
				'#5B3256',
				'#BF55EC',
				'#8E44AD',
				'#9B59B6',
				'#BE90D4',
				'#4D8FAC',
				'#5D8CAE',
				'#22A7F0',
				'#19B5FE',
				'#59ABE3',
				'#48929B',
				'#317589',
				'#89C4F4',
				'#4B77BE',
				'#1F4788',
				'#003171',
				'#044F67',
				'#264348',
				'#7A942E',
				'#8DB255',
				'#5B8930',
				'#6B9362',
				'#407A52',
				'#006442',
				'#87D37C',
				'#26A65B',
				'#26C281',
				'#049372',
				'#2ABB9B',
				'#16A085',
				'#36D7B7',
				'#03A678',
				'#4DAF7C',
				'#D9B611',
				'#F3C13A',
				'#F7CA18',
				'#E2B13C',
				'#A17917',
				'#F5D76E',
				'#F4D03F',
				'#FFA400',
				'#E08A1E',
				'#FFB61E',
				'#FAA945',
				'#FFA631',
				'#FFB94E',
				'#E29C45',
				'#F9690E',
				'#CA6924',
				'#F5AB35',
				'#BFBFBF',
				'#BDC3C7',
				'#757D75',
				'#ABB7B7',
				'#6C7A89',
				'#95A5A6'];
	return _color[Math.floor(Math.random()*_color.length)];
}
function getHourMin(value){
	
	var _return = '';
	_hour = Math.floor(value / 60);
	_minute = (value % 60);

	if(_hour > 0){
		_return += _hour+' jam ';
	}
	if(_minute > 0){
		_return += _minute+' menit';
	}
	return _return;
}
function defaultDate(date){
	if(date=='lifetime'){
		return 'Seumur Hidup';
	}
	if(date != '' && date != null && typeof date !== 'undefined'){	
		date = moment(date).format('dddd, DD MMMM YYYY');
	}

	if(date == null || date == ''){
		return '--';
	}
	return date;
}
$(function(){
	$(document).on('click', function(e){
		e.stopPropagation();
		$('.dropdown.open').removeClass('open');
		
	})
	$('.dropdown-toggle').on('click', function(e){
		e.stopPropagation();
		$(this).parent().toggleClass('open');
	})
	$('.dropdown-menu').on('click', function(e){
		e.stopPropagation();
	})
	$('.npwp').iMask({
		type : 'fixed',
		mask : '99.999.999.9-999.999',
	});	
})
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
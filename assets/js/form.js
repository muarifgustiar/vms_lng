;(function($, window, document, undefined){
	
	var pluginName 	= 'form';

	function Plugin(element, options){

		defaults 	= {
			url : null,
			wrapper : null,
			formTags : function(){
				return true;
			},
			onsubmit: function(){

			},
			generateSuccess: function(){

			},
			onError: function(data){

			},
			onSuccess: function(data){

			},
			formWrap: true,
			isReset: true,
			successMessage : '<strong>Sukses!</strong> Berhasil menyimpan data',
			errorMessage : '<strong>Gagal menyimpan data!</strong> Terjadi kesalahan'
		};
		self = this;

		this.element = element;

		this.options = $.extend( {}, defaults, options) ;

		this._defaults = defaults;
		this._name = pluginName;
		this.input = {};
		this.init();
		

		$(self.formTags).on('submit', function(e){
			e.preventDefault();
			self.send($(this));
		});

	}

	Plugin.prototype = {
		init : function(){
			$(self.element).empty();
			self.generateFormTags(self.formTags);
			var key = 0;
			$.each(this.options.form, function(keys, value){

				_count = (typeof self.input.length == 'undefined') ? 0 : self.input.length;
				
				if(value.readonly == true){
					var input = self.generateReadonly(key, value, value.type);
					self.input[_count] = input;
				}else{
					switch(value.type){
						case 'text' :
						case 'password' :
						case 'hidden' :
						case 'npwp' :
						case 'money' :
						case 'money_asing' :
							var input = self.generateText(key, value, value.type);
							self.input[_count] = input;
						break;
						case 'file' : 
							var input = self.generateFile(key, value);
							self.input[_count] = input;
						break;
						case 'textarea' : 
							var input = self.generateTextarea(key, value);
							self.input[_count] = input;
						break;
						case 'search' : 
							var input = self.generateSearch(key, value);
							self.input[_count] = input;
						break;
						case 'dropdown' :
							var input = self.generateDropdown(key, value);
							self.input[_count] = input;
						break;
						case 'radio' :
						case 'radioList' :
							var input = self.generateRadio(key, value);
							self.input[_count] = input;
						break;
						case 'time' :
							var input = self.generateTimePicker(key, value);
							self.input[_count] = input;
						break;
						case 'date' :
							var input = self.generateDate(key, value);
							self.input[_count] = input;
						break;
						case 'lifetimeDate' :
							var input = self.generateLifetimeDate(key, value);
							self.input[_count] = input;
						break;
						case 'date_range' :
							var input = self.generateDateRange(key, value);
							self.input[_count] = input;
						break;
						case 'date_range_lifetime' :
							var input = self.generateDateRangeLifetime(key, value);
							self.input[_count] = input;
						break;
						case 'dateperiod' :
							var input = self.generateDatePeriod(key, value);
							self.input[_count] = input;
						break;
					}
				}
				key++;

			});

			this.generateButton(this.formTags, this.options.button);
			
			$('.npwp').iMask({
				type : 'fixed',
				mask : '99.999.999.9-999.999',
			});

			$('.money').iMask({
				type : 'number'
			});

		},

		/**
		Generate HTML table
		**/
		button : {

			buttonWrapper : function(element){
				$(element).append('<div class="form-group btn-group"></div>');

				return $('.form-group.btn-group', element);
			},

			submit : function(element, key, value){
				$(element).append('<button type="submit" class="btn btn-primary btn-submit '+value.class+'" >'+value.label+'</button>');
			},
			delete : function(element, key, value){
				$(element).append('<button type="submit" class="btn btn-danger btn-submit '+value.class+'" >'+value.label+'</button>');
			},
			reset : function(element, key, value){
				$(element).append('<button type="button" class="btn btn-danger btn-reset '+value.class+'" >'+value.label+'</button>');
				$('.btn-reset', element).on('click', function(e){
					var parent = $(this).closest('.form');
					$('.form-control', parent).val('');
				})
			},
			cancel : function(element, key, value){
				$(element).append('<button class="btn btn-default  btn-cancel '+value.class+'" >'+value.label+'</button>');
				$('.btn-cancel', element).on('click', function(e){
					e.preventDefault();
					var _parent = $(this).parents('.modal');
					$('.close', _parent).trigger('click');
				})
			},
			yes :function(element, key, value){
				$(element).append('<button type="button" class="btn btn-success btn-yes '+value.class+'" >'+value.label+'</button>');
			},
			no : function(element, key, value){
				$(element).append('<button type="button" class="btn btn-danger btn-no '+value.class+'" >'+value.label+'</button>');
			},
			button : function(element, key, value){
				$(element).append('<button type="button" class="btn btn-primary '+value.class+'" >'+value.label+'</button>');
			}

		},
		send : function(el){
			self.removeError(el);
			form = $(el);
			formData = new FormData($(el)[0]);
						
			$.ajax({
				async: false,
				url : $(form).attr('action'),
				method : 'POST',
				data: formData,
				processData: false,
				contentType: false,
				dataType: 'json',	
   				beforeSend: function(xhr){
					$('.btn-submit',form).attr('disabled', 'disabled').addClass('btn-loader');
				},
				success: function(xhr){
					if(xhr.status=='success'){
						self.generateSuccess(xhr);
					}else{
						self.options.onError(xhr);
						self.generateError(xhr.form);
					}
				},
				error: function(xhr){
					
				},
				complete: function(xhr){
					form = $('.btn-submit',form);
					

					setTimeout(function(){
						form.attr('disabled', false);
						form.removeClass('btn-loader');
					}, 1000);
				}
			})
		},
		generateButton: function (wrapper, button){
			var _wrapper = this.button.buttonWrapper(wrapper);

			$.each(button, function(key, value){
				switch(value.type){
					case 'submit' : 
						self.button.submit(_wrapper, key, value);
					break;
					case 'yes' : 
						self.button.yes(_wrapper, key, value);
					break;
					case 'no' : 
						self.button.no(_wrapper, key, value);
					break;
					case 'reset' : 
						self.button.reset(_wrapper, key, value);
					break;
					case 'delete' : 
						self.button.delete(_wrapper, key, value);
					break;
					case 'cancel' :
						self.button.cancel(_wrapper, key, value);
					break;
					default:
						self.button.button(_wrapper, key, value);
				}
			})
		},
		generateWrapper : function(key, element, read_only=false){
			var _read_only = (read_only==false) ? '' : 'read_only';
			
			$(element).append('<fieldset class="form-group '+_read_only+'" for=""></fieldset>');
			return $('fieldset', element)[key];
		},
		generateFile: function(key, data, type){

			_this = this;
			var _closeTags;
			data.id = (typeof data.id == "undefined") ? '' : 'input'+ key;
			data.value = (typeof data.value == "undefined" || data.value==null) ? '' : data.value ;
			var wrapper = this.generateWrapper(key, this.formTags);
			is_required = (/required/.test(data.rules)) ? '*' : '';
			$(wrapper).append('<label for="'+data.id+'">'+data.label+is_required+'</label>');
			$(wrapper).append('<input type="file" class="form-control" id="'+data.id+'"  name="'+data.field+'" class="'+data.class+'"/><input type="hidden" name="'+data.field+'" value="'+data.value+'">');
			if(data.value!=''){
				$(wrapper).append('<div class="fileUploadBlock"><i class="fa fa-upload"></i>&nbsp;<a href="'+data.upload_path+'/'+data.value+'" target="_blank">'+data.value+'</a><div class="deleteFile"><i class="fa fa-trash"></i></div></div>');
				$('.deleteFile', wrapper).on('click',function(e){
					$('.form-control',wrapper).show();
					$('input[type=hidden]',wrapper).val('');
					$('.fileUploadBlock', wrapper).remove();
				});
				$('.form-control',wrapper).hide();
				$('.error-help',wrapper).hide();
			}
			
			
			$('input.form-control ',wrapper).on('change', function(e){
				_this.removeError(wrapper);

				var _files = $(this);
				var myFormData  = new FormData();
				myFormData .append(data.field, $('.form-control', wrapper).prop('files')[0]);
				myFormData .append('allowed_types', data.allowed_types);
				
				$.ajax({
					url: data.upload_url,
					type: 'POST',
					processData: false,
					contentType: false,
					dataType: 'json',
					data: myFormData,
					beforeSend: function(){
						_files.addClass('sending');
					},
					success: function(xhr){
						_files.val('');
						if(xhr.status=='success'){
							$(wrapper).append('<div class="fileUploadBlock"><i class="fa fa-upload"></i>&nbsp;<a href="'+xhr.upload_path+'">'+xhr.file_name+'</a><div class="deleteFile"><i class="fa fa-trash"></i></div></div>');
							$('.deleteFile', wrapper).on('click',function(e){
								$('.form-control',wrapper).show();
								$('input[type=hidden]',wrapper).val('');
								$('.fileUploadBlock', wrapper).remove();
							});
							$('input[type=hidden]',wrapper).val(xhr.file_name);
							$('.form-control',wrapper).hide();
							$('.error-help',wrapper).hide();
							_files.removeClass('sending');

						}else{
							$(wrapper).addClass('form-error');
							$(wrapper).append('<small class="error-help">'+xhr.message+'</small>');
							_files.removeClass('sending');
						}
						
					}
				});
			});
			if(data.caption){
				$(wrapper).append(data.caption);
			}
			return $('.form-control', wrapper);
		},
		generateText : function(key, data, type){
			var _closeTags;

			data.id = (typeof data.id == "undefined") ? '' : 'input'+ key;
			data.value = (typeof data.value == "undefined" || data.value==null) ? '' : data.value ;
			data.class = (typeof data.class == "undefined" || data.class==null) ? '' : data.class ;
			data.placeholder = (typeof data.placeholder == "undefined") ? '' : data.placeholder ;

			data.rules = (typeof data.rules == "undefined") ? '' : data.rules  ;
			_class = (type == 'npwp') ? 'npwp' : '';
			_class = (type == 'money' || type=='money_asing') ? 'money' : '';
			is_required = (/required/.test(data.rules)) ? '*' : '';
			if(type!="hidden"){
				var wrapper = this.generateWrapper(key, this.formTags);
				if(typeof data.label !='undefined'){
					$(wrapper).append('<label for="'+data.id+'">'+data.label+is_required+'</label>');
				}
				if(typeof data.icon !='undefined'){
					$(wrapper).append('<div class="input-group"><div class="input-group-addon" ><i class="'+data.icon+'"></i></div><input type="'+type+'" class="form-control '+data.class+'" id="'+data.id+'" value="'+data.value+'" name="'+data.field+'" placeholder="'+data.placeholder+'"/></div>');
				}else{
					if(type=='npwp') type='text';
					if(type=='money') {
						type='text';
						data.value = $.number(data.value,0, '.',',');
					}
					if(type=='money_asing'){
						var _select = "<select name='"+data.field[0]+"' class='form-control mg-xs-2' >";
						$.each(data.source, function(keys, value){
							_selected = (data.value==keys) ? 'selected' : '';
							_select += "<option value='"+keys+"' "+_selected+">"+value+"</option>";
						});
						_select += "</select>";
						$(wrapper).append(_select);
						_class += ' mg-xs-10';
						data.field = data.field[1];
					}
					$(wrapper).append('<input type="'+type+'" class="form-control '+_class+' '+data.class+'" id="'+data.id+'" value="'+data.value+'" name="'+data.field+'" placeholder="'+data.placeholder+'"/></div>');
				}
				
			}else{
				$(this.formTags).append('<input type="'+type+'" class="form-control '+data.class+' '+npwp+'" id="'+data.id+'" value="'+data.value+'" name="'+data.field+'"/>');
			}
			
			

			if(data.caption){
				$(wrapper).append(data.caption);
			}
			return $('.form-control', wrapper);
		},
		generateDate: function(key, data){
			data.id = (typeof data.id == "undefined") ? '' : 'input'+ key;
			data.value = (typeof data.value == "undefined" || data.value==null) ? '' : data.value ;
			data.placeholder = (typeof data.placeholder == "undefined") ? '' : data.placeholder ;
			data.class = (typeof data.class == "undefined" || data.class==null) ? '' : data.class ;
			data.rules = (typeof data.rules == "undefined") ? '' : data.rules  ;
			is_required = (/required/.test(data.rules)) ? '*' : '';
			var wrapper = this.generateWrapper(key, this.formTags);

			$(wrapper).append('<label for="'+data.id+'">'+data.label+is_required+'</label><div class="wrapper"></div>');
			
				$('.wrapper',wrapper).append('<input type="text" class="form-control datePicker '+data.class+'" id="'+data.id+key+'" value="'+data.value+'" name="'+data.field+'" />');
			
			$('.datePicker', wrapper).datetimepicker({
				timepicker: false,
				format: 'Y-M-D'
			});

			if(data.caption){
				$(wrapper).append(data.caption);
			}
			return $('.form-control', wrapper);
		}, 
		generateLifetimeDate: function(key, data){
			data.id = (typeof data.id == "undefined") ? '' : 'input'+ key;
			data.value = (typeof data.value == "undefined" || data.value==null) ? '' : data.value ;
			data.placeholder = (typeof data.placeholder == "undefined") ? '' : data.placeholder ;
			data.class = (typeof data.class == "undefined" || data.class==null) ? '' : data.class ;
			data.rules = (typeof data.rules == "undefined") ? '' : data.rules  ;
			is_required = (/required/.test(data.rules)) ? '*' : '';
			var wrapper = this.generateWrapper(key, this.formTags);

			$(wrapper).append('<label for="'+data.id+'">'+data.label+is_required+'</label><div class="wrapper"></div>');
			var setValue;
			if(data.value=='lifetime'){
				setValue='';
			}else{
				setValue=data.value;
			}
			$('.wrapper',wrapper).append('<input type="text" class="form-control datePicker '+data.class+'" id="'+data.id+key+'" value="'+setValue+'" name="'+data.field+'" />');
			$('.wrapper',wrapper).append('<div class="lifetimeWrapper"><input type="checkbox" class="form-control lifetime" id="'+data.id+key+'" value="lifetime" name="'+data.field+'" />&nbsp;<span>Seumur Hidup</span></div>');
			if(data.value=='lifetime'){
				$('.lifetime.form-control',wrapper).prop('checked','checked');
			}
			$('.datePicker', wrapper).datetimepicker({
				timepicker: false,
				format: 'Y-M-D'
			});

				if($('.lifetime.form-control',wrapper).is(':checked')){
					$('.form-control.datePicker',wrapper).hide();
				}

				$('.lifetime.form-control',wrapper).on('change',function(){
					$('.form-control.datePicker',wrapper).toggle();
					$('.form-control.datePicker',wrapper).val('');
				})


			if(data.caption){
				$(wrapper).append(data.caption);
			}
			return $('.form-control', wrapper);
		}, 
		generateDateRangeLifetime: function(key, data){
			data.id = (typeof data.id == "undefined") ? '' : 'input'+ key;
			data.value = (typeof data.value == "undefined" || data.value==null) ? '' : data.value ;
			data.placeholder = (typeof data.placeholder == "undefined") ? '' : data.placeholder ;
			data.class = (typeof data.class == "undefined" || data.class==null) ? '' : data.class ;
			data.rules = (typeof data.rules == "undefined") ? '' : data.rules  ;
			is_required = (/required/.test(data.rules)) ? '*' : '';
			var wrapper = this.generateWrapper(key, this.formTags);

			$(wrapper).append('<label for="'+data.id+'">'+data.label+is_required+'</label><div class="rangeWrapper"></div><div class="wrapper"></div>');
			$.each(data.field, function(key, value){
				if(key!=0){
					$('.rangeWrapper',wrapper).append(' - ')
				}
				$('.rangeWrapper',wrapper).append('<input type="text" class="form-control datePicker dateRange '+data.class+'" id="'+data.id+key+'" value="" name="'+value+'" />');
			});
			$('.wrapper',wrapper).append('<div class="lifetimeWrapper"><input type="checkbox" class="form-control lifetime '+data.class+'" id="'+data.id+key+'" value="lifetime" name="'+data.field+'" />&nbsp;<span>Seumur Hidup</span></div>');
			
			if(data.value=='lifetime'){
				$('.lifetime.form-control',wrapper).prop('checked','checked');
			}

			$('.datePicker', wrapper).datetimepicker({
				timepicker: false,
				format: 'Y-M-D'
			});

			if($('.lifetime.form-control',wrapper).is(':checked')){
				$('.rangeWrapper',wrapper).hide();
			}

			$('.lifetime.form-control',wrapper).on('change',function(){
				$('.rangeWrapper',wrapper).toggle();
				$('.form-control.datePicker.dateRange',wrapper).val('');
			})

			if(data.caption){
				$(wrapper).append(data.caption);
			}
			return $('.form-control', wrapper);
		}, 
		generateReadonly: function(key, data, type){
			data.value = (typeof data.value == "undefined" || data.value==null) ? '' : data.value ;
			var wrapper = this.generateWrapper(key, this.formTags, true);
			if(typeof data.label !='undefined'){
				$(wrapper).append('<label for="'+data.id+'">'+data.label+'</label>');
			}
			if(type=='file'){
				$(wrapper).append('<b>:</b><span><a href="'+base_url+'assets/lampiran/'+data.field+'/'+data.value+'">'+data.value+'</a></span>');
			}else if(type=='radio'){
				data.value = (typeof data.source[data.value] == "undefined" || data.source[data.value]==null) ? '' : data.source[data.value] ;
				$(wrapper).append('<b>:</b><span>'+data.value+'</span>');
			}else{
				$(wrapper).append('<b>:</b><span>'+data.value+'</span>');
			}
			
			
			return $('.form-control', wrapper);
		},
		generateTextarea : function(key, data, type){
			
			var wrapper = this.generateWrapper(key, this.formTags);
			data.id = (typeof data.id == "undefined") ? '' : 'input'+ key;
			data.value = (typeof data.value == "undefined" || data.value==null) ? '' : data.value ;
			data.class = (typeof data.class == "undefined" || data.class==null) ? '' : data.class ;
			data.rules = (typeof data.rules == "undefined") ? '' : data.rules  ;
			is_required = (/required/.test(data.rules)) ? '*' : '';
			
			if(typeof data.label !='undefined'){
				$(wrapper).append('<label for="'+data.id+'">'+data.label+is_required+'</label>');
			}

			$(wrapper).append('<textarea class="form-control '+data.class+'" id="'+data.id+'" name="'+data.field+'" >'+data.value+'</textarea>');
			
			if(data.caption){
				$(wrapper).append(data.caption);
			}
			return $('.form-control', wrapper);
		},
		generateDropdown: function(key, data){
			data.id = (typeof data.id == "undefined") ? '' : 'input'+ key;
			data.value = (typeof data.value == "undefined" || data.value==null) ? '' : data.value ;
			data.class = (typeof data.class == "undefined" || data.class==null) ? '' : data.class ;
			var wrapper = this.generateWrapper(key, this.formTags);
			is_required = (/required/.test(data.rules)) ? '*' : '';
			if(typeof data.label !='undefined'){
				$(wrapper).append('<label for="'+data.id+'">'+data.label+is_required+'</label>');
			}

			var _select = "<select name='"+data.field+"' id='"+data.id+"' class='form-control "+data.class+"' >";
			$.each(data.source, function(keys, value){
				_selected = (data.value==keys) ? 'selected' : '';
				_select += "<option value='"+keys+"' "+_selected+">"+value+"</option>";
			})
			_select += "</select>";

			$(wrapper).append(_select);

			if(data.caption){
				$(wrapper).append(data.caption);
			}
			return $('.form-control', wrapper);
		},
		generateRadio: function(key, data){
			data.id = (typeof data.id == "undefined") ? '' : 'input'+ key;
			data.value = (typeof data.value == "undefined" || data.value==null) ? '' : data.value ;
			data.class = (typeof data.class == "undefined" || data.class==null) ? '' : data.class ;	
			var wrapper = this.generateWrapper(key, this.formTags);
			is_required = (/required/.test(data.rules)) ? '*' : '';
			if(typeof data.label !='undefined'){
				$(wrapper).append('<label for="'+data.id+'">'+data.label+is_required+'</label>');
			}
			var _check = '<div class="radioWrapper">';
			$.each(data.source, function(keys, value){
				
				_checked = (data.value===keys) ? 'checked' : '';
				
				if(data.type=='radioList') _check +='<div class="radioList">';
				_check += "<input type='radio' value='"+keys+"' "+_checked+" name='"+data.field+"' class='form-control "+data.class+"'><label>"+value+"</label> ";
				if(data.type=='radioList') _check +='</div>';
			});
			_check+='</div>';
			$(wrapper).append(_check);

			if(data.caption){
				$(wrapper).append(data.caption);
			}
			return $('.form-control', wrapper);
		},
		generateTimePicker: function(key, data){
			data.id = (typeof data.id == "undefined") ? '' : 'input'+ key;
			data.value = (typeof data.value == "undefined" || data.value==null) ? '' : data.value ;
			data.placeholder = (typeof data.placeholder == "undefined") ? '' : data.placeholder ;
			data.class = (typeof data.class == "undefined" || data.class==null) ? '' : data.class ;
			data.rules = (typeof data.rules == "undefined") ? '' : data.rules  ;
			var wrapper = this.generateWrapper(key, this.formTags);
			is_required = (/required/.test(data.rules)) ? '*' : '';

			
			if(typeof data.label !='undefined'){
				$(wrapper).append('<label for="'+data.id+'">'+data.label+is_required+'</label>');
			}
			$(wrapper).append('<div class="input-group timepicker"><input type="text" class="form-control '+data.class+'" id="'+data.id+'" value="'+data.value+'" name="'+data.field+'" placeholder="'+data.placeholder+'"/></div>');

			$('.timepicker', wrapper).clockpicker({autoclose: true});

			if(data.caption){
				$(wrapper).append(data.caption);
			}
			return $('.form-control', wrapper);
		},
		generateSearch: function(key, data){
			data.id = (typeof data.id == "undefined") ? '' : 'input'+ key;
			data.value = (typeof data.value == "undefined" || data.value==null) ? '' : data.value ;
			data.placeholder = (typeof data.placeholder == "undefined") ? '' : data.placeholder ;
			data.class = (typeof data.class == "undefined" || data.class==null) ? '' : data.class ;
			data.rules = (typeof data.rules == "undefined") ? '' : data.rules  ;
			is_required = (/required/.test(data.rules)) ? '*' : '';
			var wrapper = this.generateWrapper(key, this.formTags);
			if(typeof data.label !='undefined'){
				$(wrapper).append('<label for="'+data.id+'">'+data.label+is_required+'</label>');
			}
			$(wrapper).append('<input type="text" class="form-control searchInput '+data.class+'" autocomplete="off" id="'+data.id+'" value="'+data.value+'" name="'+data.field+'" placeholder="'+data.placeholder+'"/><div class="searchOption"></div>');

			if(data.caption){
				$(wrapper).append(data.caption);
			}
			return $('.form-control', wrapper);
		},
		
		generateDateRange: function(key, data){
			data.id = (typeof data.id == "undefined") ? '' : 'input'+ key;
			data.value = (typeof data.value == "undefined" || data.value==null) ? '' : data.value ;
			data.placeholder = (typeof data.placeholder == "undefined") ? '' : data.placeholder ;
			data.class = (typeof data.class == "undefined" || data.class==null) ? '' : data.class ;
			data.rules = (typeof data.rules == "undefined") ? '' : data.rules  ;
			is_required = (/required/.test(data.rules)) ? '*' : '';
			var wrapper = this.generateWrapper(key, this.formTags);

			$(wrapper).append('<label for="'+data.id+'">'+data.label+is_required+'</label><div class="rangeWrapper"></div>');
			$.each(data.field, function(key, value){
				if(key!=0){
					$('.rangeWrapper',wrapper).append(' - ')
				}
				$('.rangeWrapper',wrapper).append('<input type="text" class="form-control datePicker dateRange '+data.class+'" id="'+data.id+key+'" value="" name="'+value+'" />');
			});

			$('.datePicker', wrapper).datetimepicker({
				timepicker: false,
				format: 'Y-M-D'
			});

			if(data.caption){
				$(wrapper).append(data.caption);
			}
			return $('.form-control', wrapper);
		}, 
		generateDatePeriod: function(key, data){
			data.id = (typeof data.id == "undefined") ? '' : 'input'+ key;
			data.value = (typeof data.value == "undefined" || data.value==null) ? '' : data.value ;
			data.placeholder = (typeof data.placeholder == "undefined") ? '' : data.placeholder ;
			data.class = (typeof data.class == "undefined" || data.class==null) ? '' : data.class ;
			data.rules = (typeof data.rules == "undefined") ? '' : data.rules  ;
			is_required = (/required/.test(data.rules)) ? '*' : '';
			var wrapper = this.generateWrapper(key, this.formTags);
			if(typeof data.label !='undefined'){
				$(wrapper).append('<label for="'+data.id+'">'+data.label+is_required+'</label>');
			}
			$(wrapper).append('<input type="text" class="form-control dateperiod '+data.class+'" id="'+data.id+'" value="'+data.value+'" name="'+data.field+'" />');
			$('.dateperiod', wrapper).daterangepicker({
				datepickerOptions: {
					maxDate: null
				}
			});
			if(data.caption){
				$(wrapper).append(data.caption);
			}
			return $('.form-control', wrapper);
		},
		generateFormTags: function(){
			var url = (this.options.url==''|| this.options.url==null||typeof this.options.url =='undefined') ? '' :'action="'+this.options.url+'"';
			if(this.options.formWrap){
				$(this.element).append('<div class="form blockWrapper"><form '+url+' method="POST" enctype="multitype/form-data"></form></div>');
				this.formTags = $('form',this.element);
			}else{
				$(this.element).append('<div class="form blockWrapper"></div>');
				this.formTags = $('.form',this.element);
			}
			
		},

		generateSuccess: function(xhr){
			this.options.onSuccess(xhr);
			this.generateAlert('success');
			
			if(this.options.isReset){
				this.resetForm();
			}
			this.options.generateSuccess();

		},

		resetForm: function(){
			$('.form-control', this.element).val('');
			$('.form-control',this.element).show();
			$('input[type=hidden]',this.element).val('');
			$('.fileUploadBlock', this.element).remove();
		},

		generateAlert: function(type){
			$('.alert-notif').remove();
			switch(type){
				case 'success' 	: $(this.formTags).before('<div class="alert alert-success alert-notif">'+this.options.successMessage+'</div>'); break;
				case 'error'	: $(this.formTags).before('<div class="alert alert-danger alert-notif">'+this.options.errorMessage+'</div>'); break;
			}
			setTimeout(function(){
				$('.alert-notif',this.formTags).remove();
			}, 3000);
		},

		generateError: function(data){
			this.generateAlert('error');
			_this = this;
			$.each(data, function(key, value){

				if(value != '' && value != null && typeof value != 'undefined'){
					el = $('[name="'+key+'"]',_this.element);

					el.addClass('field-error');
					wrapper = el.closest('.form-group');
					wrapper.addClass('form-error');
					wrapper.append('<small class="error-help">'+value+'</small>');
				}
			});
			
		},
		removeError: function(el){
			$('.field-error',el).removeClass('field-error');
			$('.form-error').removeClass('form-error');
			$('.error-help',el).remove();
		},
		destroy : function(el){
			$(this).empty();

		},
	};

	$.fn[ pluginName ] = function ( options ) {

       	this.each(function() {
                if ( !$.data( this, "plugin_" + pluginName ) ) {
                        $.data( this,pluginName, new Plugin( this, options ) );
                }
        });
        // chain jQuery functions

        return this;

    };

    

})( jQuery, window, document )


<script type="text/javascript">

$(function(){
	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var _xhr;
	$.ajax({
		url: '<?php echo site_url('pengalaman/formFilter')?>',
		async: false,
		dataType: 'json',
		success:function(xhr){
			_xhr = xhr;
		}
	});


	var __currency;
	$.ajax({
		url: '<?php echo site_url('pengalaman/getCurrency')?>',
		async: false,
		dataType: 'json',
		success:function(xhr){
			__currency = xhr;
		}
	});
	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('pengalaman/getData'); ?>',
		data: dataPost,
		
		headers: [
			{
				"key"	: "job_name",
				"value"	: "Nama Paket Pekerjaan"
			},{
				"key"	: "job_location",
				"value"	: "Lokasi"
			},{
				"key"	: "job_giver",
				"value"	: "Pemberi Tugas"
			},{
				"key"	: "phone_no",
				"value"	: "No. Telp. Pemberi Tugas"
			},{
				"key"	: "contract_no",
				"value"	: "No. Kontrak"
			},{
				"key"	: "contract_date",
				"value"	: "Tanggal Kontrak",
				"sort"	: false
			},{
				"key"	: "price_idr",
				"value"	: "Nilai Kontrak (Rp)"
			},{
				"key"	: "price_foreign",
				"value"	: "Nilai Kontrak (Kurs Asing)"
			},{
				"key"	: "contract_file",
				"value"	: "Lampiran Dokumen Kontrak",
				"sort"	: false
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		init:function(){
			$('#tabs').tabs();
		},
		columnDefs : [{
			renderCell: function(data, row, key, el){

				return 'Rp.'+$.number(data[7].value,0, '.',',');
			},
			target : [6]
		},{
			renderCell: function(data, row, key, el){

				return __currency[data[8].value]+'. '+$.number(data[9].value,0, '.',',');
			},
			target : [7]
		},{
			renderCell: function(data, row, key, el){
				console.log(data);
				return '<a href="'+base_url+'assets/lampiran/'+data[10].key+'/'+data[10].value+'" target="_blank"><span><i class="fa fa-download"></i>&nbsp;Lampiran</span></a>';
			},
			target : [8]
		},{
			renderCell: function(data, row, key, el){

				return defaultDate(data[5].value)+' - '+ defaultDate(data[6].value);
			},
			target : [5]
		},{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=editButton(site_url+"pengalaman/edit/"+data[11].value, data[11].value);
				html +=deleteButton(site_url+"pengalaman/remove/"+data[11].value, data[11].value);
				
				return html;
			},
			target : [9]
		}],
		
		additionFeature: function(el){
			el.append(insertButton(site_url+"pengalaman/insert"));
		},
		finish: function(){
			var edit = $('.buttonEdit').modal({
				render : function(el, data){
					_self = edit;

					data.onSuccess = function(){
						
						table.data('plugin_tableGenerator').fetchData();
						
					};
					data.isReset = false;
					
					$(el).form(data).data('form');

				}
			});

			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					_self = edit;
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){

						$(del).data('modal').close();

						table.data('plugin_tableGenerator').fetchData();
						
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});
		},

		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
	var add = $('.buttonAdd').modal({
		render : function(el, data){
			
			data.onSuccess = function(){
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});


</script>
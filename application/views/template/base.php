<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>VMS PGN LNG</title>
	<link rel="stylesheet" href="<?php echo base_url('assets/styles/normalize.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/font/font-awesome/css/font-awesome.min.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/js/fullcalendar/fullcalendar.min.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datepicker/jquery.datetimepicker.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/js/clockpicker/src/clockpicker.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/js/clockpicker/dist/jquery-clockpicker.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/js/daterangepicker/jquery.comiseo.daterangepicker.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/js/jquery-ui/jquery-ui.min.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/styles/base.css'); ?>" type="text/css" media="screen"/>
</head>
<body>

<div id="container">

	<div class="sidebar">
		<div class="bar-side"></div>
		<br class="clear">
		<div class="navbar-brand">
			<a href="<?php echo base_url();?>" >
				<img src="assets/images/pgn-lng-logo.png">
			</a>	
		</div>	
		<br class="clear">
		<!--
		<div class="user-nav">
			<span>Selamat datang, {user}</span>
			<a href="#" class="dropdown-toggle user-icon">
				<img src="<?php echo base_url('assets/images/round-account-button-with-user-inside.png')?>">
			</a>
			<div class="dropdown-menu">
				<ul>
					<li></li> <a href="<?php echo site_url('main/logout')?>"><i class="fa fa-sign-out"></i>Logout</a>	
				</ul>
			</div>
		</div>
		-->
		{sideMenu}
	</div>

	<header class="navbar">
		
				

		<div class="user-nav">
			<div id="inner">
				<div id="user-nav-image">
					<div id="user-nav-imagee">
						<div id="image" style="background-image:url(<?php echo base_url('assets/images/transcosmos.jpg')?>)"></div>
					</div>
				</div>
			</div>
			<div id="user-nav-content">
				<span id="greeting">logged in as</span><br>
				<span id="username">{user}</span>
			</div>
		</div>
		<div id="logout-frame">
			<a href="<?php echo site_url('main/logout')?>">
				<div id="button">
					<i class="fa fa-power-off"></i>
					<div id="logout-text">Logout</div>
				</div>
			</a>	
		</div>
	</header>

	<section class="contentWrap">
		<div class="hbox">
			
			<div class="main">
				<div class="mainWrapper">
					<!--<div id="main-header-background"></div>-->
					<div class="mainwrapper-pull">
						<?php
							if($this->session->userdata('alert'))
						?>
						{breadcrumb}
						<h1 class="page-heading">{header}</h1>
						<div class="row">
							{content}
						</div>
					</div>	
				</div>
			</div>
		</div>
	</section>
</div>

</body>
<script>
	var base_url = "<?php echo base_url()?>";
	var site_url = "<?php echo site_url()?>";
</script>

<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui/jquery-ui.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.imask.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/tableGenerator_v2.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/filter.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/form.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/modal.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/moment-with-locales.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/fullcalendar/fullcalendar.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/tinymce/js/tinymce/tinymce.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/clockpicker/src/clockpicker.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/common.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/datepicker/jquery.datetimepicker.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.number.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/daterangepicker/jquery.comiseo.daterangepicker.js');?>"></script>

{script}
</html>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>VMS PGN LNG</title>
	<link rel="stylesheet" href="<?php echo base_url('assets/styles/normalize.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/font/font-awesome/css/font-awesome.min.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/styles/base.css'); ?>" type="text/css" media="screen"/>
</head>
<body class="loginBody">
<div class="container">
	<div class="row">
		<div class="loginArea-new">
			<img src="<?php echo base_url('assets/images/pgn-lng-logo.png');?>" class="login-logo">
			<div id="left-frame">
				<div id="title">
					Sistem Penyedia Barang Jasa<br>
					<span id="small">VMS & E-PROCUREMENT PGN LNG <!--@ <?php echo date('Y') ?>--></span>
				</div>
			</div>
			<div id="right-frame">
				<div id="header">
					<i class="fa fa-user"></i>
					<div id="text">Silahkan Login</div>
				</div>
				<br class="clear">
				<div class="form">
				</div>
				<div class="form-group btn-group">
					<a href="<?php echo site_url('vendor/register')?>" class="btn btn-success btn-submit buttonBlock">DAFTAR VENDOR BARU</a>
				</div>
			</div>
			<!--
			<div class="contentForm">
				<div class="form">
				</div>
				
				<div class="blockWrapper">
					<em>- or -</em>
					<div class="form-group btn-group">
						<a href="<?php echo site_url('vendor/register')?>" class="btn btn-success btn-submit buttonBlock">DAFTAR VENDOR BARU</a>
					</div>
				</div>
				
			</div>
			<div class="footer">
				<small>VMS & E-PROCUREMENT PGN LNG @ <?php echo date('Y') ?>;</small>
			</div>
			-->
		</div>
	</div>
</div>
</body>
<script>
	var base_url = "<?php echo base_url()?>";
	var site_url = "<?php echo site_url()?>";
</script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.imask.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/form.js');?>"></script>
<script type="text/javascript">
	$(document).ready(function(e){
		var data = $('.loginArea-new #right-frame .form').form({
			url: '<?php echo site_url('main/check'); ?>',
			form: [
				{
					field : 'username',
					type : 'text',
					icon : 'fa fa-user',
					placeholder: 'Username'

				},
				{
					field : 'password',
					type : 'password',
					icon : 'fa fa-key',
					placeholder : 'Password'
				}
			],
			button : [
				{
					type : 'submit',
					label : 'LOGIN',
					field : 'submit',
					class : 'buttonBlock'
				}
			],
			onError: function(xhr){
				this.errorMessage = xhr.message;
				
			},
			onSuccess: function(xhr){
				this.successMessage = xhr.message;
				window.location = xhr.url;
			}
		});
	})
</script>
</html>
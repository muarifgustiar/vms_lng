<script type="text/javascript">

$(function(){
	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('vendor/listDPT'); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "legal_name",
				"value"	: "Badan Usaha"
			},{
				"key"	: "name",
				"value"	: "Nama Penyedia Barang & Jasa"
			},{
				"key"	: "category_name",
				"value"	: "Kategori"
			},{
				"key"	: "npwp_code",
				"value"	: "NPWP"
			},{
				"key"	: "nppkp_code",
				"value"	: "NPPKP"
			},{
				"key"	: "score",
				"value"	: "Skor CSMS"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '<a href="<?php echo site_url('admin/certificate/dpt/')?>'+data[6].value+'"><i class="fa fa-print"></i>&nbsp;Cetak Sertifikat</a>';
				return html;
			},
			target : [6]
		}],
		finish: function(){
			
		}
	});

});


</script>
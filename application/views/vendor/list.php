<div class="mg-lg-12">
	<div class="block">
		<div id="tabs">
			<ul class="tabs nav-tabs" id="tabAbsent">
				<li ><a href="#dpt">Daftar Penyedia Barang & Jasa</a></li>
				<li ><a href="#tunggu">Daftar Tunggu</a></li>
			</ul>
			<div class="tab-pane active" id="dpt">
				<div class="tab-pane active innerTabs" id="recordPending">
					<div id="tableGenerator">
					</div>
				</div>
			</div>
			<div class="tab-pane active" id="tunggu">
				<div class="tab-pane active innerTabs" id="recordTunggu">
					<div id="recordTungguTable">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
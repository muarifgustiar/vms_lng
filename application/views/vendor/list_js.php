<script type="text/javascript">

$(function(){
	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('vendor/listData'); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "legal_name",
				"value"	: "Badan Usaha"
			},{
				"key"	: "vendor",
				"value"	: "Nama Penyedia Barang & Jasa"
			},{
				"key"	: "username",
				"value"	: "Username"
			},{
				"key"	: "status",
				"value"	: "Status"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		init:function(){
			$('#tabs').tabs();
		},
		columnDefs : [{
			renderCell: function(data, row, key, el){
				if(row=='1'){
					return 'Aktif';
				}else{
					return 'Tidak Aktif';
				}
			},
			target : [3]
		},{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=deleteButton(site_url+"vendor/remove/"+data[4].value, data[4].value);
				return html;
			},
			target : [4]
		}],
		
		additionFeature: function(el){
			el.append(insertButton(site_url+"vendor/tambah"));
		},
		finish: function(){
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){

						$(del).data('modal').close();

						table.data('plugin_tableGenerator').fetchData();
						
					};
					data.isReset = true;
					
					$('.form', el).form(data).data('form');
				}
			});
		}
	});

	var table = $('#recordTungguTable').tableGenerator({
		url: '<?php echo site_url('vendor/waiting_list'); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "legal_name",
				"value"	: "Badan Usaha"
			},{
				"key"	: "vendor",
				"value"	: "Nama Penyedia Barang & Jasa"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		init:function(){
			$('#tabs').tabs();
		},
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html ='<a href="<?php echo site_url('approval/approval/');?>/'+row+'" class="btn btn-default"><i class="fa fa-search"></i>&nbspCek Data</a>';
				return html;
			},
			target : [2]
		}],
		
		additionFeature: function(el){
			el.append(insertButton(site_url+"vendor/tambah"));
		},
		finish: function(){
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){

						$(del).data('modal').close();

						table.data('plugin_tableGenerator').fetchData();
						
					};
					data.isReset = true;
					
					$('.form', el).form(data).data('form');
				}
			});
		}
	});
});


</script>
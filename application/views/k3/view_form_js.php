<script type="text/javascript">
<?php $user = $this->session->userdata('user'); ?>
$(function(){
	$.ajax({
		url : '<?php echo site_url('k3/view_form_csms/'.$user['id_user'])?>',
		method: 'POST',
		async : false,
		dataType : 'json',
		success: function(xhr){
			// console.log(xhr);
			xhr.onSuccess = function(data){
				window.location = '<?php echo site_url('k3');?>';
			}
			xhr.successMessage = 'Berhasil Menyimpan Data';
			xhr.url = '<?php echo site_url('k3/saveCSMS')?>';
			var _form = $('#csms');
			var __form = _form.form(xhr);
			$('form',_form).empty();
			var __assign = __form.data('form');
			var __html = '';
			var __idwrap = 0;
			var __id = 0;

			__assign.formTags = $('form', _form);
			__assign.generateButton(__assign.formTags, xhr.button);

			var reset = $('.btn-reset').modal({
				url: '<?php echo site_url('k3/resetForm/')?>/'+xhr.csms_file.csms_file.id,
				header: 'Reset Data',
				render : function(el, data){

					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin mereset data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						location.reload();
						
					};
					data.successMessage = 'Sukses mereset data!';
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

			var edit = $('.btn-edit').on('click', function(){
				window.location = '<?php echo site_url('k3/form_csms')?>';
			})

			$.each(xhr.quest_all, function(key, value){
				$('form', _form).append('<div class="headerQuestion" id="headerQuestion'+key+'"><h2>Bagian '+key+'.  '+value.label+'</h2><div class="subHeader"></div><div>');
				var __headerQuestion = $('form #headerQuestion'+key, _form);

				$.each(value.data, function(keySub, valueSub){
					var __questionWrapper = null;
					var __q = (typeof valueSub.question == 'undefined')?'':'<h3>'+valueSub.question+'</h3>'; 
					$('.subHeader',__headerQuestion).append('<div class="subHeaderQuestion">'+__q+'<div class="questionWrapper" id="questionWrapper'+__idwrap+'"></div></div>');
					__questionWrapper = $('form #questionWrapper'+__idwrap, _form);

					$.each(valueSub.data, function(keyQuestWrap, valueQuestWrap){
						__questionWrapper.append('<div class="questionSubWrapper" id="questionSubWrapper'+__id+'"></div>');

						var __question = $('form #questionSubWrapper'+__id, _form);
						__assign.formTags = __question;
						var $i = 0;
						$.each(valueQuestWrap, function(keyQuestion, valueQuestion){
							// __question.append(valueQuestion.value);
							var __val = '';
							if(typeof xhr.csms_file.answer[keyQuestion] !='undefined'){
								var __val = xhr.csms_file.answer[keyQuestion].value;
							}
							console.log(__val+keyQuestion);
							switch(valueQuestion.type){
								case 'textarea':
									var __data = {
										value:__val,
										label: valueQuestion.value,
										field: 'quest['+keyQuestion+']',
										readonly: true
									};
									
									__assign.generateReadonly($i, __data, 'textarea');
									$i++;
								break;
								case 'file':
									var __data = {
										value: __val,
										readonly: true,
										label: valueQuestion.value,
										field: valueQuestion.label,
										name: valueQuestion.label,
										upload_path: '<?php echo base_url('assets/lampiran/')?>/'+valueQuestion.label,
										upload_url: '<?php echo site_url('k3/upload_lampiran_form')?>',
										allowed_types:'pdf|jpeg|jpg|png|gif|rar|zip'
									};
									
									__assign.generateReadonly($i, __data, 'file');
									$i++;
								break;
								case 'radio':
									var __data = {
										value: __val,
										readonly: true,
										label: valueQuestion.value,
										field: 'quest['+keyQuestion+']',
										source: valueQuestion.label.split('|'),
										
									};
									
									__assign.generateReadonly($i, __data,'radio');
									$i++;
								break;

							}
						});
						__id++;
					});
					__idwrap++;
				})
			});
			
		}
	});
});


</script>
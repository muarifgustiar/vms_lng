<script type="text/javascript">

$(function(){
	var xhr;
	$.ajax({
		url : '<?php echo site_url('k3/getData')?>',
		method: 'POST',
		async : false,
		dataType : 'json',
		success: function(xhr){
			$('#detailK3').form(xhr);
			var edit = $('.btn-edit').modal({
				url: '<?php echo site_url('k3/edit/')?>/'+xhr.id,
				header: 'Proses Data',
				render : function(el, data){
					_self = edit;

					data.onSuccess = function(){
						location.reload();
					};
					data.isReset = false;
					$(el).form(data).data('form');

				}
			});
			var del = $('.btn-reset').modal({
				url: '<?php echo site_url('k3/resetForm/')?>/'+xhr.id,
				header: 'Proses Data',
				render : function(el, data){
					_self = edit;
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin mereset data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						location.reload();
						
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});
		}


	});
});


</script>
<script type="text/javascript">

$(function(){
	var data;
	$.ajax({
		url: '<?php echo site_url('k3/field_k3')?>',
		async: false,
		dataType: 'json',
		success:function(xhr){
			data = xhr;
		}
	})
	var fw = $('#formK3').formWizard({
		data: data,
		url: '<?php echo site_url('k3/save/')?>',
		onNext: function(){
			_wizard = $(fw).data('formWizard');
			
			step = _wizard.options.data.step;
			var _step = [];
			var i = 1;
			$.each(step, function(key, value){
				_step[i] = key;
				i++;
			})
			_formWrapper = $('.wizard-content #step'+_wizard.options.currentPosition);
			_form = $('form',_wizard.options.wrapper);
			formData = new FormData( _form[0]);
			formData .append('validation', _step[_wizard.options.currentPosition]);

			$.ajax({
				url: '<?php echo site_url('k3/simpan')?>',
				data: formData,
				method : 'POST',
				processData: false,
				contentType: false,
				async: false,
				dataType: 'json',	
				success: function(xhr){
					_formWrapper.data('form').element = _formWrapper;
					

					if(xhr.status=='error') {
						_formWrapper.data('form').options.errorMessage = 'Terjadi Kesalahan';
						_formWrapper.data('form').generateError(xhr.form);
						_return = false;
					}else{
						_formWrapper.data('form').removeError(_formWrapper);
						_return = true;
					}
				}
			});
			if(_wizard.options.currentPosition==1){
				
				if($('input.form-control[name="csms"]:checked').val()==2){
					window.location = '<?php echo site_url('k3/form_csms')?>';
					return false;
				}
			}


			return _return;
		},
		onSubmit: function(){

		},
		onSuccessSubmit: function(){
			location.reload();
		},
		onSuccess: function(){

		}
	})
});	
</script>
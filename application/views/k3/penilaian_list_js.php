<script type="text/javascript">

$(function(){
	dataPost = {
		order: 'a.id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('k3/listPenilaian'); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "legal_name",
				"value"	: "Badan Usaha"
			},{
				"key"	: "vendor",
				"value"	: "Nama Penyedia Barang & Jasa"
			},{
				"key"	: "score",
				"value"	: "Skor Terakhir"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		init:function(){
			$('#tabs').tabs();
		},
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';
				html +='<a class="btn btn-default" href="'+base_url+'k3/form_penilaian/'+data[3].value+'"><i class="fa fa-pencil-square"></i>&nbsp; Nilai CSMS</a>';
				html +='<a class="btn btn-default" href="'+base_url+'k3/penilaian_view/'+data[3].value+'"><i class="fa fa-search"></i>&nbsp; Lihat Nilai</a>';
				html +='<a class="btn btn-default" href="'+base_url+'k3/history_nilai/'+data[3].value+'"><i class="fa fa-database"></i>&nbsp; Histori Nilai</a>';
				return html;
			},
			target : [3]
		}],
		
	});
});


</script>
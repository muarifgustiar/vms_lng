<script type="text/javascript">
<?php $user = $this->session->userdata('user'); ?>
$(function(){
	$.ajax({
		url : '<?php echo site_url('k3/get_form_csms/'.$user['id_user'])?>',
		method: 'POST',
		async : false,
		dataType : 'json',
		success: function(xhr){
			// console.log(xhr);
			xhr.onSuccess = function(data){
				window.location = '<?php echo site_url('k3');?>';
			}
			xhr.successMessage = 'Berhasil Menyimpan Data';
			xhr.url = '<?php echo site_url('k3/saveCSMS')?>';
			var _form = $('.formCSMS');
			var __form = _form.form(xhr);
			$('form',_form).empty();
			var __assign = __form.data('form');
			var __html = '';
			var __idwrap = 0;
			var __id = 0;
			$.each(xhr.quest_all, function(key, value){
				$('form', _form).append('<div class="headerQuestion" id="headerQuestion'+key+'"><h2>Bagian '+key+'.  '+value.label+'</h2><div class="subHeader"></div><div>');
				var __headerQuestion = $('form #headerQuestion'+key, _form);

				$.each(value.data, function(keySub, valueSub){
					var __questionWrapper = null;
					var __q = (typeof valueSub.question == 'undefined')?'':'<h3>'+valueSub.question+'</h3>'; 
					$('.subHeader',__headerQuestion).append('<div class="subHeaderQuestion">'+__q+'<div class="questionWrapper" id="questionWrapper'+__idwrap+'"></div></div>');
					__questionWrapper = $('form #questionWrapper'+__idwrap, _form);

					$.each(valueSub.data, function(keyQuestWrap, valueQuestWrap){
						__questionWrapper.append('<div class="questionSubWrapper" id="questionSubWrapper'+__id+'"></div>');

						var __question = $('form #questionSubWrapper'+__id, _form);
						__assign.formTags = __question;
						var $i = 0;
						$.each(valueQuestWrap, function(keyQuestion, valueQuestion){
							var __val = '';

							if(typeof xhr.csms_file.answer != 'undefined' && typeof xhr.csms_file.answer[keyQuestion] !='undefined'){
								
								var __val = xhr.csms_file.answer[keyQuestion].value;
							}

							switch(valueQuestion.type){
								case 'textarea':
									var __data = {
										
										value:__val,
										label: valueQuestion.value,
										field: 'quest['+keyQuestion+']',
									};
									
									__assign.generateTextarea($i, __data, 'textarea');
									$i++;
								break;
								case 'file':
									var __data = {
										value:__val,
										label: valueQuestion.value,
										field: 'quest['+keyQuestion+']',
										name: valueQuestion.label,
										upload_path: '<?php echo base_url('assets/lampiran/')?>/'+valueQuestion.label,
										upload_url: '<?php echo site_url('k3/upload_lampiran_form')?>',
										allowed_types:'pdf|jpeg|jpg|png|gif|rar|zip'
									};
									
									__assign.generateFile($i, __data, 'file');
									$i++;
								break;
								case 'radio':
									__val = parseInt(__val);
									var __data = {
										value:__val,
										label: valueQuestion.value,
										field: 'quest['+keyQuestion+']',
										source: valueQuestion.label.split('|'),
										
									};
									
									__assign.generateRadio($i, __data);
									$i++;
								break;

							}
						});
						__id++;
					});
					__idwrap++;
				})
			});
			
			__assign.formTags = $('form', _form);
			__assign.generateButton(__assign.formTags, xhr.button);
		}
	});
});


</script>
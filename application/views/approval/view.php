<div class="mg-lg-12">
	<div class="block">
		<div id="tabs">
			<ul class="tabs nav-tabs" id="tabAbsent">
				<li id="administrasi"><a href="<?php echo site_url('approval/administrasi/'.$id.'/0');?>">Administrasi</a></li>
				<li id="akta"><a href="<?php echo site_url('approval/akta/'.$id.'/0');?>">Akta</a></li>
				<li id="situ"><a href="<?php echo site_url('approval/situ/'.$id.'/0');?>">Situ</a></li>
				<li id="tdp"><a href="<?php echo site_url('approval/tdp/'.$id.'/0');?>">TDP</a></li>
				<li id="pengurus"><a href="<?php echo site_url('approval/pengurus/'.$id.'/0');?>">Pengurus</a></li>
				<li id="pemilik"><a href="<?php echo site_url('approval/pemilik/'.$id.'/0');?>">Pemilik</a></li>
				<li id="izin"><a href="<?php echo site_url('approval/izin/'.$id.'/0');?>">Izin Usaha</a></li>
				<li id="agen"><a href="<?php echo site_url('approval/agen/'.$id.'/0');?>">Pabrikan/Keagenan/Distributor</a></li>
				<li id="pengalaman"><a href="<?php echo site_url('approval/pengalaman/'.$id.'/0');?>">Pengalaman</a></li>
				<?php if(is_k3($id)){ ?>
				<li id="csms"><a href="<?php echo site_url('approval/csms/'.$id.'/0');?>">CSMS</a></li>
				<?php } ?>
				<li ><a href="<?php echo site_url('approval/verifikasi/'.$id);?>">Verifikasi DPT</a></li>
			</ul>
		</div>
	</div>
</div>
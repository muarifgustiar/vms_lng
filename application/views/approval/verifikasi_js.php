<script type="text/javascript">

$(function(){
    $('.graphBar').highchart({

    	colors: ["#2CC36B", "#F39C12", "#C0392B",],
        chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['Data']
        },
        yAxis: {
            title: {
                text: 'Total Data Dimasukan'
            }
        },
        legend: {
            backgroundColor: '#FFFFFF',

            reversed: true
		},
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },

        series: [{
    		name: 'Data Sesuai',
            data: [<?php echo count($approval_data[1])?>]
        },{
            name: 'Belum Di Verifikasi',
            data: [<?php echo count($approval_data[0])?>]
        }, {
            name: 'Data tidak valid',
            data: [<?php echo count($approval_data[3])?>]
        }]
    });
});



</script>
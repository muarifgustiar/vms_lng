<script type="text/javascript">

$(function(){
	$('#tabs').tabs({
		load: function(event, ui){
			var __id = ui.tab.attr('id');
			switch(__id){
				case 'administrasi':
					$.ajax({
						url : '<?php echo site_url('administrasi/approval/'.$id.'/0')?>',
						method: 'POST',
						async : false,
						dataType : 'json',
						success: function(xhr){
							xhr.formWrap = false;
							$('.formAdministrasi').form(xhr);
						}


					});
				break;
				case 'akta':
					dataPost = {
						order: 'id',
						sort: 'desc'
					};
					var optTypeAkta = {
						'pendirian':'Akta Pendirian',
						'perubahan':'Akta - Akta Perubahan Terakhir (mengenai anggaran dasar)',
						'direksi':'Akta - Akta Perubahan Terakhir (mengenai susunan terakhir direksi dan komisaris)',
						'saham':'Akta - Akta Perubahan Terakhir (mengenai susunan terakhir pemegang saham)'
					};
					var table = $('#aktaTable').tableGenerator({
						url: '<?php echo site_url('akta/getApproval/'.$id); ?>',						
						headers: [
							{
								"key"	: "type",
								"value"	: "Tipe"
							},{
								"key"	: "no",
								"value"	: "No Akta"
							},{
								"key"	: "notaris",
								"value"	: "Notaris"
							},{
								"key"	: "issue_date",
								"value"	: "Tanggal Penerbitan"
							},{
								"key"	: "akta_file",
								"value"	: "Lampiran Akta",
								"sort"	: false
							},{
								"key"	: "authorize_by",
								"value"	: "Lembaga Pengesah"
							},{
								"key"	: "authorize_no",
								"value"	: "No Pengesahan"
							},{
								"key"	: "authorize_date",
								"value"	: "Tanggal Pengesahan"
							},{
								"key"	: "authorize_file",
								"value"	: "Lampiran Pengesahan",
								"sort"	: false
							},{
								"key"	: "akta_extension_file",
								"value"	: "Bukti Perpanjangan Akta"
							},{
								"key"	: "mandatory",
								"value"	: "<i class='fa fa-exclamation-triangle' style='color:#f39c12'></i>",
								"sort"	: false
							},{
								"key"	: "ok",
								"value"	: "<i class='fa fa-check' style='color:#27ae60'></i>",
								"sort"	: false
							},{
								"key"	: "no",
								"value"	: "<i class='fa fa-times' style='color: #c1392b'></i>",
								"sort"	: false
							},

						],
						
						columnDefs : [{
							renderCell: function(data, row, key, el){
								return optTypeAkta[row];
							},
							target : [0]
						},{
							renderCell: function(data, row, key, el){
								return defaultDate(row);
							},
							target : [3,7]
						},{
							renderCell: function(data, row, key, el){
								return '<a href="'+base_url+'assets/lampiran/'+data[key].key+'/'+row+'" target="_blank"><span><i class="fa fa-download"></i>&nbsp;Lampiran</span></a>';
							},
							target : [4,8,9]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[11].value=='1'||data[11].value=='3'){
									_checked = 'checked';
								} 
								return '<input type="checkbox" name="akta['+data[10].value+'][mandatory]" '+_checked+' value="1">';
							},
							target : [10]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[11].value=='1'||data[11].value=='2'){
									_checked = 'checked';
								} 
								return '<input type="radio" name="akta['+data[10].value+'][status]" value="1" '+_checked+'>';
							},
							target : [11]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[11].value=='3'||data[11].value=='4') {
									_checked = 'checked';
								}
								return '<input type="radio" name="akta['+data[10].value+'][status]" value="0" '+_checked+'>';
							},
							target : [12]
						}],
					});
				break;

				case 'situ':
					var table = $('#situTable').tableGenerator({
						url: '<?php echo site_url('situ/getApproval/'.$id); ?>',
						
						headers: [
							{
								"key"	: "type",
								"value"	: "Nama Surat"
							},{
								"key"	: "no",
								"value"	: "Nomor"
							},{
								"key"	: "address",
								"value"	: "Alamat"
							},{
								"key"	: "issue_date",
								"value"	: "Tanggal"
							},{
								"key"	: "expire_date",
								"value"	: "Masa Berlaku"
							},{
								"key"	: "situ_file",
								"value"	: "Lampiran SKDP",
								"sort"	: false
							},{
								"key"	: "authorize_by",
								"value"	: "Lembaga Penerbit"
							},{
								"key"	: "file_extension_situ",
								"value"	: "Bukti sedang dalam proses perpanjangan",
								"sort"	: false
							},{
								"key"	: "mandatory",
								"value"	: "<i class='fa fa-exclamation-triangle' style='color:#f39c12'></i>",
								"sort"	: false
							},{
								"key"	: "ok",
								"value"	: "<i class='fa fa-check' style='color:#27ae60'></i>",
								"sort"	: false
							},{
								"key"	: "no",
								"value"	: "<i class='fa fa-times' style='color: #c1392b'></i>",
								"sort"	: false
							},
						],
						columnDefs : [{
							renderCell: function(data, row, key, el){
								return defaultDate(row);
							},
							target : [3,4]
						},{
							renderCell: function(data, row, key, el){
								return '<a href="'+base_url+'assets/lampiran/'+data[key].key+'/'+row+'" target="_blank"><span><i class="fa fa-download"></i>&nbsp;Lampiran</span></a>';
							},
							target : [5,7]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[9].value=='1'||data[9].value=='3'){
									_checked = 'checked';
								} 
								return '<input type="checkbox" name="situ['+data[8].value+'][mandatory]" '+_checked+' value="1">';
							},
							target : [8]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[9].value=='1'||data[9].value=='2'){
									_checked = 'checked';
								} 
								return '<input type="radio" name="situ['+data[8].value+'][status]" value="1" '+_checked+'>';
							},
							target : [9]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[9].value=='3'||data[9].value=='4') {
									_checked = 'checked';
								}
								return '<input type="radio" name="situ['+data[8].value+'][status]" value="0" '+_checked+'>';
							},
							target : [10]
						}]
					});
				break;

				case 'tdp':
					var table = $('#tdpTable').tableGenerator({
						url: '<?php echo site_url('tdp/getApproval/'.$id); ?>',
						
						headers: [
							{
								"key"	: "no",
								"value"	: "No Akta"
							},{
								"key"	: "issue_date",
								"value"	: "Tanggal Penerbitan"
							},{
								"key"	: "expire_date",
								"value"	: "Masa Berlaku"
							},{
								"key"	: "authorize_by",
								"value"	: "Lembaga Penerbit"
							},{
								"key"	: "tdp_file",
								"value"	: "File Akta",
								"sort"	: false
							},{
								"key"	: "tdp_extension_file",
								"value"	: "Bukti Perpanjangan",
								"sort"	: false
							},{
								"key"	: "mandatory",
								"value"	: "<i class='fa fa-exclamation-triangle' style='color:#f39c12'></i>",
								"sort"	: false
							},{
								"key"	: "ok",
								"value"	: "<i class='fa fa-check' style='color:#27ae60'></i>",
								"sort"	: false
							},{
								"key"	: "no",
								"value"	: "<i class='fa fa-times' style='color: #c1392b'></i>",
								"sort"	: false
							},
						],
						columnDefs : [{
							renderCell: function(data, row, key, el){
								return defaultDate(row);
							},
							target : [1,2]
						},{
							renderCell: function(data, row, key, el){
								return '<a href="'+base_url+'assets/lampiran/'+data[key].key+'/'+row+'" target="_blank"><span><i class="fa fa-download"></i>&nbsp;Lampiran</span></a>';
							},
							target : [4,5]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[7].value=='1'||data[7].value=='3'){
									_checked = 'checked';
								} 
								return '<input type="checkbox" name="tdp['+data[6].value+'][mandatory]" '+_checked+' value="1">';
							},
							target : [6]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[7].value=='1'||data[7].value=='2'){
									_checked = 'checked';
								} 
								return '<input type="radio" name="tdp['+data[6].value+'][status]" value="1" '+_checked+'>';
							},
							target : [7]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[7].value=='3'||data[7].value=='4') {
									_checked = 'checked';
								}
								return '<input type="radio" name="tdp['+data[6].value+'][status]" value="0" '+_checked+'>';
							},
							target : [8]
						}]
					});
				break;

				case 'pengurus':
					var table = $('#pengurusTable').tableGenerator({
						url: '<?php echo site_url('pengurus/getApproval/'.$id); ?>',
						
						headers: [
							{
								"key"	: "no_akta",
								"value"	: "No Akta"
							},{
								"key"	: "no",
								"value"	: "Nomor"
							},{
								"key"	: "name",
								"value"	: "Nama"
							},{
								"key"	: "position",
								"value"	: "Posisi"
							},{
								"key"	: "pengurus_file",
								"value"	: "Lampiran SKDP",
								"sort"	: false
							},{
								"key"	: "expire_date",
								"value"	: "Masa Berlaku"
							},{
								"key"	: "mandatory",
								"value"	: "<i class='fa fa-exclamation-triangle' style='color:#f39c12'></i>",
								"sort"	: false
							},{
								"key"	: "ok",
								"value"	: "<i class='fa fa-check' style='color:#27ae60'></i>",
								"sort"	: false
							},{
								"key"	: "no",
								"value"	: "<i class='fa fa-times' style='color: #c1392b'></i>",
								"sort"	: false
							},
						],
						columnDefs : [{
							renderCell: function(data, row, key, el){
								return defaultDate(row);
							},
							target : [5]
						},{
							renderCell: function(data, row, key, el){
								return '<a href="'+base_url+'assets/lampiran/'+data[key].key+'/'+row+'" target="_blank"><span><i class="fa fa-download"></i>&nbsp;Lampiran</span></a>';
							},
							target : [4]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[7].value=='1'||data[7].value=='3'){
									_checked = 'checked';
								} 
								return '<input type="checkbox" name="pengurus['+data[6].value+'][mandatory]" '+_checked+' value="1">';
							},
							target : [6]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[7].value=='1'||data[7].value=='2'){
									_checked = 'checked';
								} 
								return '<input type="radio" name="pengurus['+data[6].value+'][status]" value="1" '+_checked+'>';
							},
							target : [7]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[7].value=='3'||data[7].value=='4') {
									_checked = 'checked';
								}
								return '<input type="radio" name="pengurus['+data[6].value+'][status]" value="0" '+_checked+'>';
							},
							target : [8]
						}]
					});
				break;
				case 'pemilik':
					var table = $('#pemilikTable').tableGenerator({
						url: '<?php echo site_url('pemilik/getApproval/'.$id); ?>',
						
						headers: [{
								"key"	: "no_akta",
								"value"	: "No Akta"
							},{
								"key"	: "name",
								"value"	: "Nama"
							},{
								"key"	: "shares",
								"value"	: "Komposisi Saham (lembar)"
							},{
								"key"	: "percentage",
								"value"	: "Komposisi Saham (Nominal)"
							},{
								"key"	: "mandatory",
								"value"	: "<i class='fa fa-exclamation-triangle' style='color:#f39c12'></i>",
								"sort"	: false
							},{
								"key"	: "ok",
								"value"	: "<i class='fa fa-check' style='color:#27ae60'></i>",
								"sort"	: false
							},{
								"key"	: "no",
								"value"	: "<i class='fa fa-times' style='color: #c1392b'></i>",
								"sort"	: false
							},
						],
						columnDefs : [{
							renderCell: function(data, row, key, el){
								return $.number(row,0, '.',',');
							},
							target : [3]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[5].value=='1'||data[5].value=='3'){
									_checked = 'checked';
								} 
								return '<input type="checkbox" name="pemilik['+data[4].value+'][mandatory]" '+_checked+' value="1">';
							},
							target : [4]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[5].value=='1'||data[5].value=='2'){
									_checked = 'checked';
								} 
								return '<input type="radio" name="pemilik['+data[4].value+'][status]" value="1" '+_checked+'>';
							},
							target : [5]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[5].value=='3'||data[5].value=='4') {
									_checked = 'checked';
								}
								return '<input type="radio" name="pemilik['+data[4].value+'][status]" value="0" '+_checked+'>';
							},
							target : [6]
						}]
					});
				break;
				case 'izin':
					var __dptType;
					$.ajax({
						url: '<?php echo site_url('izin/getDPTType')?>',
						async: false,
						dataType: 'json',
						success:function(xhr){
							__dptType = xhr;
						}
					})
					var __iuType = {
						'siup':'SIUP',
						'siujk': 'SIUJK',
						'ijin_lain':'Surat Ijin Usaha Lainnya',
						'sbu':'SBU',
						'asosiasi':'Sertifikat Asosiasi/Lainnya'
					};
					var table = $('#izinTable').tableGenerator({
						url: '<?php echo site_url('izin/getApproval/'.$id); ?>',
						
						headers: [{
								"key"	: "id_dpt_type",
								"value"	: "Klasifikasi Penyedia Barang & Jasa"
							},{
								"key"	: "type",
								"value"	: "Tipe"
							},{
								"key"	: "no",
								"value"	: "No"
							},{
								"key"	: "issue_date",
								"value"	: "Tanggal Penerbitan"
							},{
								"key"	: "grade",
								"value"	: "Grade"
							},{
								"key"	: "qualification",
								"value"	: "Kualifikasi"
							},{
								"key"	: "authorize_by",
								"value"	: "Lembaga Pengesah"
							},{
								"key"	: "expire_date",
								"value"	: "Masa Berlaku"
							},{
								"key"	: "izin_file",
								"value"	: "Lampiran Izin"
							},{
								"key"	: "bsb",
								"value"	: "Bidang / Sub-bidang",
								"sort"	: false
							},{
								"key"	: "mandatory",
								"value"	: "<i class='fa fa-exclamation-triangle' style='color:#f39c12'></i>",
								"sort"	: false
							},{
								"key"	: "ok",
								"value"	: "<i class='fa fa-check' style='color:#27ae60'></i>",
								"sort"	: false
							},{
								"key"	: "no",
								"value"	: "<i class='fa fa-times' style='color: #c1392b'></i>",
								"sort"	: false
							},
						],
						columnDefs : [{

							renderCell: function(data, row, key, el){
								console.log(data);
								return __dptType[row];
							},
							target : [0]
						},{
							renderCell: function(data, row, key, el){
								return __iuType[row];
							},
							target : [1]
						},{
							renderCell: function(data, row, key, el){
								return defaultDate(row);
							},
							target : [3,7]
						},{
							renderCell: function(data, row, key, el){
								return '<a href="'+base_url+'assets/lampiran/'+data[key].key+'/'+row+'" target="_blank"><span><i class="fa fa-download"></i>&nbsp;Lampiran</span></a>';
							},
							target : [8]
						},{
							renderCell: function(data, row, key, el){
								var __bsb;
								var __html;
								$.ajax({
									url: '<?php echo site_url('izin/getBSB')?>',
									async: false,
									data:{
										id_ijin_usaha : data[9].value
									},
									method: 'POST',
									dataType: 'json',
									success:function(xhr){

										__bsb = xhr;
										
										__html = '<ul>';
										$.each(__bsb, function(key, value){
											__html += '<li>'+key;
											__html += '<ul>';
												$.each(value, function(keyBSB, valueBSB){
													__html += '<li>'+valueBSB+'</li>';
												});
											__html += '</ul>';
											__html += '</li>';
										});
										__html += '<ul>';
									}
								})
								return __html;
							},
							target : [9]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[10].value=='1'||data[10].value=='3'){
									_checked = 'checked';
								} 
								return '<input type="checkbox" name="izin['+data[9].value+'][mandatory]" '+_checked+' value="1">';
							},
							target : [10]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[10].value=='1'||data[10].value=='2'){
									_checked = 'checked';
								} 
								return '<input type="radio" name="izin['+data[9].value+'][status]" value="1" '+_checked+'>';
							},
							target : [11]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[10].value=='3'||data[10].value=='4') {
									_checked = 'checked';
								}
								return '<input type="radio" name="izin['+data[9].value+'][status]" value="0" '+_checked+'>';
							},
							target : [12]
						}]
					});
				break;
				case 'agen':
					var table = $('#agenTable').tableGenerator({
						url: '<?php echo site_url('agen/getApproval/'.$id); ?>',
						
						headers: [{
								"key"	: "no",
								"value"	: "No"
							},{
								"key"	: "principal",
								"value"	: "Principal"
							},{
								"key"	: "issue_date",
								"value"	: "Tanggal"
							},{
								"key"	: "type",
								"value"	: "Tipe",
							},{
								"key"	: "expiry_date",
								"value"	: "Masa Berlaku"
							},{
								"key"	: "agen_file",
								"value"	: "Lampiran Agen"
							},{
								"key"	: "mandatory",
								"value"	: "<i class='fa fa-exclamation-triangle' style='color:#f39c12'></i>",
								"sort"	: false
							},{
								"key"	: "ok",
								"value"	: "<i class='fa fa-check' style='color:#27ae60'></i>",
								"sort"	: false
							},{
								"key"	: "no",
								"value"	: "<i class='fa fa-times' style='color: #c1392b'></i>",
								"sort"	: false
							},
						],
						columnDefs : [{
							renderCell: function(data, row, key, el){
								return defaultDate(row);
							},
							target : [2,4]
						},{
							renderCell: function(data, row, key, el){
								return '<a href="'+base_url+'assets/lampiran/'+data[key].key+'/'+row+'" target="_blank"><span><i class="fa fa-download"></i>&nbsp;Lampiran</span></a>';
							},
							target : [5]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[7].value=='1'||data[7].value=='3'){
									_checked = 'checked';
								} 
								return '<input type="checkbox" name="agen['+data[6].value+'][mandatory]" '+_checked+' value="1">';
							},
							target : [6]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[7].value=='1'||data[7].value=='2'){
									_checked = 'checked';
								} 
								return '<input type="radio" name="agen['+data[6].value+'][status]" value="1" '+_checked+'>';
							},
							target : [7]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[7].value=='3'||data[7].value=='4') {
									_checked = 'checked';
								}
								return '<input type="radio" name="agen['+data[6].value+'][status]" value="0" '+_checked+'>';
							},
							target : [8]
						}]
					});
				break;
				case 'pengalaman':
				var __currency;
				$.ajax({
					url: '<?php echo site_url('pengalaman/getCurrency')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						__currency = xhr;
					}
				});
					var table = $('#pengalamanTable').tableGenerator({
						url: '<?php echo site_url('pengalaman/getApproval/'.$id); ?>',
						
						headers: [{
								"key"	: "job_name",
								"value"	: "Nama Paket Pekerjaan"
							},{
								"key"	: "job_location",
								"value"	: "Lokasi"
							},{
								"key"	: "job_giver",
								"value"	: "Pemberi Tugas"
							},{
								"key"	: "phone_no",
								"value"	: "No. Telp. Pemberi Tugas"
							},{
								"key"	: "contract_no",
								"value"	: "No. Kontrak"
							},{
								"key"	: "contract_date",
								"value"	: "Tanggal Kontrak",
								"sort"	: false
							},{
								"key"	: "price_idr",
								"value"	: "Nilai Kontrak (Rp)"
							},{
								"key"	: "price_foreign",
								"value"	: "Nilai Kontrak (Kurs Asing)"
							},{
								"key"	: "contract_file",
								"value"	: "Lampiran Dokumen Kontrak",
								"sort"	: false
							},{
								"key"	: "mandatory",
								"value"	: "<i class='fa fa-exclamation-triangle' style='color:#f39c12'></i>",
								"sort"	: false
							},{
								"key"	: "ok",
								"value"	: "<i class='fa fa-check' style='color:#27ae60'></i>",
								"sort"	: false
							},{
								"key"	: "no",
								"value"	: "<i class='fa fa-times' style='color: #c1392b'></i>",
								"sort"	: false
							},
						],
						columnDefs : [{
							renderCell: function(data, row, key, el){

								return 'Rp.'+$.number(data[7].value,0, '.',',');
							},
							target : [6]
						},{
							renderCell: function(data, row, key, el){

								return __currency[data[8].value]+'. '+$.number(data[9].value,0, '.',',');
							},
							target : [7]
						},{
							renderCell: function(data, row, key, el){
								console.log(data);
								return '<a href="'+base_url+'assets/lampiran/'+data[10].key+'/'+data[10].value+'" target="_blank"><span><i class="fa fa-download"></i>&nbsp;Lampiran</span></a>';
							},
							target : [8]
						},{
							renderCell: function(data, row, key, el){

								return defaultDate(data[5].value)+' - '+ defaultDate(data[6].value);
							},
							target : [5]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[10].value=='1'||data[10].value=='3'){
									_checked = 'checked';
								} 
								return '<input type="checkbox" name="pengalaman['+data[9].value+'][mandatory]" '+_checked+' value="1">';
							},
							target : [9]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[10].value=='1'||data[10].value=='2'){
									_checked = 'checked';
								} 
								return '<input type="radio" name="pengalaman['+data[9].value+'][status]" value="1" '+_checked+'>';
							},
							target : [10]
						},{
							renderCell: function(data, row, key, el){
								var _checked;
								if(data[10].value=='3'||data[10].value=='4') {
									_checked = 'checked';
								}
								return '<input type="radio" name="pengalaman['+data[9].value+'][status]" value="0" '+_checked+'>';
							},
							target : [11]
						}]
					});
				break;
			}

			$('form',this).on('submit', function(e){
				e.preventDefault();
				_this = this;
				form = $(this);
				formData = new FormData($(this)[0]);
				
				$.ajax({
					async: false,
					url : $(_this).attr('action'),
					method : 'POST',
					data: formData,
					processData: false,
					contentType: false,
					dataType: 'json',	
	   				beforeSend: function(xhr){
	   					console.log($(_this).attr('action'));
						// $('.btn-submit',form).attr('disabled', 'disabled').addClass('btn-loader');
					},
					success: function(xhr){
						console.log($(_this).attr('action'));
					},
					complete: function(xhr){
						console.log($(_this).attr('action'));
					}
				})
			});
		}
	});
});


</script>
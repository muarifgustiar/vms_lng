<div class="blockWrapper">
	<form method="POST">
		<h3>Summary Data</h3>
		<div class="graphBar" style="min-width: 400px; height: 150px; margin: 0 auto">
		</div>
		<div class="alert alert-success row">
			<h4><?php echo count($approval_data[1])?> data telah sesuai</h4>
			<ul>
				<?php foreach($approval_data[1] as $key =>$value){ ?>
				<li><?php echo $value;?></li>
				<?php } ?>
			</ul>
		</div>
		<div class="alert alert-warning row">
			<h4><?php echo count($approval_data[0])?> data belum terverifikasi</h4>
			<ul>
				
				<?php foreach($approval_data[0] as $key =>$value){ ?>
				<li><?php echo $value;?></li>
				<?php } ?>
			</ul>
		</div>
		<div class="alert alert-danger row">
			<h4><?php echo count($approval_data[3])?> data tidak sesuai</h4>
			<ul>
				
				<?php foreach($approval_data[3] as $key =>$value){ ?>
				<li><?php echo $value;?></li>
				<?php } ?>
			</ul>
		</div>
		<div class="buttonRegBox clearfix">
			<?php 
			if(!$need_approve){ ?>
				<!--<input type="hidden" id="certificate_no" name="certificate_no" value="<?php echo $certificate_no;?>">-->
				<button type="submit" class="btn btn-primary"><i class="fa fa-user-plus"></i>&nbsp;Angkat Menjadi DPT</button>
			<?php } ?>
		</div>
	</form>
</div>

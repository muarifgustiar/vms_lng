<div class="mg-lg-7 ">
	<div class="block ">
		<form action="<?php echo site_url('approval/administrasi/'.$id_vendor.'/1')?>" method="POST">
			<div class="formAdministrasi">
			</div>
			<div class="approvalWrap clearfix">
				<label class="orangeAtt">
					<input type="checkbox" name="mandatory" value="1" <?php echo $this->data_process->set_mandatory($data_status);?>>&nbsp;<i class="fa fa-exclamation-triangle"></i>&nbsp;Mandatory
				</label>
				<label class="nephritisAtt">
					<input type="radio" name="status" value="1" <?php echo $this->data_process->set_yes_no(1,$data_status);?>>&nbsp;<i class="fa fa-check"></i>&nbsp;OK
				</label>
				<label class="pomegranateAtt">
					<input type="radio" name="status" value="0" <?php echo $this->data_process->set_yes_no(0,$data_status);?>>&nbsp;<i class="fa fa-times"></i>&nbsp;Not OK
				</label>
				<div class="buttonRegBox clearfix">
					<input type="submit" value="Simpan" class="btn btn-primary" name="simpan">
				</div>
			</div>
			
		</form>
	</div>
</div>
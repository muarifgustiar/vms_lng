<script type="text/javascript">

$(function(){
	dataPost = {
		order: 'id',
		sort: 'desc'
	};
	var optTypeAkta = {
		'pendirian':'Akta Pendirian',
		'perubahan':'Akta - Akta Perubahan Terakhir (mengenai anggaran dasar)',
		'direksi':'Akta - Akta Perubahan Terakhir (mengenai susunan terakhir direksi dan komisaris)',
		'saham':'Akta - Akta Perubahan Terakhir (mengenai susunan terakhir pemegang saham)'
	};
	var _xhr;
	$.ajax({
					url: '<?php echo site_url('akta/formFilter')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})
	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('akta/getData'); ?>',
		data: dataPost,
		
		headers: [
			{
				"key"	: "type",
				"value"	: "Tipe"
			},{
				"key"	: "no",
				"value"	: "No Akta"
			},{
				"key"	: "notaris",
				"value"	: "Notaris"
			},{
				"key"	: "issue_date",
				"value"	: "Tanggal Penerbitan"
			},{
				"key"	: "akta_file",
				"value"	: "Lampiran Akta",
				"sort"	: false
			},{
				"key"	: "authorize_by",
				"value"	: "Lembaga Pengesah"
			},{
				"key"	: "authorize_no",
				"value"	: "No Pengesahan"
			},{
				"key"	: "authorize_date",
				"value"	: "Tanggal Pengesahan"
			},{
				"key"	: "authorize_file",
				"value"	: "Lampiran Pengesahan",
				"sort"	: false
			},{
				"key"	: "akta_extension_file",
				"value"	: "Bukti Perpanjangan Akta"
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		init:function(){
			$('#tabs').tabs();
		},
		columnDefs : [{
			renderCell: function(data, row, key, el){
				return optTypeAkta[row];
			},
			target : [0]
		},{
			renderCell: function(data, row, key, el){
				return defaultDate(row);
			},
			target : [3,7]
		},{
			renderCell: function(data, row, key, el){
				return '<a href="'+base_url+'assets/lampiran/'+data[key].key+'/'+row+'" target="_blank"><span><i class="fa fa-download"></i>&nbsp;Lampiran</span></a>';
			},
			target : [4,8,9]
		},{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=editButton(site_url+"akta/edit/"+data[10].value, data[10].value);
				html +=deleteButton(site_url+"akta/remove/"+data[10].value, data[10].value);
				
				return html;
			},
			target : [10]
		}],
		
		additionFeature: function(el){
			el.append(insertButton(site_url+"akta/insert"));
		},
		finish: function(){
			var edit = $('.buttonEdit').modal({
				render : function(el, data){
					_self = edit;

					data.onSuccess = function(){
						
						table.data('plugin_tableGenerator').fetchData();
						
					};
					data.isReset = false;
					
					$(el).form(data).data('form');

				}
			});

			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					_self = edit;
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){

						$(del).data('modal').close();

						table.data('plugin_tableGenerator').fetchData();
						
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});
		},

		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});


</script>
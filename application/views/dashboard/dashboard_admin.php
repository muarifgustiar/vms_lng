<div class="mg-lg-12">
	<!-- <div class="block"> -->
		<div class="blockMenuWrapper">
			<?php if($session['id_role']=='1'){?>
			<div class="mg-lg-3 blockMenu blockMenuOrange">
				<a href="<?php echo site_url('master/workHour')?>">
					<i class="fa fa-clock-o"></i>
					<div class="caption">Jam Kerja</div>
				</a>
			</div>
			<div class="mg-lg-3 blockMenu blockMenuWistful">
				<a href="<?php echo site_url('master/calendar')?>">
					<i class="fa fa-calendar"></i>
					<div class="caption">Kalendar Kerja</div>
				</a>
			</div>
			<div class="mg-lg-3 blockMenu blockMenuBluewood">
				<a href="<?php echo site_url('master/shift')?>">
					<i class="fa fa-exchange"></i>
					<div class="caption">Pengaturan Shift</div>
				</a>
			</div>
			<div class="mg-lg-3 blockMenu blockMenuMeadow">
				<a href="<?php echo site_url('master/temporary')?>">
					<i class="fa fa-database"></i>
					<div class="caption">Data Temporary</div>
				</a>
			</div>
			<div class="mg-lg-3 blockMenu blockMenuRazmatazz">
				<a href="<?php echo site_url('master/formula')?>">
					<i class="fa fa-gears"></i>
					<div class="caption">Formula</div>
				</a>
			</div>
			<?php }

			if($session['id_role']=='1'||$session['id_role']=='2'){ ?>
			<div class="mg-lg-3 blockMenu blockMenuButtercup">
				<a href="<?php echo site_url('payroll')?>">
					<i class="fa fa-money"></i>
					<div class="caption">Payroll</div>
				</a>
			</div>
			<?php } ?>
		</div>
	<!-- </div> -->
</div>
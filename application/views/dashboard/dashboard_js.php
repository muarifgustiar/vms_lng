<script type="text/javascript">
$(function(){
	dataPost = {};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('dashboard/get_absensi'); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "id_pegawai",
				"value"	: "ID Pegawai"
			},{
				"key"	: "time_in",
				"value"	: "Waktu Masuk"
			},{
				"key"	: "time_out",
				"value"	: "Waktu Keluar"
			},{
				"key"	: "status",
				"value"	: "Status"
			}
		],
		optHeaders: [{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key){

				html = editButton(site_url+"edit/"+data[4].value, data[4].value);
				html +=deleteButton(site_url+"delete/"+data[4].value, data[4].value);
				return html;
			},
			target : 4
		}],
		additionFeature: function(el){
			el.empty();
			el.append(insertButton(site_url+"master/insert"));
		}
	}).dataFilter({
		init: function(el){
			dataPost.filter = null
		},
		targetButton : '.tableAddition',
		callback : function(el){
			$(el).data('plugin_tableGenerator').fetchData();
		}
	})
});
</script>
<script type="text/javascript">

$(function(){
	dataPost = {
		order: 'id',
		sort: 'desc'
	};
	var _xhr;
	$.ajax({
					url: '<?php echo site_url('agen/formFilter')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})
	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('agen/getData'); ?>',
		data: dataPost,
		
		headers: [
			{
				"key"	: "no",
				"value"	: "No"
			},{
				"key"	: "principal",
				"value"	: "Principal"
			},{
				"key"	: "issue_date",
				"value"	: "Tanggal"
			},{
				"key"	: "type",
				"value"	: "Tipe",
			},{
				"key"	: "expiry_date",
				"value"	: "Masa Berlaku"
			},{
				"key"	: "agen_file",
				"value"	: "Lampiran Agen"
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		init:function(){
			$('#tabs').tabs();
		},
		columnDefs : [{
			renderCell: function(data, row, key, el){
				return defaultDate(row);
			},
			target : [2,4]
		},{
			renderCell: function(data, row, key, el){
				return '<a href="'+base_url+'assets/lampiran/'+data[key].key+'/'+row+'" target="_blank"><span><i class="fa fa-download"></i>&nbsp;Lampiran</span></a>';
			},
			target : [5]
		},{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=editButton(site_url+"agen/edit/"+data[6].value, data[6].value);
				html +=deleteButton(site_url+"agen/remove/"+data[6].value, data[6].value);
				
				return html;
			},
			target : [6]
		}],
		
		additionFeature: function(el){
			el.append(insertButton(site_url+"agen/insert"));
		},
		finish: function(){
			var edit = $('.buttonEdit').modal({
				render : function(el, data){
					_self = edit;

					data.onSuccess = function(){
						
						table.data('plugin_tableGenerator').fetchData();
						
					};
					data.isReset = false;
					
					$(el).form(data).data('form');

				}
			});

			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					_self = edit;
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){

						$(del).data('modal').close();

						table.data('plugin_tableGenerator').fetchData();
						
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});
		},

		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});


</script>
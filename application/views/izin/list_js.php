<script type="text/javascript">

$(function(){
	dataPost = {
		order: 'id',
		sort: 'desc'
	};
	
	var _xhr;
	$.ajax({
					url: '<?php echo site_url('izin/formFilter')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})
	var __dptType;
	$.ajax({
		url: '<?php echo site_url('izin/getDPTType')?>',
		async: false,
		dataType: 'json',
		success:function(xhr){
			__dptType = xhr;
		}
	})
	var __iuType = {
		'siup':'SIUP',
		'siujk': 'SIUJK',
		'ijin_lain':'Surat Ijin Usaha Lainnya',
		'sbu':'SBU',
		'asosiasi':'Sertifikat Asosiasi/Lainnya'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('izin/getData'); ?>',
		data: dataPost,
		
		headers: [
			{
				"key"	: "id_dpt_type",
				"value"	: "Klasifikasi Penyedia Barang & Jasa"
			},{
				"key"	: "type",
				"value"	: "Tipe"
			},{
				"key"	: "no",
				"value"	: "No"
			},{
				"key"	: "issue_date",
				"value"	: "Tanggal Penerbitan"
			},{
				"key"	: "grade",
				"value"	: "Grade"
			},{
				"key"	: "qualification",
				"value"	: "Kualifikasi"
			},{
				"key"	: "authorize_by",
				"value"	: "Lembaga Pengesah"
			},{
				"key"	: "expire_date",
				"value"	: "Masa Berlaku"
			},{
				"key"	: "izin_file",
				"value"	: "Lampiran Izin"
			},{
				"key"	: "bsb",
				"value"	: "Bidang / Sub-bidang",
				"sort"	: false
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		init:function(){
			$('#tabs').tabs();
		},
		columnDefs : [{
			renderCell: function(data, row, key, el){
				return __dptType[row];
			},
			target : [0]
		},{
			renderCell: function(data, row, key, el){
				return __iuType[row];
			},
			target : [1]
		},{
			renderCell: function(data, row, key, el){
				return defaultDate(row);
			},
			target : [3,7]
		},{
			renderCell: function(data, row, key, el){
				return '<a href="'+base_url+'assets/lampiran/'+data[key].key+'/'+row+'" target="_blank"><span><i class="fa fa-download"></i>&nbsp;Lampiran</span></a>';
			},
			target : [8]
		},{
			renderCell: function(data, row, key, el){
				var __bsb;
				var __html;
				$.ajax({
					url: '<?php echo site_url('izin/getBSB')?>',
					async: false,
					data:{
						id_ijin_usaha : data[9].value
					},
					method: 'POST',
					dataType: 'json',
					success:function(xhr){

						__bsb = xhr;
						console.log(__bsb);
						__html = '<ul>';
						$.each(__bsb, function(key, value){
							__html += '<li>'+key;
							__html += '<ul>';
								$.each(value, function(keyBSB, valueBSB){
									__html += '<li>'+valueBSB+'</li>';
								});
							__html += '</ul>';
							__html += '</li>';
						});
						__html += '<ul>';
					}
				})
				return __html;
			},
			target : [9]
		},{
			renderCell: function(data, row, key, el){
				var html = '';
				html = '<a href="'+site_url+"izin/editBSB/"+data[9].value+'" class="btn btn-default editBSB"><i class="fa fa-list-alt"></i>&nbsp;Bidang / Sub-bidang</a>';
				html +=editButton(site_url+"izin/edit/"+data[9].value, data[9].value);
				html +=deleteButton(site_url+"izin/remove/"+data[9].value, data[9].value);
				
				return html;
			},
			target : [10]
		}],
		
		additionFeature: function(el){
			el.append(insertButton(site_url+"izin/insert"));
		},
		finish: function(){
			var editBSB = $('.editBSB').modal({
				header: 'Bidang / Sub-bidang',
				render : function(el, data){
					$(el).form(data).data('form');
       				$('form',el).prepend('<fieldset class="form-group treeWrapper blockWrapper"><div class="treeView" data-role="treeview"></div></fieldset>');
					$('.treeView', el).append('<ul></ul>');
					$.each(data.tree, function(key, value){
						$('.treeView > ul', el).append('<li class="treeDropdown closeTree"><span>'+value.label+'</span><ul id="head'+key+'"></ul></li>');
						$.each(value.data, function(keyTree, valueTree){
							var _checked = '';
							if(valueTree.value==1) _checked = 'checked';
							$('.treeView  #head'+key, el)
							.append('<li><label><input type="checkbox" name="bsb['+key+'][]" value="'+keyTree+'" '+_checked+'>&nbsp;'+valueTree.label+'</label></li>');
						})
						
					});
					$('.treeDropdown > span').on('click', function(){
						$(this).closest('.treeDropdown').toggleClass('closeTree');
					});
				}
			});
			var edit = $('.buttonEdit').modal({
				render : function(el, data){
					_self = edit;

					data.onSuccess = function(){
						
						table.data('plugin_tableGenerator').fetchData();
						
					};
					data.isReset = false;
					
					$(el).form(data).data('form');

				}
			});

			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					_self = edit;
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){

						$(del).data('modal').close();

						table.data('plugin_tableGenerator').fetchData();
						
					};
					data.isReset = true;
					
					$('.form', el).form(data).data('form');
				}
			});
		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
	var add = $('.buttonAdd').modal({
		render : function(el, data){
			var fw = $(el).formWizard({
				data: data,
				url: '<?php echo site_url('izin/insert_izin')?>',
				onNext: function(){
					_wizard = $(fw).data('formWizard');
					
					step = _wizard.options.data.step;
					var _step = [];
					var i = 1;
					$.each(step, function(key, value){
						_step[i] = key;
						i++;
					})
					_formWrapper = $('.wizard-content #step'+_wizard.options.currentPosition);
					_form = $('form',_wizard.options.wrapper);
					formData = new FormData( _form[0]);
					formData .append('validation', _step[_wizard.options.currentPosition]);

					$.ajax({
						url: '<?php echo site_url('izin/simpan')?>',
						data: formData,
						method : 'POST',
						processData: false,
						contentType: false,
						async: false,
						dataType: 'json',	
						success: function(xhr){
							_formWrapper.data('form').element = _formWrapper;
							

							if(xhr.status=='error') {
								_formWrapper.data('form').options.errorMessage = 'Terjadi Kesalahan';
							_formWrapper.data('form').generateError(xhr.form);
								_return = false;
							}else{
								_formWrapper.data('form').removeError(_formWrapper);
								_return = true;
							}
						}
					});


					

					return _return;
				},
				onSubmit: function(){

				},
				onSuccessSubmit: function(){
					table.data('plugin_tableGenerator').fetchData();
					location.reload();
				},
				onSuccess: function(){

					/*HIDE UNNECESSARY FIELD*/

					var iu = {
						1: ['siup', 'ijin_lain', 'asosiasi'],
						2: ['siup', 'ijin_lain', 'asosiasi'],
						3: ['siup', 'ijin_lain', 'asosiasi'],
						4: ['siujk', 'sbu', 'asosiasi'],
						5: ['siujk', 'sbu', 'asosiasi']
					};
					$('input.form-control[name="id_dpt_type"]').on('change', function(){
						var dpt_type = $(this).val();
						$('input.form-control[name="type"]').prop('checked',false);
						$('input.form-control[name="type"]').closest('.radioList').hide();
						
						$.each(iu[dpt_type], function(key, value){

							$('input.form-control[name="type"][value="'+value+'"]').closest('.radioList').show();
						})
					})
					
					_formField = {
						'siup': ['no', 'issue_date','qualification','expiry_date','izin_file'],
						'sbu': ['authorize_by','no', 'issue_date','expiry_date','izin_file'],
						'asosiasi': ['authorize_by','no', 'issue_date','expiry_date','izin_file'],
						'ijin_lain': ['authorize_by','no', 'issue_date','qualification','expiry_date','izin_file'],
						'siujk': ['no', 'grade','issue_date','qualification','expiry_date','izin_file']
					}
					$('input.form-control[name="type"]').on('change', function(){
						var iuType = $(this).val();
						if(iuType=='asosiasi'){
							$('.form-control[name="authorize_by"]').siblings('label').text('Anggota Asosiasi*');
						}else{
							$('.form-control[name="authorize_by"]').siblings('label').text('Lembaga Penerbit*');
						}

						$('#step3 .form-group').not('.btn-group').hide();
						$.each(_formField[iuType], function(key, value){
							$('input.form-control[name="'+value+'"]').closest('.form-group').show();
						});

					});

					$('input.form-control[name="id_dpt_type"]').on('change', function(){
						__id_dpt_type = $(this);
						var dpt_type = $(this).val();
						$('input.form-control[name="type"]').prop('checked',false);
						$('input.form-control[name="type"]').closest('.radioList').hide();
						
						$.each(iu[dpt_type], function(key, value){

							$('input.form-control[name="type"][value="'+value+'"]').closest('.radioList').show();
						})
						_wizard = $(fw).data('formWizard');
						$.ajax({
							url: '<?php echo site_url('izin/getBidangSubBidang')?>',
							data: {
								id_dpt_type : dpt_type
							},
							method : 'POST',
							async: false,
							dataType: 'json',	
							success: function(xhr){
								$('.treeWrapper').remove();
								$('.wizard-content #step4 .form.blockWrapper',_wizard.options.wrapper).prepend('<fieldset class="form-group treeWrapper"><div class="treeView" data-role="treeview"></div></fieldset>');
								$('.wizard-content #step4 .treeView').append('<ul></ul>')
								$.each(xhr, function(key, value){
									$('.wizard-content #step4 .treeView > ul').append('<li class="treeDropdown closeTree"><span>'+value.label+'</span><ul id="head'+key+'"></ul></li>');
									$.each(value.data, function(keyTree, valueTree){
										$('.wizard-content #step4 .treeView #head'+key)
										.append('<li><label><input type="checkbox" name="bsb['+key+'][]" value="'+keyTree+'">&nbsp;'+valueTree.label+'</label></li>');
									})
									
								});
								$('.treeDropdown > span').on('click', function(){
									$(this).closest('.treeDropdown').toggleClass('closeTree');
								});
							}
						})
					})
					

				}
			})
		}
	});
});


</script>
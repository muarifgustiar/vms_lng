<script type="text/javascript">

$(function(){
	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var _xhr;
	$.ajax({
					url: '<?php echo site_url('situ/formFilter')?>',
					async: false,
					dataType: 'json',
					success:function(xhr){
						_xhr = xhr;
					}
				})
	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('situ/getData'); ?>',
		data: dataPost,
		
		headers: [
			{
				"key"	: "type",
				"value"	: "Nama Surat"
			},{
				"key"	: "no",
				"value"	: "Nomor"
			},{
				"key"	: "address",
				"value"	: "Alamat"
			},{
				"key"	: "issue_date",
				"value"	: "Tanggal"
			},{
				"key"	: "expire_date",
				"value"	: "Masa Berlaku"
			},{
				"key"	: "situ_file",
				"value"	: "Lampiran SKDP",
				"sort"	: false
			},{
				"key"	: "authorize_by",
				"value"	: "Lembaga Penerbit"
			},{
				"key"	: "file_extension_situ",
				"value"	: "Bukti sedang dalam proses perpanjangan",
				"sort"	: false
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		init:function(){
			$('#tabs').tabs();
		},
		columnDefs : [{
			renderCell: function(data, row, key, el){
				return defaultDate(row);
			},
			target : [3,4]
		},{
			renderCell: function(data, row, key, el){
				return '<a href="'+base_url+'assets/lampiran/'+data[key].key+'/'+row+'" target="_blank"><span><i class="fa fa-download"></i>&nbsp;Lampiran</span></a>';
			},
			target : [5,7]
		},{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=editButton(site_url+"situ/edit/"+data[8].value, data[8].value);
				html +=deleteButton(site_url+"situ/remove/"+data[8].value, data[8].value);
				
				return html;
			},
			target : [8]
		}],
		
		additionFeature: function(el){
			el.append(insertButton(site_url+"situ/insert"));
		},
		finish: function(){
			var edit = $('.buttonEdit').modal({
				render : function(el, data){
					_self = edit;

					data.onSuccess = function(){
						
						table.data('plugin_tableGenerator').fetchData();
						
					};
					data.isReset = false;
					
					$(el).form(data).data('form');

				}
			});

			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					_self = edit;
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){

						$(del).data('modal').close();

						table.data('plugin_tableGenerator').fetchData();
						
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});
		},

		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});


</script>
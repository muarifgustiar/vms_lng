<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pemilik extends VMS {

	public $form;
	public $modelAlias = 'pm';
	public $alias = 'ms_pengurus';
	public $module = 'Pemilik';
	public function __construct(){
		parent::__construct();		
		$this->load->model('Pemilik_model','pm');
		$this->load->model('Akta_model','am');		
		$user = $this->session->userdata('user');
		$this->form = array(
				'form' => array(
					array(
						'field'	=> 	'id_akta',
						'type'	=>	'dropdown',
						'label'	=>	'No Akta Penetapan',
						'rules' => 	'required',
						'source'=>	$this->am->getAktaOption('saham')
					),
					array(
						'field'	=> 	'name',
						'type'	=>	'text',
						'label'	=>	'Name',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'shares',
						'type'	=>	'text',
						'label'	=>	'Komposisi Saham (lembar)',
						'rules' => 	'required|integer',
					),
					array(
						'field'	=> 	'percentage',
						'type'	=>	'money',
						'label'	=>	'Komposisi Saham (Nominal)',
						'rules' => 	'required',
					)
					
				),
				'successAlert'=>'Berhasil mengubah data!',
				'filter'=>array(
					array(
						'field'	=> 	'id_akta',
						'type'	=>	'dropdown',
						'label'	=>	'No Akta Pengangkatan',
						'source'=>	$this->am->getAktaOption('saham')
					),
					array(
						'type'	=>	'text',
						'label'	=>	'Name',
						'field' =>  'name'
					)
				)
			);
		$this->insertUrl = site_url('pemilik/save/'.$user['id_user']);
		$this->updateUrl = 'pemilik/update';
		$this->deleteUrl = 'pemilik/delete/';
		$this->getData = $this->pm->getData($this->form);
		$this->form_validation->set_rules($this->form['form']);
	}

	public function index(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('pemilik'),
			'title' => 'Pemilik'
		));
		
		$this->header = 'Pemilik';
		$this->content = $this->load->view('pemilik/list',null, TRUE);
		$this->script = $this->load->view('pemilik/list_js', null, TRUE);
		parent::index();
	}

  	public function getApproval($id){
		$config['query']    = $this->pm->getData($this->form, $id);
        
        $return = $this->tablegenerator->initialize($config);
        echo json_encode($return);
	}
	
}

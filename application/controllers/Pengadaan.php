<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pengadaan extends MY_Controller {

	public $form;
	public $modelAlias = 'am';
	public $alias = 'ms_agen';
	public $module = 'Agen';
	public function __construct(){
		parent::__construct();		
		$this->load->model('Agen_model','am');	
		$user = $this->session->userdata('user');
		$this->form = array(
				'form' => array(
					array(
						'field'	=> 	'name',
						'type'	=>	'text',
						'label'	=>	'Nama Paket Pengadaan',
						'rules' => 	'required'
					),
					array(
						'type'	=>	'money',
						'label'	=>	'Nilai HPS',
						'field'	=>	'price_idr',
						'caption'=>	'(Dalam rupiah)',
					),
					array(
						'field'	=> 	'principal',
						'type'	=>	'text',
						'label'	=>	'Nama Principal',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'issue_date',
						'type'	=>	'date',
						'label'	=>	'Tanggal',
						'rules' => 	'required|forward_date',
					),array(
						'field'	=> 	'expiry_date',
						'type'	=>	'lifetimeDate',
						'label'	=>	'Masa Berlaku',
						'rules' => 	'required|backdate',
					),
					array(
						'field'	=> 	'agen_file',
						'type'	=>	'file',
						'label'	=>	'Lampiran',
						'rules' => 	'required',
						'upload_path'=>base_url('assets/lampiran/agen_file/'),
						'upload_url'=>site_url('agen/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
					)
				),
				'successAlert'=>'Berhasil mengubah data!',
				'filter'=>array(
					array(
						'field'	=> 	'type',
						'type'	=>	'dropdown',
						'label'	=>	'Pabrikan/Keagenan/Distributor',
						'source'=> 	array(
										'Pabrikan'=>'Pabrikan',
										'Agen Tunggal'=>'Agen Tunggal',
										'Distributor Tunggal'=>'Distributor Tunggal'
									)
					),
					array(
						'field'	=> 	'no',
						'type'	=>	'text',
						'label'	=>	'No.',
					),
					array(
						'field'	=> 	'principal',
						'type'	=>	'text',
						'label'	=>	'Nama Principal',
					),
					array(
						'field'	=> 	'issue_date',
						'type'	=>	'date',
						'label'	=>	'Tanggal'
					),
					array(
						'type'	=>	'date_range_lifetime',
						'label'	=>	'Masa Berlaku',
						'name'	=>	'expiry_date',
						'field'	=> 	array(
											'expiry_date_start',
											'expiry_date_end'
										)

					),
				)
			);
		$this->insertUrl = site_url('agen/save/'.$user['id_user']);
		$this->updateUrl = 'agen/update';
		$this->deleteUrl = 'agen/delete/';
		$this->getData = $this->am->getData($this->form);
		$this->form_validation->set_rules($this->form['form']);
	}

	public function index(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('agen'),
			'title' => 'Agen'
		));
		
		$this->header = 'Agen';
		$this->content = $this->load->view('agen/list',null, TRUE);
		$this->script = $this->load->view('agen/list_js', null, TRUE);
		parent::index();
	}	
	public function getApproval($id){
		$config['query']    = $this->am->getData($this->form, $id);
        
        $return = $this->tablegenerator->initialize($config);
        echo json_encode($return);
	}
}

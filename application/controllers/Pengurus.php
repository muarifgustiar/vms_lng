<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pengurus extends VMS {

	public $form;
	public $modelAlias = 'pm';
	public $alias = 'ms_pengurus';
	public $module = 'Pengurus';
	public function __construct(){
		parent::__construct();		
		$this->load->model('Pengurus_model','pm');	
		$this->load->model('Akta_model','am');	
		$user = $this->session->userdata('user');
		$this->form = array(
				'form' => array(
					array(
						'field'	=> 	'id_akta',
						'type'	=>	'dropdown',
						'label'	=>	'No Akta Pengangkatan',
						'rules' => 	'required',
						'source'=>	$this->am->getAktaOption('direksi')
					),
					array(
						'field'	=> 	'name',
						'type'	=>	'text',
						'label'	=>	'Name',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'position',
						'type'	=>	'dropdown',
						'label'	=>	'Posisi',
						'rules' => 	'required',
						'source'=>	array(
										'Direktur Utama'=>'Direktur Utama',
										'Direktur'=>'Direktur',
										'Komisaris'=>'Komisaris'
									)
					),
					array(
						'field'	=> 	'no',
						'type'	=>	'text',
						'label'	=>	'Nomor Identitas (KTP/Passport/KITAS)*',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'expire_date',
						'type'	=>	'lifetimeDate',
						'label'	=>	'Masa Berlaku',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'pengurus_file',
						'type'	=>	'file',
						'label'	=>	'Bukti scan Identitas',
						'rules' => 	'required',
						'upload_path'=>base_url('assets/lampiran/pengurus_file/'),
						'upload_url'=>site_url('pengurus/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
					),
				),
				'successAlert'=>'Berhasil mengubah data!',
				'filter'=>array(
					array(
						'field'	=> 	'id_akta',
						'type'	=>	'dropdown',
						'label'	=>	'No Akta Pengangkatan',
						'source'=>	$this->am->getAktaOption('direksi')
					),
					array(
						'type'	=>	'text',
						'label'	=>	'Name',
						'field' =>  'name'
					),
					array(
						'field'	=> 	'position',
						'type'	=>	'dropdown',
						'label'	=>	'Posisi',
						'source'=>	array(
										'Direktur Utama'=>'Direktur Utama',
										'Direktur'=>'Direktur',
										'Komisaris'=>'Komisaris'
									)
					),
					array(
						'field'	=> 	'no',
						'type'	=>	'text',
						'label'	=>	'Nomor Identitas (KTP/Passport/KITAS)*',
					),
					array(
						'type'	=>	'date_range_lifetime',
						'label'	=>	'Masa Berlaku',
						'name'	=>	'expire_date',
						'field'	=> 	array(
											'expire_date_start',
											'expire_date_end'
										)

					),
				)
			);
		$this->insertUrl = site_url('pengurus/save/'.$user['id_user']);
		$this->updateUrl = 'pengurus/update';
		$this->deleteUrl = 'pengurus/delete/';
		$this->getData = $this->pm->getData($this->form);
		$this->form_validation->set_rules($this->form['form']);
	}

	public function index(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('pengurus'),
			'title' => 'Pengurus'
		));
		
		$this->header = 'Pengurus';
		$this->content = $this->load->view('pengurus/list',null, TRUE);
		$this->script = $this->load->view('pengurus/list_js', null, TRUE);
		parent::index();
	}

  	public function getApproval($id){
		$config['query']    = $this->pm->getData($this->form, $id);
        
        $return = $this->tablegenerator->initialize($config);
        echo json_encode($return);
	}
	
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tdp extends VMS {

	public $form;
	public $modelAlias = 'tm';
	public $alias = 'ms_tdp';
	public $module = 'TDP';
	public function __construct(){
		parent::__construct();		
		$this->load->model('Tdp_model','tm');	
		$user = $this->session->userdata('user');
		$this->form = array(
				'form' => array(
					array(
						'field'	=> 	'no',
						'type'	=>	'text',
						'label'	=>	'Nomor',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'issue_date',
						'type'	=>	'date',
						'label'	=>	'Tanggal',
						'rules' => 	'required|forward_date',
					),
					array(
						'field'	=> 	'expire_date',
						'type'	=>	'lifetimeDate',
						'label'	=>	'Masa Berlaku',
						'rules' => 	'required|backdate',
					),
					array(
						'field'	=> 	'authorize_by',
						'type'	=>	'text',
						'label'	=>	'Lembaga Penerbit',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'tdp_file',
						'type'	=>	'file',
						'label'	=>	'Bukti scan dokumen TDP',
						'rules' => 	'required',
						'upload_path'=>base_url('assets/lampiran/tdp_file/'),
						'upload_url'=>site_url('tdp/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
					),
					
					array(
						'field'	=> 	'tdp_extension_file',
						'type'	=>	'file',
						'label'	=>	'Bukti sedang dalam proses pengurusan',
						'rules' => 	'',
						'upload_path'=>base_url('assets/lampiran/tdp_extension_file/'),
						'upload_url'=>site_url('tdp/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
					),
				),
				'successAlert'=>'Berhasil mengubah data!',
				'filter'=>array(
					array(
						'type'	=>	'text',
						'label'	=>	'Nomor',
						'field' =>  'no'
					),
					array(
						'type'	=>	'text',
						'label'	=>	'Lembaga Penerbit',
						'field' =>  'authorize_by'
					),
					array(
						'type'	=>	'date_range',
						'label'	=>	'Tanggal',
						'name'	=>	'issue_date',
						'field'	=> 	array(
											'issue_date_start',
											'issue_date_end'
										)

					),
					array(
						'type'	=>	'date_range_lifetime',
						'label'	=>	'Masa Berlaku',
						'name'	=>	'expire_date',
						'field'	=> 	array(
											'expire_date_start',
											'expire_date_end'
										)

					),
				)
			);
		$this->insertUrl = site_url('tdp/save/'.$user['id_user']);
		$this->updateUrl = 'tdp/update';
		$this->deleteUrl = 'tdp/delete/';
		$this->getData = $this->tm->getData($this->form);
		$this->form_validation->set_rules($this->form['form']);
	}

	public function index(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('tdp'),
			'title' => 'TDP'
		));
		
		$this->header = 'TDP';
		$this->content = $this->load->view('tdp/list',null, TRUE);
		$this->script = $this->load->view('tdp/list_js', null, TRUE);
		parent::index();
	}

  	public function getApproval($id){
		$config['query']    = $this->tm->getData($this->form, $id);
        
        $return = $this->tablegenerator->initialize($config);
        echo json_encode($return);
	}
	
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Situ extends VMS {

	public $form;
	public $modelAlias = 'sm';
	public $alias = 'ms_situ';
	public $module = 'SKDP';
	public function __construct(){
		parent::__construct();		
		$this->load->model('Situ_model','sm');	
		$user = $this->session->userdata('user');
		$this->form = array(
				'form' => array(
					array(
						'field'	=> 	'type',
						'type'	=>	'dropdown',
						'label'	=>	'Nama Surat',
						'rules' => 	'required',
						'source'=> 	array(
										'Surat Keterangan Domisili Perusahaan (SKDP)'=>'Surat Keterangan Domisili Perusahaan (SKDP)',
										'Surat Izin Tempat Usaha (SITU)'=>'Surat Izin Tempat Usaha (SITU)',
										'Herregisterasi SKDP'=>'Herregisterasi SKDP'
									)
					),
					array(
						'field'	=> 	'no',
						'type'	=>	'text',
						'label'	=>	'Nomor',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'address',
						'type'	=>	'textarea',
						'label'	=>	'Alamat',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'issue_date',
						'type'	=>	'date',
						'label'	=>	'Tanggal',
						'rules' => 	'required|forward_date',
					),
					array(
						'field'	=> 	'issue_by',
						'type'	=>	'text',
						'label'	=>	'Lembaga Penerbit',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'expire_date',
						'type'	=>	'lifetimeDate',
						'label'	=>	'Masa Berlaku',
						'rules' => 	'required|backdate',
					),
					array(
						'field'	=> 	'situ_file',
						'type'	=>	'file',
						'label'	=>	'Bukti scan dokumen SKDP',
						'rules' => 	'required',
						'upload_path'=>base_url('assets/lampiran/situ_file/'),
						'upload_url'=>site_url('situ/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
					),
					
					array(
						'field'	=> 	'situ_extension_file',
						'type'	=>	'file',
						'label'	=>	'Bukti sedang dalam proses pengurusan',
						'rules' => 	'',
						'upload_path'=>base_url('assets/lampiran/akta_extension_file/'),
						'upload_url'=>site_url('situ/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
					),
				),
				'successAlert'=>'Berhasil mengubah data!',
				'filter'=>array(
					array(
						'type'	=>	'dropdown',
						'label'	=>	'Nama Surat',
						'field' =>	'type',
						'source'=> 	array(
										'Surat Keterangan Domisili Perusahaan (SKDP)'=>'Surat Keterangan Domisili Perusahaan (SKDP)',
										'Surat Izin Tempat Usaha (SITU)'=>'Surat Izin Tempat Usaha (SITU)',
										'Herregisterasi SKDP'=>'Herregisterasi SKDP'
									)
					),
					array(
						'type'	=>	'text',
						'label'	=>	'Nomor',
						'field' =>  'no'
					),
					array(
						'type'	=>	'text',
						'label'	=>	'Lembaga Penerbit',
						'field' =>  'issue_by'
					),
					array(
						'type'	=>	'date_range',
						'label'	=>	'Tanggal',
						'name'	=>	'issue_date',
						'field'	=> 	array(
											'issue_date_start',
											'issue_date_end'
										)

					),
					array(
						'type'	=>	'date_range_lifetime',
						'label'	=>	'Masa Berlaku',
						'name'	=>	'expire_date',
						'field'	=> 	array(
											'expire_date_start',
											'expire_date_end'
										)

					),
				)
			);
		$this->insertUrl = site_url('situ/save/'.$user['id_user']);
		$this->updateUrl = 'situ/update';
		$this->deleteUrl = 'situ/delete/';
		$this->getData = $this->sm->getData($this->form);
		$this->form_validation->set_rules($this->form['form']);
	}

	public function index(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('situ'),
			'title' => 'SKDP'
		));
		
		$this->header = 'SKDP';
		$this->content = $this->load->view('situ/list',null, TRUE);
		$this->script = $this->load->view('situ/list_js', null, TRUE);
		parent::index();
	}

	public function getApproval($id){
		$config['query']    = $this->sm->getData($this->form, $id);
        
        $return = $this->tablegenerator->initialize($config);
        echo json_encode($return);
	}
	
}

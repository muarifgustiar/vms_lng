<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct(){
		parent::__construct();

	}

	public function index()
	{
		$user = $this->session->userdata('user');
        $admin = $this->session->userdata('admin');
        $data['session'] = (($user) ? $user : $admin);
		$this->header = 'Selamat Datang '.(($user) ? $user['name'] : $admin['name']);

		// if($admin){
		// 	$this->content = $this->load->view('dashboard/dashboard_admin',$data, TRUE);
		// }
		// if($user){
			
		// 	$this->content = $this->load->view('dashboard/dashboard_user',$data, TRUE);
		// }
		// $this->script = $this->load->view('dashboard/dashboard_js', null, TRUE);

		parent::index();
	}
	
}

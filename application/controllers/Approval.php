<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Approval extends MY_Controller {
	public $form;
	public $modelAlias = 'am';
	public function __construct(){
		parent::__construct();		
		$this->load->library('Data_process');
	}

	public function approval($id){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('administrasi'),
			'title' => 'Approval'
		));
		$array['id'] = $id;
		$this->header = 'Approval';
		$this->content = $this->load->view('approval/view',$array, TRUE);
		$this->script = $this->load->view('approval/view_js', $array, TRUE);
		parent::index();
	}
	
	public function administrasi($id, $process=false){
		if($process==true){
			$data = $this->vm->get_data($id);
			$result = $this->data_process->check($id, $this->input->post(),$data['id'],'ms_vendor_admistrasi');
			
		}else{
			$data = $this->vm->get_data($id);
			$this->load->view('approval/administrasi', $data, FALSE);
		}
		
	}
	public function akta($id, $process=false){
		if($process==true){
			$data = $this->vm->get_data($id);
			foreach($this->input->post('akta') as $key => $value){
				$this->data_process->check($id, $value,$key,'ms_akta');
			}

		}else{
			$data['id_vendor'] = $id;
			$this->load->view('approval/akta', $data, FALSE);
		}
	}

	public function situ($id, $process=false){
		if($process==true){
			$data = $this->vm->get_data($id);
			foreach($this->input->post('situ') as $key => $value){
				$this->data_process->check($id, $value,$key,'ms_situ');
			}

		}else{
			$data['id_vendor'] = $id;
			$this->load->view('approval/situ', $data, FALSE);
		}
	}
	public function tdp($id, $process=false){
		if($process==true){
			$data = $this->vm->get_data($id);
			foreach($this->input->post('tdp') as $key => $value){
				$this->data_process->check($id, $value,$key,'ms_tdp');
			}

		}else{
			$data['id_vendor'] = $id;
			$this->load->view('approval/tdp', $data, FALSE);
		}
	}
	public function pengurus($id, $process=false){
		if($process==true){
			$data = $this->vm->get_data($id);
			foreach($this->input->post('pengurus') as $key => $value){
				$this->data_process->check($id, $value,$key,'ms_pengurus');
			}

		}else{
			$data['id_vendor'] = $id;
			$this->load->view('approval/pengurus', $data, FALSE);
		}
	}
	public function pemilik($id, $process=false){
		if($process==true){
			$data = $this->vm->get_data($id);
			foreach($this->input->post('pemilik') as $key => $value){
				$this->data_process->check($id, $value,$key,'ms_pemilik');
			}

		}else{
			$data['id_vendor'] = $id;
			$this->load->view('approval/pemilik', $data, FALSE);
		}
	}
	public function izin($id, $process=false){
		if($process==true){
			$data = $this->vm->get_data($id);
			foreach($this->input->post('izin') as $key => $value){
				$this->data_process->check($id, $value,$key,'ms_ijin_usaha');
			}

		}else{
			$data['id_vendor'] = $id;
			$this->load->view('approval/izin', $data, FALSE);
		}
	}
	public function agen($id, $process=false){
		if($process==true){
			$data = $this->vm->get_data($id);
			foreach($this->input->post('agen') as $key => $value){
				$this->data_process->check($id, $value,$key,'ms_agen');
			}

		}else{
			$data['id_vendor'] = $id;
			$this->load->view('approval/agen', $data, FALSE);
		}
	}
	public function pengalaman($id, $process=false){
		if($process==true){
			$data = $this->vm->get_data($id);
			foreach($this->input->post('pengalaman') as $key => $value){
				$this->data_process->check($id, $value,$key,'ms_pengalaman');
			}

		}else{
			$data['id_vendor'] = $id;
			$this->load->view('approval/pengalaman', $data, FALSE);
		}
	}

	public function verifikasi($id){
		$this->load->model('approval_model','am');
		$data = $this->vm->get_vendor_name($id);
		$data['id_vendor'] = $id;
		$data['approval_data'] = $this->am->get_total_data($id);
		$data['spv']	= $this->am->get_spv_mail(8);
		
		$data['graphBar'] = array(
									0=>
										array('val'=>(count($data['approval_data'][0]))/$data['approval_data']['total']*100,
											'color'=>'#f39c12'
										),
									1=>
										array('val'=>(count($data['approval_data'][1])+count($data['approval_data'][2]))/$data['approval_data']['total']*100,
											'color'=>'#2cc36b'
										),
									3=>array('val'=>(count($data['approval_data'][3])+count($data['approval_data'][4]))/$data['approval_data']['total']*100,
											'color'=>'#c0392b'
										),
								);
		$this->content = $this->load->view('approval/verifikasi',$data, TRUE);
		$this->script = $this->load->view('approval/verifikasi_js', $data, TRUE);
		parent::index();
		

		// $email['dpt'] 		= $this->vm->get_dpt_mail($id);
		// $email['msg'] 		= 
		// 				$email['dpt']['legal_name'].". ".$email['dpt']['name']." telah masuk kedalam daftar tunggu di Vendor Management System PT PGN LNG Indonesia.<br/>
		// 				Untuk selanjutnya, silahkan memeriksa kembali kelengkapan data - data calon DPT di aplikasi.<br/><br/>
		// 				Terima kasih.<br/>
		// 				PT PGN LNG Indonesia'";
		// $email['subject']	= "Daftar Tunggu DPT - Vendor Management System PT PGN LNG Indonesia";


		// if($this->input->post('simpan')){
			
		// 	#email SPV
		// 	foreach ($data['spv'] as $keyspv => $valuespv) {
		// 		$this->utility->mail($valuespv['email'],$email['msg'],$email['subject']);
		// 	}

		// 	if(count($data['approval_data'][0])>0||count($data['approval_data'][3])>0){
		// 		$this->session->set_flashdata('msgError','<p class="errorMsg">Tidak Bisa diangkat menjadi vendor. Ada beberapa data yang harus diperbaiki</p>');	
		// 	}else{
		// 		$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data terkirim untuk disetujui!</p>');	

		// 		$data['approval_data'] = $this->am->angkat_vendor($id);
				
		// 		redirect(site_url('approval/verification/'.$id));
		// 	}

		// }

		// $data['id_data'] = $id;
		// $layout['content'] =  $this->load->view('verifikasi',$data,TRUE);
		// $layout['script'] =  $this->load->view('verifikasi_js',$data,TRUE);
		// $item['header'] = $this->load->view('admin/header',$this->session->userdata('admin'),TRUE);
		
		// $this->load->model('approval_model','appm');
		
		// $layout['approval_data'] = $this->appm->get_total_data($id);
		// $item['content'] = $this->load->view('admin/dashboard',$layout,TRUE);
		
		// $this->load->view('approval/verifikasi',$item);
	}

	public function approve($id){
		$this->load->model('approval_model','am');
		$this->load->model('vendor/vendor_model','vm');
		
		// $res = $this->am->approve($id);
		$email['dpt'] 		= $this->vm->get_dpt_mail($id);
		$email['adm'] 		= $this->vm->get_adm_mail($this->session->userdata('admin')['id_role']);
		$email['msg'] 		= "
								Kepada <i>".$email['dpt']['legal_name'].". ".$email['dpt']['name'].".</i>
									<br/><br/>
								Dengan ini kami sampaikan bahwa perusahaan saudara telah diangkat menjadi DPT(Daftar Penyedia Barang/Jasa Terdaftar) di PT PGN LNG Indonesia.
									<br/><br/><br/>
								Terima kasih,
									<br/>
								PT PGN LNG Indonesia.";
		$email['subject']	= "Pemberitahuan Pengangkatan DPT - Sistem Aplikasi Kelogistikan PT PGN LNG Indonesia";
		$email['to'] = array();
		$email['to'][] = $email['dpt']['email'];
		foreach ($email['adm'] as $keyadm => $valueadm) {
			$email['to'][] = $valueadm['email'];
		}
		
		$this->am->approve($id);
		foreach ($email['to'] as $keyto => $valueto) {
			$this->utility->mail($valueto, $email['msg'], $email['subject']);
		}
		
		$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Vendor telah diangkat menjadi DPT!</p>');	
		redirect('admin/admin_vendor/waiting_list');
	}


}

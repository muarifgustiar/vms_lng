<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Administrasi extends VMS {
	public $form;
	public $modelAlias = 'vm';
	public function __construct(){
		parent::__construct();		
		$this->load->model('Vendor_model','vm');	
		$this->form = array(
				'form' => array(
					array(
						'field'	=> 	'id_legal',
						'type'	=>	'dropdown',
						'label'	=>	'Jenis Badan Usaha',
						'rules' => 	'required',
						'source'=> 	$this->vm->get_legal()
					),
					array(
						'field'	=> 	'name',
						'type'	=>	'text',
						'label'	=>	'Nama Badan Usaha',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'npwp_code',
						'type'	=>	'npwp',
						'label'	=>	'NPWP',
						'rules' => 	'valid_npwp|required',
					),
					array(
						'field'	=> 	'npwp_file',
						'type'	=>	'file',
						'label'	=>	'Lampiran NPWP',
						'rules' => 	'required',
						// 'rules' => 	'callback_do_upload[npwp_file]',
						'upload_path'=>base_url('assets/lampiran/npwp_file/'),
						'upload_url'=>site_url('administrasi/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
					),
					array(
						'field'	=> 	'nppkp_code',
						'type'	=>	'text',
						'label'	=>	'NPPKP',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'nppkp_file',
						'type'	=>	'file',
						'label'	=>	'Lampiran NPPKP',
						'rules' => 	'required',
						// 'rules' => 	'callback_do_upload[nppkp_file]',
						'upload_path'=>base_url('assets/lampiran/nppkp_file/'),
						'upload_url'=>site_url('administrasi/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
					),
					array(
						'field'	=> 	'vendor_office_status',
						'type'	=>	'radio',
						'label'	=>	'Status Vendor',
						'rules' => 	'required',
						'source' =>	array(
								'pusat'=>'Pusat',
								'cabang'=>'Cabang'
								)
					),
					array(
						'field'	=> 	'vendor_address',
						'type'	=>	'text',
						'label'	=>	'Alamat',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'vendor_country',
						'type'	=>	'text',
						'label'	=>	'Negara',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'vendor_province',
						'type'	=>	'text',
						'label'	=>	'Provinsi',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'vendor_city',
						'type'	=>	'text',
						'label'	=>	'Kota',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'vendor_postal',
						'type'	=>	'text',
						'label'	=>	'Kode Pos',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'vendor_fax',
						'type'	=>	'text',
						'label'	=>	'Fax',
						'rules' => 	'',
					),
					array(
						'field'	=> 	'vendor_phone',
						'type'	=>	'text',
						'label'	=>	'Telephone',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'vendor_email',
						'type'	=>	'text',
						'label'	=>	'Email',
						'rules' => 	'valid_email|required',
					),
					array(
						'field'	=> 	'vendor_website',
						'type'	=>	'text',
						'label'	=>	'Website',
						'rules' => 	'',
					)
				),
				'successAlert'=>'Berhasil mengubah data!'
			);
		$this->form_validation->set_rules($this->form['form']);
		$this->updateUrl = 'administrasi/update';
	}

	public function index(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('administrasi'),
			'title' => 'Administrasi'
		));
		
		$this->header = 'Administrasi';
		$this->content = $this->load->view('administrasi/view',null, TRUE);
		$this->script = $this->load->view('administrasi/view_js', null, TRUE);
		parent::index();
	}
	public function getData(){
		$getData = $this->vm->selectAdministrasiData();
		foreach($this->form['form'] as $key => $value){
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $getData[$value['field']];
		}
		echo json_encode($this->form);
	}
	public function ubah(){
		$modelAlias = $this->modelAlias;
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('administrasi'),
			'title' => 'Administrasi'
		));

		$this->breadcrumb->addlevel(2, array(
			'url' => site_url('administrasi/ubah'),
			'title' => 'Ubah Administrasi'
		));

		$this->header = 'Ubah Administrasi';
		$this->content = $this->load->view('administrasi/ubah',null, TRUE);
		$this->script = $this->load->view('administrasi/ubah_js', null, TRUE);
		parent::index();
	}

	public function hapus($id){
		if($this->am->delete($id)){
			$this->dpt->non_iu_change($user['id_user']);
			$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Sukses menghapus data!</p>');
			redirect(site_url('akta'));
		}else{
			$this->session->set_flashdata('msgSuccess','<p class="msgError">Gagal menghapus data!</p>');
			redirect(site_url('akta'));
		}
	}

	public function approval($id){
		$getData = $this->vm->selectAdministrasiData($id);
		foreach($this->form['form'] as $key => $value){
			$this->form['form'][$key]['readonly'] = TRUE;
			$this->form['form'][$key]['value'] = $getData[$value['field']];
		}
		echo json_encode($this->form);
	}
	
}

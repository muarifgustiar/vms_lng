<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Akta extends VMS {

	public $form;
	public $modelAlias = 'am';
	public $alias = 'ms_akta';
	public $module = 'Akta';
	public function __construct(){
		parent::__construct();		
		$this->load->model('Akta_model','am');	
		$user = $this->session->userdata('user');
		$this->form = array(
				'form' => array(
					array(
						'field'	=> 	'type',
						'type'	=>	'dropdown',
						'label'	=>	'Jenis Akta',
						'rules' => 	'required',
						'source'=> 	array(
										'pendirian'=>'Akta Pendirian',
										'perubahan'=>'Akta - Akta Perubahan Terakhir (mengenai anggaran dasar)',
										'direksi'=>'Akta - Akta Perubahan Terakhir (mengenai susunan terakhir direksi dan komisaris)',
										'saham'=>'Akta - Akta Perubahan Terakhir (mengenai susunan terakhir pemegang saham)'
									)
					),
					array(
						'field'	=> 	'no',
						'type'	=>	'text',
						'label'	=>	'No Akta',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'notaris',
						'type'	=>	'text',
						'label'	=>	'Notaris',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'issue_date',
						'type'	=>	'date',
						'label'	=>	'Tanggal Akta',
						'rules' => 	'required|forward_date',
					),
					array(
						'field'	=> 	'akta_file',
						'type'	=>	'file',
						'label'	=>	'Lampiran Akta',
						'rules' => 	'required',
						'upload_path'=>base_url('assets/lampiran/akta_file/'),
						'upload_url'=>site_url('akta/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
					),
					array(
						'field'	=> 	'authorize_by',
						'type'	=>	'text',
						'label'	=>	'Lembaga Pengesah',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'authorize_date',
						'type'	=>	'date',
						'label'	=>	'Tanggal Pengesahan',
						'rules' => 	'required|forward_date',
					),
					array(
						'field'	=> 	'authorize_no',
						'type'	=>	'text',
						'label'	=>	'No Pengesahan',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'authorize_file',
						'type'	=>	'file',
						'label'	=>	'Bukti Pengesahan Akta',
						'rules' => 	'required',
						'upload_path'=>base_url('assets/lampiran/authorize_file/'),
						'upload_url'=>site_url('akta/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
					),
					array(
						'field'	=> 	'akta_extension_file',
						'type'	=>	'file',
						'label'	=>	'Bukti sedang dalam proses pengurusan',
						'rules' => 	'',
						'upload_path'=>base_url('assets/lampiran/akta_extension_file/'),
						'upload_url'=>site_url('akta/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
					),
				),
				'successAlert'=>'Berhasil mengubah data!',
				'filter'=>array(
					array(
						'type'	=>	'dropdown',
						'label'	=>	'Jenis Akta',
						'field' =>	'type',
						'source'=> 	array(
										'pendirian'=>'Akta Pendirian',
										'perubahan'=>'Akta - Akta Perubahan Terakhir (mengenai anggaran dasar)',
										'direksi'=>'Akta - Akta Perubahan Terakhir (mengenai susunan terakhir direksi dan komisaris)',
										'saham'=>'Akta - Akta Perubahan Terakhir (mengenai susunan terakhir pemegang saham)'
									)
					),
					array(
						'type'	=>	'text',
						'label'	=>	'No. Akta',
						'field' =>  'no'
					),
					array(
						'type'	=>	'text',
						'label'	=>	'Notaris',
						'field' =>  'notaris'
					),
					array(
						'type'	=>	'date_range',
						'label'	=>	'Tanggal Penerbitan Akta',
						'name'	=>	'issue_date',
						'field'	=> 	array(
											'issue_date_start',
											'issue_date_end'
										)

					),
					array(
						'type'	=>	'text',
						'label'	=>	'Lembaga Pengesah',
						'field' =>  'authorize_by'
					),
					array(
						'type'	=>	'text',
						'label'	=>	'No. Pengesahan',
						'field' =>  'authorize_no'
					),
					array(
						'type'	=>	'date_range',
						'label'	=>	'Tanggal Pengesahan',
						'name'	=>	'authorize_date',
						'field'	=> 	array(
											'authorize_date_start',
											'authorize_date_end'
										)

					)
				)
			);
		$this->insertUrl = site_url('akta/save/'.$user['id_user']);
		$this->updateUrl = 'akta/update';
		$this->deleteUrl = 'akta/delete/';
		$this->getData = $this->am->getData($this->form);
		$this->form_validation->set_rules($this->form['form']);
	}

	public function index(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('akta'),
			'title' => 'Akta'
		));
		
		$this->header = 'Akta';
		$this->content = $this->load->view('akta/list',null, TRUE);
		$this->script = $this->load->view('akta/list_js', null, TRUE);
		parent::index();
	}	

	public function getApproval($id){
		$config['query']    = $this->am->getData($this->form, $id);
        
        $return = $this->tablegenerator->initialize($config);
        echo json_encode($return);
	}
}

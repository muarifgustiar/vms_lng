<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Vendor extends MY_Controller {
	public $modelAlias = 'vm';
	public $form;
	public function __construct(){
		parent::__construct();
		
		$this->load->model('Vendor_model','vm');	
		$this->form = array(
				'form' => array(
					array(
						'field'	=> 	'id_legal',
						'type'	=>	'dropdown',
						'label'	=>	'Jenis Badan Usaha',
						'rules' => 	'required',
						'source'=> 	$this->vm->get_legal()
					),
					array(
						'field'	=> 	'name',
						'type'	=>	'text',
						'label'	=>	'Nama Badan Usaha',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'npwp_code',
						'type'	=>	'npwp',
						'label'	=>	'NPWP*',
						'rules' => 	'valid_npwp|required',
					),
					array(
						'field'	=> 	'email',
						'type'	=>	'text',
						'label'	=>	'Email**',
						'rules' => 	'valid_email|required|is_unique[ms_vendor_admistrasi.vendor_email]',
					)
				)
			);
		$this->insertUrl = site_url('vendor/save');
		$this->updateUrl = 'vendor/update';
		$this->deleteUrl = 'vendor/delete/';
		$this->form_validation->set_rules($this->form['form']);	
	}

	public function index(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor'),
			'title' => 'Vendor'
		));

		$this->header = 'Vendor';
		$this->content = $this->load->view('vendor/list',null, TRUE);
		$this->script = $this->load->view('vendor/list_js', null, TRUE);
		parent::index();
	}

	public function listData(){
		$config['query']	= $this->vm->daftar_vendor();
		$return	= $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function tambah(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor'),
			'title' => 'Vendor'
		));

		$this->breadcrumb->addlevel(2, array(
			'url' => site_url('vendor'),
			'title' => 'Daftar Calon Penyedia Barang & Jasa'
		));

		$this->header = 'Daftar Calon Penyedia Barang & Jasa';
		$this->content = $this->load->view('vendor/daftar_vendor',null, TRUE);
		$this->script = $this->load->view('vendor/daftar_vendor_js', null, TRUE);
		parent::index();
	}

	public function form_daftar(){
		$this->form['url'] = site_url('vendor/save');
		$this->form['button'] = array(
                array(
                    'type'=>'submit',
                    'label'=>'Simpan'
                )
            );
		echo json_encode($this->form);
	}	

	public function save($data=null){
		$modelAlias = $this->modelAlias;
        if($this->validation()){
            $save = $this->input->post();
            $vendor_data['vendor_status'] = 0;
            $vendor_data['npwp_code'] = $save['npwp_code'];
            $vendor_data['name'] = $save['name'];
            $vendor_data['is_active'] = 1;
            $vendor_data['ever_blacklisted'] = 0;
            $vendor_data['entry_stamp'] = timestamp();
            $vendor_data['del'] = 0;
            $id = $this->$modelAlias->insert_vendor($vendor_data);

            $vendor_administrasi_data['id_vendor'] = $id;
            // $vendor_administrasi_data['email'] = $save['email'];
            $vendor_administrasi_data['npwp_code'] = $save['npwp_code'];
            $vendor_administrasi_data['id_legal'] = $save['id_legal'];
            $vendor_administrasi_data['entry_stamp'] = timestamp();
            $vendor_administrasi_data['del'] = 0;
            $this->$modelAlias->insert_data_vendor('ms_vendor_admistrasi',$vendor_administrasi_data);

            $password = password_generator();
            $vendor_login['id_user'] = $id;
            $vendor_login['type'] = 'user';
            $vendor_login['username'] = $save['email'];
            $vendor_login['password'] = do_hash($password, 'sha1');
            $vendor_login['password_raw'] = $password;
            $vendor_login['entry_stamp'] = timestamp();
            $vendor_login['del'] = 0;
            $this->$modelAlias->insert_data_vendor('ms_login',$vendor_login);

            $message = 'Perusahaan saudara telah terdaftar kedalam Sistem Aplikasi Kelogistikan PT PGN LNG Indonesia.
					Berikut username &amp; password saudara : 
					
					Username : '.(isset($save['email'])?$save['email']:'').'
					Password : '.(isset($password)?$password:'').'
					
					Untuk selanjutnya, silahkan melengkapi data - data dan dokumen saudara di aplikasi.
					Terima kasih.
					PT PGN LNG Indonesia';
            email($save['email'], $message, 'Pendaftaran Vendor PT PGN LNG Indonesia');
        }
    }

	public function waiting_list(){
		$config['query']	= $this->vm->get_waiting_list();
		$return	= $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}


	public function dpt(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('dpt'),
			'title' => 'DPT'
		));

		$this->header = 'DPT';
		$this->content = $this->load->view('vendor/dpt',null, TRUE);
		$this->script = $this->load->view('vendor/dpt_js', null, TRUE);
		parent::index();
	}
	public function listDPT(){
		$config['query']	= $this->vm->get_dpt_list();
		$return	= $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}
}
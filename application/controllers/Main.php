<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('Main_model', 'mm');

	}
	public function index(){
		if($this->session->userdata('user')||$this->session->userdata('admin')){
			redirect(site_url('dashboard'));
		}else{
			$this->login();
		}
	}
	public function login(){
		if($this->session->userdata('user')){
			redirect(site_url('dashboard'));
		}
		$this->load->view('login');
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect(site_url());
	}

	public function check(){
		$return['status'] = 'error';
		$return['message']= 'Terjadi Kesalahan';

		$result = $this->mm->check($this->input->post());
		if($result){
			$return['status'] = 'success';
			$return['message']= 'Berhasil';
			$return['url'] = site_url('dashboard');

		}else{
			$return['status'] = 'error';
			$return['message']= 'Data tidak dikenal. Silahkan login kembali!';
		}
			

		echo json_encode($return);
	}
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Izin extends VMS {

	public $formWizard;
	public $modelAlias = 'im';
	public $alias = 'ms_ijin_usaha';
	public $module = 'Izin';
	public function __construct(){
		parent::__construct();		
		$this->load->model('Izin_model','im');	
		$user = $this->session->userdata('user');
		$this->formWizard = array(
			'step'=>array(
				'klasifikasi'=>array(
					'label'=>'Klasifikasi Usaha',
					'form'=>array(
								array(
									'field'	=> 	'id_dpt_type',
									'type'	=>	'radioList',
									'label'	=>	'Klasifikasi Usaha',
									'rules' => 	'required',
									'source'=>	array(
													1=>'Pengadaan Barang',
													2=>'Jasa Konsultan non-Konstruksi',
													3=>'Jasa Lainnya',
													4=>'Jasa Pekerjaan Konstruksi',
													5=>'Jasa Konsultan Perencana/Pengawas Konstruksi'
												)
								)
							),
					'button'=>array(
						array(
		                    'type'=>'next',
		                    'label'=>'Lanjut',
		                    'class'=>'btn-next'
		                )
					)

				),
				'siu'=>array(
					'label'=>'Surat Izin Usaha',
					'form'=>array(
								array(
									'field'	=> 	'type',
									'type'	=>	'radioList',
									'label'	=>	'Surat Ijin Usaha',
									'rules' => 	'required',
									'source'=>	array(
													'siujk'=>'SIUJK',
													'sbu'=>'SBU',
													'siup'=>'SIUP',
													'ijin_lain'=>'Surat Ijin Usaha Lainnya',
													'asosiasi'=>'Sertifikat Asosiasi/Lainnya'
												)
								)
								
							),
					'button'=>array(
						array(
		                    'type'=>'prev',
		                    'label'=>'Sebelumnya',
		                    'class'=>'btn-prev'
		                ),array(
		                    'type'=>'next',
		                    'label'=>'Lanjut',
		                    'class'=>'btn-next'
		                )
					)
				),
				'data'=>array(
					'label'=>'Pengisian Data',
					'form'=>array(
								array(
									'field'	=> 	'authorize_by',
									'type'	=>	'text',
									'label'	=>	($this->input->post('type')=='asosiasi') ? 'Anggota Asosiasi': 'Lembaga Penerbit',
									'rules' => 	'required',
								),array(
									'field'	=> 	'no',
									'type'	=>	'text',
									'label'	=>	'Nomor',
									'rules' => 	'required',
								),
								array(
									'field'	=> 	'grade',
									'type'	=>	'dropdown',
									'label'	=>	'Grade',
									'rules' => 	'required',
									'source'=>	array(
													'a'=>'A',
													'b'=>'B',
													'c'=>'C'
												)
								),
								array(
									'field'	=> 	'issue_date',
									'type'	=>	'date',
									'label'	=>	'Tanggal',
									'rules' => 	'required|forward_date',
								),
								array(
									'field'	=> 	'qualification',
									'type'	=>	'radio',
									'label'	=>	'Kualifikasi',
									'rules' => 	'required',
									'source'=>	array(
													'kecil'=>'Kecil',
													'menengah'=>'Menengah',
													'besar'=>'Besar'
												)
								),
								array(
									'field'	=> 	'expiry_date',
									'type'	=>	'lifetimeDate',
									'label'	=>	'Masa Berlaku',
									'rules' => 	'required',
								),
								array(
									'field'	=> 	'izin_file',
									'type'	=>	'file',
									'label'	=>	'Lampiran',
									'rules' => 	'required',
									'upload_path'=>base_url('assets/lampiran/izin_file/'),
									'upload_url'=>site_url('izin/upload_lampiran'),
									'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
								),
							),
					'button'=>array(
						array(
		                    'type'=>'prev',
		                    'label'=>'Sebelumnya',
		                    'class'=>'btn-prev'
		                ),array(
		                    'type'=>'next',
		                    'label'=>'Lanjut',
		                    'class'=>'btn-next'
		                )
					)
				),
				'bsb'=>array(
					'label'=>'Bidang / Sub-Bidang',
					'form'=>array(
								array(
									'field'	=> 	'bsb',
									'label'	=>	'Bidang Sub-Bidang',
									'rules' => 	'callback_requiredTree'
								)
							),
					'button'=>array(
						array(
		                    'type'=>'prev',
		                    'label'=>'Sebelumnya',
		                    'class'=>'btn-prev'
		                ),array(
		                    'type'=>'next',
		                    'label'=>'Simpan',
		                     'class'=>'btn-next'
		                )
					)
				)
			),
			
		);
		$this->insertUrl = site_url('izin/save/'.$user['id_user']);
		$this->updateUrl = 'izin/update';
		$this->deleteUrl = 'izin/delete/';
		$this->getData = $this->im->getData($this->form);

	}

	public function index(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('izin'),
			'title' => 'Izin Usaha'
		));
		
		$this->header = 'Izin Usaha';
		$this->content = $this->load->view('izin/list',null, TRUE);
		$this->script = $this->load->view('izin/list_js', null, TRUE);
		parent::index();
	}
	public function insert(){
		echo json_encode($this->formWizard);
	}
	public function formFilter(){
        $return['button'] = array(
                array(
                    'type'=>'button',
                    'label'=>'Filter',
                    'class'=>'btn-filter'
                ),
                array(
                    'type'=>'reset',
                    'label'=>'Reset'
                )
            );
        $return['form'] = array(
			array(
				'field'	=> 	'id_dpt_type',
				'type'	=>	'dropdown',
				'label'	=>	'Klasifikasi Usaha',
				
				'source'=>	array(
								1=>'Pengadaan Barang',
								2=>'Jasa Konsultan non-Konstruksi',
								3=>'Jasa Lainnya',
								4=>'Jasa Pekerjaan Konstruksi',
								5=>'Jasa Konsultan Perencana/Pengawas Konstruksi'
							)
			),
			array(
				'field'	=> 	'type',
				'type'	=>	'dropdown',
				'label'	=>	'Surat Ijin Usaha',
				
				'source'=>	array(
								'siujk'=>'SIUJK',
								'sbu'=>'SBU',
								'siup'=>'SIUP',
								'ijin_lain'=>'Surat Ijin Usaha Lainnya',
								'asosiasi'=>'Sertifikat Asosiasi/Lainnya'
							)
			),
			array(
				'field'	=> 	'authorize_by',
				'type'	=>	'text',
				'label'	=>	'Anggota Asosiasi/Lembaga Penerbit',
				
			),array(
				'field'	=> 	'no',
				'type'	=>	'text',
				'label'	=>	'Nomor',
				
			),
			array(
				'field'	=> 	'grade',
				'type'	=>	'dropdown',
				'label'	=>	'Grade',
				'source'=>	array(
								'a'=>'A',
								'b'=>'B',
								'c'=>'C'
							)
			),
			array(
				'name'	=> 	'issue_date',
				'type'	=>	'date_range',
				'label'	=>	'Tanggal',
				'field'	=> 	array(
											'issue_date_start',
											'issue_date_end'
										)
			),
			array(
				'field'	=> 	'qualification',
				'type'	=>	'dropdown',
				'label'	=>	'Kualifikasi',
				'source'=>	array(
								'kecil'=>'Kecil',
								'menengah'=>'Menengah',
								'besar'=>'Besar'
							)
			),
			array(
				'field'	=> 	'expiry_date',
				'type'	=>	'lifetimeDate',
				'label'	=>	'Masa Berlaku',
				
			)
		);
        echo json_encode($return);
    }
	public function simpan(){
		

		$__validation = $this->formWizard['step'][$_POST['validation']]['form'];
		if($_POST['validation']=='data'){
			$submitted = array();
			switch ($this->input->post('type')) {
				case 'siup':
					$submitted = array(1,3,4,5,6);
					break;
				
				case 'sbu':
					$submitted = array(0,1,3,5,6);
					break;
				case 'asosiasi':
					$submitted = array(0,1,3,5,6);
					break;
				case 'ijin_lain':
					$submitted = array(0,1,3,4,5,6);
					break;
				case 'siujk':
					$submitted = array(1,2,3,4,5,6);
					break;
			}
			$__val = array();
			foreach ($submitted as $key => $value) {
				$__val[$key] = $__validation[$value];
			}
			// print_r($__val);
			$this->form_validation->set_rules($__val);
			$this->validation($__val);
		}else{

			$this->form_validation->set_rules($this->formWizard['step'][$_POST['validation']]['form']);
			$this->validation($__validation);
		}
		
	}
	public function validation($form=null){
        $_r = false;
        if($this->form_validation->run()==FALSE){  
            $return['status'] = 'error';
            foreach($form as $value){
                $return['form'][$value['field']] = form_error($value['field']);
                if(isset($value['type'])){
                	if($value['type']=='file'){
                    $return['file'][$value['field']] = $this->session->userdata($value['field']);
		            }
		        }
            }
            
            $_r = false;
        }else{
            $return['status'] = 'success'; 
            $_r = true;          
        }
        echo json_encode($return);
        return $_r;
    }
    function getBidangSubBidang(){
    	echo json_encode($this->im->getBidangSubBidang($this->input->post('id_dpt_type')));
    }
    public function requiredTree($str){
    	
			if(count($this->input->post('bsb'))==0||$this->input->post('bsb')==null){
				// echo 'true';
				$this->form_validation->set_message('requiredTree', 'Bidang Sub-Bidang tidak boleh kosong');
				return false;

			}else{

				return true;

			}

		}
	public function insert_izin(){
		$id =$this->im->insert_izin($this->input->post());
		if($id){
			$this->deleteTemp($this->input->post(), null, $this->formWizard['step']['data']);
			$this->dpt->iu_change($id);
			echo json_encode(array('status'=>'success'));
		}
	}
	public function getDPTType(){
		echo json_encode($this->im->getDPTType());
	}
	public function getBSB(){
		echo json_encode($this->im->getBSBList($this->input->post('id_ijin_usaha')));
	}
	public function getApproval($id){
		$config['query']    = $this->im->getData($this->form, $id);
        
        $return = $this->tablegenerator->initialize($config);
        echo json_encode($return);
	}
	public function editBSB($id){
		$getData = $this->im->selectData($id);
		$return['url'] = site_url('izin/updateBSB/'.$id);
		$return['button'] = array(
                array(
                    'type'=>'cancel',
                    'label'=>'Batal'
                ),array(
                    'type'=>'submit',
                    'label'=>'Simpan',
                )
            );

		$bsb = $this->im->getBidangSubBidang($getData['id_dpt_type']);
		$vendorBSB = $this->im->getVendorBidangSubBidang($id);
		foreach($bsb as $key => $value){
			foreach ($value['data'] as $keyBSB => $valueBSB) {
				$bsb[$key]['data'][$keyBSB]['value'] = 
				$vendorBSB[$key][$keyBSB];
			}
		}
		$return['tree'] = $bsb;
		echo json_encode($return);
	}
	public function updateBSB($id){
		$bsb = $this->input->post('bsb');
		$getData = $this->im->selectData($id);

		$this->db->where('id_ijin_usaha', $id)->delete('ms_iu_bsb');
		foreach ($bsb as $key => $value) {
			foreach($value as $keybsb){
				$this->table = 'ms_iu_bsb';
				$databsb['id_vendor'] = $getData['id_vendor'];
				$databsb['id_ijin_usaha'] = $id;
				$databsb['id_bidang'] = $key;
				$databsb['id_sub_bidang'] = $keybsb;
				$databsb['entry_stamp'] = timestamp();
				$this->im->insert_bsb($databsb);
			}
		}
		$this->dpt->iu_change($id);
		echo json_encode(array('status'=>'success'));
	}
	public function edit($id){
        $modelAlias = $this->modelAlias;
        $data   = $this->$modelAlias->selectData($id);
        $klasifikasi = array(
								1=>'Pengadaan Barang',
								2=>'Jasa Konsultan non-Konstruksi',
								3=>'Jasa Lainnya',
								4=>'Jasa Pekerjaan Konstruksi',
								5=>'Jasa Konsultan Perencana/Pengawas Konstruksi'
							);
        $siu = 	array(
        			'siujk'=>'SIUJK',
					'sbu'=>'SBU',
					'siup'=>'SIUP',
					'ijin_lain'=>'Surat Ijin Usaha Lainnya',
					'asosiasi'=>'Sertifikat Asosiasi/Lainnya'
        		);	
       	$kualifikasi =	array(
								'kecil'=>'Kecil',
								'menengah'=>'Menengah',
								'besar'=>'Besar'
							);
        $__form = array(
        	array(
        		'field'	=> 	'id_dpt_type',
				'type'	=>	'text',
				'label'	=>	'Klasifikasi Usaha',
				'readonly' => 	true,
				'value'	=>	$klasifikasi[$data['id_dpt_type']]
			),
			array(
				'field'	=> 	'type',
				'type'	=>	'text',
				'label'	=>	'Surat Ijin Usaha',
				'readonly' => 	true,
				'value'=>	$siu[$data['type']]
			),
			array(
				'field'	=> 	'authorize_by',
				'type'	=>	'text',
				'label'	=>	($data['type']=='asosiasi') ? 'Anggota Asosiasi': 'Lembaga Penerbit',
				'rules' => 	'required',
				'value'	=>	$data['authorize_by']
			),
			array(
				'field'	=> 	'no',
				'type'	=>	'text',
				'label'	=>	'Nomor',
				'rules' => 	'required',
				'value'	=>	$data['no']
			),
			array(
				'field'	=> 	'grade',
				'type'	=>	'dropdown',
				'label'	=>	'Grade',
				'rules' => 	'required',
				'source'=>	array(
								'a'=>'A',
								'b'=>'B',
								'c'=>'C'
							),
				'value'	=>	$data['grade']
			),
			array(
				'field'	=> 	'issue_date',
				'type'	=>	'date',
				'label'	=>	'Tanggal',
				'rules' => 	'required|forward_date',
				'value'	=>	$data['issue_date']
			),
			array(
				'field'	=> 	'qualification',
				'type'	=>	'radio',
				'label'	=>	'Kualifikasi',
				'rules' => 	'required',
				'source'=>	$kualifikasi,
				'value'	=>	$data['qualification']
			),
			array(
				'field'	=> 	'expiry_date',
				'type'	=>	'lifetimeDate',
				'label'	=>	'Masa Berlaku',
				'rules' => 	'required',
				'value'	=>	$data['expiry_date']
			),
			array(
				'field'	=> 	'izin_file',
				'type'	=>	'file',
				'label'	=>	'Lampiran',
				'rules' => 	'required',
				'value'	=>	$data['izin_file'],
				'upload_path'=>base_url('assets/lampiran/izin_file/'),
				'upload_url'=>site_url('izin/upload_lampiran'),
				'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
			),
        );

        switch ($data['type']) {
			case 'siup':
				$submitted = array(0,1,3,5,6,7,8);
				break;
			
			case 'sbu':
				$submitted = array(0,1,2,3,5,7,8);
				break;
			case 'asosiasi':
				$submitted = array(0,1,2,3,5,7,8);
				break;
			case 'ijin_lain':
				$submitted = array(0,1,2,3,5,6,7,8);
				break;
			case 'siujk':
				$submitted = array(0,1,3,4,5,6,7,8);
				break;
		}
		foreach ($submitted as $key => $value) {
			$form['form'][$key] = $__form[$value];
		}

        $form['url'] = site_url($this->updateUrl .'/'.$id);
        $form['button'] = array(
                array(
                    'type'=>'submit',
                    'label'=>'Ubah'
                ),
                array(
                    'type'=>'cancel',
                    'label'=>'Batal'
                )
            );
        echo json_encode($form);
    }

    public function update($id){
      	$modelAlias = $this->modelAlias;
        $data   = $this->$modelAlias->selectData($id);
      	$__form = array(
			array(
				'field'	=> 	'authorize_by',
				'label'	=>	($data['type']=='asosiasi') ? 'Anggota Asosiasi': 'Lembaga Penerbit',
				'rules' => 	'required',
				'type'	=>	'text'
			),
			array(
				'field'	=> 	'no',
				'label'	=>	'Nomor',
				'rules' => 	'required',
				'type'	=>	'text'
			),
			array(
				'field'	=> 	'grade',
				'label'	=>	'Grade',
				'rules' => 	'required',
				'type'	=>	'text'
			),
			array(
				'field'	=> 	'issue_date',
				'label'	=>	'Tanggal',
				'rules' => 	'required',
				'type'	=>	'date'
			),
			array(
				'field'	=> 	'qualification',
				'label'	=>	'Kualifikasi',
				'rules' => 	'required',
				'type'	=>	'radio',
			),
			array(
				'field'	=> 	'expiry_date',
				'label'	=>	'Masa Berlaku',
				'rules' => 	'required',
				'type'	=>	'lifetimeDate',
			),
			array(
				'field'	=> 	'izin_file',
				'label'	=>	'Lampiran',
				'rules' => 	'required',
				'type'	=>	'file',
			),
        );

        switch ($data['type']) {
			case 'siup':
				$submitted = array(1,3,4,5,6);
				break;
			case 'sbu':
				$submitted = array(0,1,3,5,6);
				break;
			case 'asosiasi':
				$submitted = array(0,1,3,5,6);
				break;
			case 'ijin_lain':
				$submitted = array(0,1,3,4,5,6);
				break;
			case 'siujk':
				$submitted = array(1,2,3,4,5,6);
				break;
		}
		$__val = array();
		foreach ($submitted as $key => $value) {
			$__val['form'][$key] = $__form[$value];
		}

        $this->form_validation->set_rules($__val['form']);
        if($this->validation($__val)){
            $save = $this->input->post();
            $lastData = $this->$modelAlias->selectData($id);

            if($this->$modelAlias->update($id, $save)){
            	$this->dpt->iu_change($id);
                $this->session->set_userdata('alert', $this->form['successAlert']);
                $this->deleteTemp($save, $lastData, $__val);
            }
        }
    }
    public function deleteTemp($save, $lastData=null, $form=null){
        foreach ($form['form'] as $key => $value) {
            if($value['type']=='file'){

                if($lastData!=null&&( $save[$value['field']] != $lastData[$value['field']])){
                    if($lastData[$value['field']]!=''){
                        unlink('./assets/lampiran/'.$value['field'].'/'.$lastData[$value['field']]);
                    }
                }
                if($save[$value['field']]!=''){
                    if(file_exists('./assets/lampiran/temp/'.$save[$value['field']])){
                        rename('./assets/lampiran/temp/'.$save[$value['field']], './assets/lampiran/'.$value['field'].'/'.$save[$value['field']]);
                    }
                }
               
                
            }
        }
    }

    public function delete($id){
        MY_Controller::delete($id);

    }
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');

class K3 extends VMS {

	public $formWizard;
	public $modelAlias = 'km';
	public $alias = 'ms_csms';
	public $module = 'Izin';
	public function __construct(){
		parent::__construct();		
		$this->load->model('K3_model','km');	
		$this->load->model('Vendor_model','um');	
		$user = $this->session->userdata('user');
		$this->insertUrl = site_url('k3/save/'.$user['id_user']);
		$this->updateUrl = 'k3/update';
		$this->deleteUrl = 'k3/delete/';
		
		$this->formWizard = array(
			'step'=>array(
				'question'=>array(
					'label'=>'Sertifikat CSMS',
					'form'=>array(
								array(
									'field'	=> 	'csms',
									'type'	=>	'radio',
									'label'	=>	'Apakah perusahaan anda memiliki sertifikat CSMS?',
									'rules' => 	'required',
									'source'=>	array(
													1=>'Punya',
													2=>'Belum Punya',
												)
								)
							),
					'button'=>array(
						array(
		                    'type'=>'next',
		                    'label'=>'Lanjut',
		                    'class'=>'btn-next'
		                )
					)

				),
				'sertifikat'=>array(
					'label'=>'Sertifikat CSMS',
					'form'=>array(
								array(
									'field'	=> 	'csms_file',
									'type'	=>	'file',
									'label'	=>	'Sertifikat CSMS',
									'rules' => 	'required',
									'upload_path'=>base_url('assets/lampiran/csms_file/'),
									'upload_url'=>site_url('k3/upload_lampiran'),
									'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
								),
								array(
									'field'	=> 	'score',
									'type'	=>	'text',
									'label'	=>	'Nilai CSMS',
									'rules' => 	'required',
								)
								
							),
					'button'=>array(
						array(
		                    'type'=>'prev',
		                    'label'=>'Sebelumnya',
		                    'class'=>'btn-prev'
		                ),array(
		                    'type'=>'next',
		                    'label'=>'Simpan',
		                     'class'=>'btn-next'
		                )
					)
				)
			),
			'url' 	=> 	$this->insertUrl
		);
	
	}

	public function index(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('k3'),
			'title' => 'K3'
		));
		 $user = $this->session->userdata('user');
		$this->header = 'K3';
		if($this->km->cek_csms()){
			$__data = $this->km->getK3Data($user['id_user']);
			if($__data['csms_file']!=''){
				$this->content = $this->load->view('k3/view',null, TRUE);
				$this->script = $this->load->view('k3/view_js', null, TRUE);
			}else{
				$this->content = $this->load->view('k3/view_form',null, TRUE);
				$this->script = $this->load->view('k3/view_form_js', null, TRUE);
			}
			
		}else{
			$this->content = $this->load->view('k3/form',null, TRUE);
			$this->script = $this->load->view('k3/form_js', null, TRUE);
		}
		
		parent::index();
	}	
	public function field_k3(){
		echo json_encode($this->formWizard);
	}
	public function simpan(){
		if($_POST['validation']=='question'){
			$this->formWizard['step'][$_POST['validation']]['form'][0]['label'] = 'Pilihan';
		}
		$__validation = $this->formWizard['step'][$_POST['validation']]['form'];
		$user = $this->session->userdata('user');
		$this->dpt->non_iu_change($user['id_user']);
		$this->form_validation->set_rules($this->formWizard['step'][$_POST['validation']]['form']);
		$this->validation($__validation);
		
	}
	public function validation($form=null){
        $_r = false;

        if($this->form_validation->run()==FALSE){  

            $return['status'] = 'error';
            foreach($form as $value){
                $return['form'][$value['field']] = form_error($value['field']);
                if(isset($value['type'])){
                	if($value['type']=='file'){
                    $return['file'][$value['field']] = $this->session->userdata($value['field']);
		            }
		        }
               

            }
            
            $_r = false;
        }else{
            $return['status'] = 'success'; 
            $_r = true;          
        }
        echo json_encode($return);
        return $_r;
    }
    public function update($id){
        $modelAlias = $this->modelAlias;
         $user = $this->session->userdata('user');
        $this->form_validation->set_rules($this->formWizard['step']['sertifikat']['form']);
        if($this->validation($this->formWizard['step']['sertifikat']['form'])){

            $save = $this->input->post();
            $save['id_csms_limit'] = $this->km->get_k3_limit($save['score']);
            $lastData = $this->$modelAlias->selectData($id);

            if($this->$modelAlias->update($id, $save)){
            	$user = $this->session->userdata('user');
            	$this->dpt->non_iu_change($user['id_user']);
                $this->session->set_userdata('alert', $this->form['successAlert']);
                $this->form['form'] = $this->formWizard['step']['sertifikat']['form'];
                $this->deleteTemp($save, $lastData);
            }
        }
    }

   public function save($data=null){
        $modelAlias = $this->modelAlias;
        $save = $this->input->post();
         $user = $this->session->userdata('user');
        $save['id_vendor'] = $user['id_user'];
        $save['data_status'] = 0;
        $save['entry_stamp'] = timestamp();
        unset($save['csms']);
        if($this->$modelAlias->save_csms_data($save)){
        	$this->dpt->non_iu_change($user['id_user']);
        	$this->form['form'] = $this->formWizard['step']['sertifikat']['form'];
            $this->deleteTemp($save);
            echo json_encode(array('status'=>'success'));
        }
    }
   	public function getData(){
   		$getData = $this->km->getK3Data();
   		$form = $this->formWizard['step']['sertifikat']['form'];
   		$form[] = array(
									'field'	=> 	'value',
									'type'	=>	'text',
									'label'	=>	'Kualifikasi CSMS',
									'rules' => 	'required',
								);
   		$_form = array();
   		$_form['id'] = $getData['id'];
   		$_form['button']=array(
						array(
		                    'type'=>'button',
		                    'label'=>'Edit CSMS',
		                    'class'=>'btn-edit'
		                ),array(
		                    'type'=>'button',
		                    'label'=>'Reset',
		                     'class'=>'btn-warning btn-reset',
		                     'icon'=>'warning'
		                )
					);
		foreach($form as $key => $value){
			$_form['form'][$key]= $value;
			$_form['form'][$key]['readonly'] = TRUE;
			$_form['form'][$key]['value'] = $getData[$value['field']];
		}

		echo json_encode($_form);
   	}
   	public function form_csms(){
   		$modelAlias = $this->modelAlias;
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('k3'),
			'title' => 'K3'
		));

		$this->breadcrumb->addlevel(2, array(
			'url' => site_url('k3/form_csms'),
			'title' => 'Form CSMS'
		));

		$this->header = '<center><b>SISTEM EVALUASI MANAJEMEN K3 KONTRAKTOR<br>
CHECKLIST SISTEM RATING PRA KUALIFIKASI</b></center>';
		$this->content = $this->load->view('k3/form_csms',null, TRUE);
		$this->script = $this->load->view('k3/form_csms_js', null, TRUE);
		parent::index();
   	}
   	public function get_form_csms(){	
		$user = $this->session->userdata('user');
		$data['data_k3']		= $this->km->getK3Answer($user['id_user']);
		$data['csms_file'] 		= $this->km->get_k3_all_data($user['id_user']);
		$data['field_quest'] 	= $this->km->get_field_quest();
		$data['ms_quest']		= $this->km->get_header_list();
		$data['sub_quest']		= $this->km->get_sub_quest_list();
		$data['data_quest']		= $this->km->get_quest_list();
		$data['data_field']		= $this->km->get_data_field();
		$data['quest_all']		= array();

		foreach($data['ms_quest'] as $key_ms => $row_ms){
			$data['quest_all'][$key_ms]['label'] 	= $row_ms;
		}

		foreach($data['sub_quest'] as $key_sub_quest => $val_sub_quest){
			foreach($val_sub_quest as $k_sub_quest => $v_sub_quest){
				$data['quest_all'][$key_sub_quest]['data'][$k_sub_quest] = $v_sub_quest;
			}
		}

		foreach($data['data_quest'] as $key_quest => $val_quest){
			$data['quest_all'][$val_quest['id_ms_header']]['data'][$val_quest['id_sub_header']]['data'][$val_quest['id']] = array();
		}

		foreach ($data['data_field'] as $key_data => $value_data) {
			$data['quest_all'][$value_data['id_ms_header']]['data'][$value_data['id_sub_header']]['data'][$value_data['id_question']][$value_data['id']] = $value_data;	
		}
		$data['button']=array(
							array(
			                    'type'=>'submit',
			                    'label'=>'Simpan',
			                    'class'=>'btn-primary'
			                )
						);
		echo json_encode($data);
	}
	public function view_form_csms(){	
		$user = $this->session->userdata('user');
		$data['data_k3']		= $this->km->getK3Answer($user['id_user']);
		$data['csms_file'] 		= $this->km->get_k3_all_data($user['id_user']);
		$data['field_quest'] 	= $this->km->get_field_quest();
		$data['ms_quest']		= $this->km->get_header_list();
		$data['sub_quest']		= $this->km->get_sub_quest_list();
		$data['data_quest']		= $this->km->get_quest_list();
		$data['data_field']		= $this->km->get_data_field();
		$data['quest_all']		= array();

		foreach($data['ms_quest'] as $key_ms => $row_ms){
			$data['quest_all'][$key_ms]['label'] 	= $row_ms;
		}

		foreach($data['sub_quest'] as $key_sub_quest => $val_sub_quest){
			foreach($val_sub_quest as $k_sub_quest => $v_sub_quest){
				$data['quest_all'][$key_sub_quest]['data'][$k_sub_quest] = $v_sub_quest;
			}
		}

		foreach($data['data_quest'] as $key_quest => $val_quest){
			$data['quest_all'][$val_quest['id_ms_header']]['data'][$val_quest['id_sub_header']]['data'][$val_quest['id']] = array();
		}

		foreach ($data['data_field'] as $key_data => $value_data) {
			$data['quest_all'][$value_data['id_ms_header']]['data'][$value_data['id_sub_header']]['data'][$value_data['id_question']][$value_data['id']] = $value_data;	
		}
		$data['button']=array(
							array(
			                    'type'=>'edit',
			                    'label'=>'<i class="fa fa-gears"></i>&nbsp;Ubah',
			                    'class'=>'btn-warning btn-edit',
			                ),
			                array(
			                    'type'=>'reset',
			                    'label'=>'<i class="fa fa-trash"></i>&nbsp;Reset',
			                    'class'=>'btn-error'
			                )
						);
		echo json_encode($data);
	}
   	public function edit($id){
        $modelAlias = $this->modelAlias;
        $data   = $this->$modelAlias->selectData($id);
        $__form = array();	
        foreach($this->formWizard['step']['sertifikat']['form'] as $key => $element){
    		$__form['form'][$key] = $element;
            $__form['form'][$key]['value'] = $data[$element['field']];
        }

        $__form['url'] = site_url($this->updateUrl .'/'.$id);
        $__form['button'] = array(
                array(
                    'type'=>'submit',
                    'label'=>'Ubah'
                ),
                array(
                    'type'=>'cancel',
                    'label'=>'Batal'
                )
            );
        echo json_encode($__form);
    }

    public function resetForm($id){
	 	$this->formDelete['url'] = site_url('k3/reset/'.$id);
        $this->formDelete['button'] = array(
                array(
                    'type'=>'delete',
                    'label'=>'Reset'
                ),
                array(
                    'type'=>'cancel',
                    'label'=>'Batal'
                )
            );
        echo json_encode($this->formDelete);
    }
   	public function reset($id){
   		$user = $this->session->userdata('user');
   		$this->dpt->non_iu_change($user['id_user']);
   		$this->delete($id);
   		$this->km->delete_answer_by_csms($id);
   	}
   	public function upload_lampiran_form(){
   		$files = $_FILES['quest'];
   		 
        foreach($files['name'] as $key => $row){
        	$data_question = $this->km->get_question_data($key);
            $file_name = $data_question['label'].'_'.name_generator($row);
            $_FILES['quest']['name'] = $file_name;  
			$_FILES['quest']['size'] = $files['size'][$key];  
			$_FILES['quest']['tmp_name'] = $files['tmp_name'][$key];  
			$_FILES['quest']['type'] = $files['type'][$key];
			$_FILES['quest']['error'] = $files['error'][$key];
            $config['upload_path'] = './assets/lampiran/temp/';
            $config['allowed_types'] = $_POST['allowed_types'];
            $this->load->library('upload');
            if(!is_dir($config['upload_path'])){
                mkdir($config['upload_path']);
            }  

            $this->upload->initialize($config);

            if ( ! $this->upload->do_upload('quest')){
                $return['status'] = 'error';
                $return['message'] = $this->upload->display_errors('','');
            }else{
                $return['status'] = 'success';
                $return['upload_path']= base_url('assets/lampiran/temp/'.$file_name);
                $return['file_name'] = $file_name;
            }
            echo json_encode($return);
        }
    }
    public function saveCSMS(){
    	$user = $this->session->userdata('user');
    	$save = $this->input->post('quest');
    	$return = $this->km->save_k3_data($save, $user['id_user']);

    	if($return){
    		$this->deleteTempK3($save); 
    		$this->dpt->non_iu_change($user['id_user']);
    		echo json_encode(array('status'=>'success'));
    	}else{
    		echo json_encode(array('status'=>'error'));
    	}
    }
    public function penilaian_csms(){
    	$this->breadcrumb->addlevel(1, array(
			'url' => site_url('k3/penilaian_csms'),
			'title' => 'CSMS'
		));
		

		$this->header = 'CSMS';
		$this->content = $this->load->view('k3/penilaian_list',$data, TRUE);
		$this->script = $this->load->view('k3/penilaian_list_js', $data, TRUE);
		parent::index();
	
    }
    public function form_penilaian($id_vendor, $act='create'){

    	$this->breadcrumb->addlevel(1, array(
			'url' => site_url('k3/penilaian_csms'),
			'title' => 'Penilaian CSMS'
		));
    	$data['act'] = $act;
    	$data['id_vendor'] = $id_vendor;
		$data['vendor'] = $this->vm->get_data($id_vendor);
		$data['ms_quest']=$this->km->get_header_list();
		
		$data['quest']=$this->km->get_quest();
		$data['field_quest'] = $this->km->get_field_quest();
		$data['evaluasi_list'] = $this->km->get_evaluasi_list();
		$data['evaluasi'] = $this->km->get_evaluasi();
		$data['data_k3']=$this->km->getK3Answer($id_vendor);
		$data['get_csms'] = $this->km->get_csms($id_vendor);
		$data['get_hse'] = $this->km->get_hse($id_vendor);
		
		$data['csms_file'] 	= $this->km->get_k3_all_data($id_vendor)['csms_file'];

		$data['value_k3']=$this->km->get_penilaian_value($id_vendor,$data['csms_file']['id']);	

		$this->header = 'CSMS';
		$this->content = $this->load->view('k3/penilaian_k3',$data, TRUE);
		$this->script = $this->load->view('k3/penilaian_k3_js', $data, TRUE);
		parent::index();
    }
    public function listPenilaian(){
    	$config['query']	= $this->km->daftar_vendor_k3();
		$return	= $this->tablegenerator->initialize($config);
		echo json_encode($return);
    }
    public function deleteTempK3($save, $lastData=null){
       	$form = $this->km->get_field_quest();
        foreach ($form as $key => $value) {
            if($value['type']=='file'){
                if($save[$value['id']]!=''){
                    if(file_exists('./assets/lampiran/temp/'.$save[$value['id']])){
                        rename('./assets/lampiran/temp/'.$save[$value['id']], './assets/lampiran/'.$value['label'].'/'.$save[$value['id']]);
                    }
                }
            }
        }
    }
    public function save_penilaian($id){
    	$res = $this->km->save_evaluasi_poin($this->input->post('evaluasi'),$id);
    	if($res){
				$email['subject']	= "Penilaian CSMS (".$vendor['type'].". ".$vendor['name'].")";
				$email['message']	= 'Admin Logistik telah selesai mengadakan penilaian CSMS untuk Penyedia Barang / Jasa '.$vendor['type'].". ".$vendor['name']."  
										<br>
										Untuk melengkapi proses verifikasi, harap login ke sistem dan mengakses menu Penilaian CSMS.
										<br><br>
										Terimakasih,<br>";
				foreach($this->km->get_admin_email(1) as $key => $admin){
					$this->utility->mail($admin['email'],$email['message'],$email['subject']);
				}
				$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Sukses menyimpan data!</p>');
				redirect('k3/penilaian_view/'.$id_vendor);
			}
    }
}

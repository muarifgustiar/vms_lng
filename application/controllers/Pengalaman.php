<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pengalaman extends VMS {

	public $form;
	public $modelAlias = 'pm';
	public $alias = 'ms_pengalaman';
	public $module = 'Pengalaman';
	public function __construct(){
		parent::__construct();		
		$this->load->model('Pengalaman_model','pm');
		$this->load->model('Akta_model','am');		
		$this->load->model('Main_model','mm');		
		$user = $this->session->userdata('user');
		$this->form = array(
				'form' => array(
					array(
						'field'	=> 	'job_name',
						'type'	=>	'text',
						'label'	=>	'Nama Paket Pekerjaan',
						'rules' => 	'required'
					),
					array(
						'field'	=> 	'job_location',
						'type'	=>	'text',
						'label'	=>	'Lokasi',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'job_giver',
						'type'	=>	'text',
						'label'	=>	'Nama Instansi Pemberi Tugas',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'phone_no',
						'type'	=>	'text',
						'label'	=>	'No. Telp Pemberi Tugas',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'contract_no',
						'type'	=>	'text',
						'label'	=>	'No. Kontrak',
						'rules' => 	'required',
					),
					array(
						'type'	=>	'date_range',
						'label'	=>	'Tanggal Kontrak',
						'name'	=>	'contract',
						'field'	=> 	array(
											'contract_start',
											'contract_end'
										),
					),
					array(
						'type'	=>	'money',
						'label'	=>	'Nilai Kontrak',
						'field'	=>	'price_idr',
						'caption'=>	'(Dalam rupiah)',
					),
					array(
						'type'	=>	'money_asing',
						'label'	=>	'Nilai Kontrak',
						'name'	=>	'price_foreign',
						'caption'=>	'(Dalam Mata Uang Asing)',
						'field' => 	array(
										'currency',
										'price_foreign'
									),
						'source'=>	$this->mm->getKurs()
					),
					array(
						'field'	=> 	'contract_file',
						'type'	=>	'file',
						'label'	=>	'Lampiran (Dokumen kontrak)',
						'rules' => 	'required',
						'upload_path'=>base_url('assets/lampiran/contract_file/'),
						'upload_url'=>site_url('pengalaman/upload_lampiran'),
						'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip'
					),
					array(
						'field'	=> 	'contract_start',
						'label'	=>	'Tanggal Kontrak Awal',
						'name'	=>	'contract',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'contract_end',
						'label'	=>	'Tanggal Kontrak Berakhir',
						'name'	=>	'contract',
						'rules' => 	'required',
					),
					array(
						'field'	=> 	'price_foreign',
						'label'	=>	'Nilai Kontrak (Dalam Mata Uang Asing)',
						'field'	=>	'price_foreign'
					),
					
				),
				'filter'=>array(
					array(
						'field'	=> 	'job_name',
						'type'	=>	'text',
						'label'	=>	'Nama Paket Pekerjaan',
					),
					array(
						'field'	=> 	'job_location',
						'type'	=>	'text',
						'label'	=>	'Lokasi',
					),
					array(
						'field'	=> 	'job_giver',
						'type'	=>	'text',
						'label'	=>	'Nama Instansi Pemberi Tugas',
					),
					array(
						'field'	=> 	'phone_no',
						'type'	=>	'text',
						'label'	=>	'No. Telp Pemberi Tugas',
					),
					array(
						'field'	=> 	'contract_no',
						'type'	=>	'text',
						'label'	=>	'No. Kontrak',
					)
				)
			);
		$this->insertUrl = site_url('pengalaman/save/'.$user['id_user']);
		$this->updateUrl = 'pengalaman/update';
		$this->deleteUrl = 'pengalaman/delete/';
		$this->getData = $this->pm->getData($this->form);
		$this->form_validation->set_rules($this->form['form']);
	}

	public function index(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('pengalaman'),
			'title' => 'Pengalaman'
		));
		
		$this->header = 'Pengalaman';
		$this->content = $this->load->view('pengalaman/list',null, TRUE);
		$this->script = $this->load->view('pengalaman/list_js', null, TRUE);
		parent::index();
	}

  	public function getCurrency(){
  		echo json_encode($this->mm->getKurs());
  	}

	public function getApproval($id){
		$config['query']    = $this->pm->getData($this->form, $id);
        
        $return = $this->tablegenerator->initialize($config);
        echo json_encode($return);
	}
	
}

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public $_sideMenu;

	public $breadcrumb;

	public $header;

	public $content;

	public $script;

    public $form;

    public $activeMenu;

    function __construct()
    {
        parent::__construct();

        $this->_sideMenu = array();
        $this->load->library('breadcrumb', array());
        $this->form_validation->set_error_delimiters('', '');
    }

    function index(){
        
        $this->setMenu();
    	$this->breadcrumb = $this->breadcrumb->generate();
    	
        $this->load->library('sideMenu', $this->_sideMenu);
        $user = $this->session->userdata('user');
        $admin = $this->session->userdata('admin');
    	$data = array(
            'user'      =>  ($user) ? $user['name'] : $admin['name'],
    		'sideMenu'	=>	$this->sidemenu->generate($this->activeMenu),
    		'breadcrumb'=>	$this->breadcrumb,
    		'header'	=>	$this->header,
    		'content'	=> 	$this->content,
    		'script'	=>	$this->script
    	);
    	$this->parser->parse('template/base', $data);
    }

    function formFilter(){
        $return['button'] = array(
                array(
                    'type'=>'button',
                    'label'=>'Filter',
                    'class'=>'btn-filter'
                ),
                array(
                    'type'=>'reset',
                    'label'=>'Reset'
                )
            );
        $return['form'] = $this->form['filter'];
        echo json_encode($return);
    }
    function setMenu(){
        $admin = $this->session->userdata('admin');
        if($admin){
            $menu = array(
                array(
                    'group' => 'dashboard',
                    'title' => 'Dashboard',
                    'icon'  => 'dashboard',
                    'url'   => site_url()
                ),
                array(
                    'group' => 'master',
                    'title' => 'Master',
                    'icon'  => 'database',
                    'url'   => '#',
                    'list'  =>  array(
                                    array(
                                        'url'=>site_url('master/user'),
                                        'title'=>'User'
                                    ),
                                    array(
                                        'url'=>site_url('master/badan_hukum'),
                                        'title'=>'Badan Hukum'
                                    ),
                                    array(
                                        'url'=>site_url('master/bidang'),
                                        'title'=>'Bidang'
                                    ),
                                    array(
                                        'url'=>site_url('master/sub_bidang'),
                                        'title'=>'Sub-Bidang'
                                    ),
                                    array(
                                        'url'=>site_url('master/kurs'),
                                        'title'=>'Kurs'
                                    ),
                                    array(
                                        'url'=>site_url('master/k3'),
                                        'title'=>'K3'
                                    ),
                                    array(
                                        'url'=>site_url('master/k3_passing_grade'),
                                        'title'=>'Passing Grade K3'
                                    ),
                                    array(
                                        'url'=>site_url('master/assessment'),
                                        'title'=>'Assessment'
                                    ),
                                    array(
                                        'url'=>site_url('master/evaluasi'),
                                        'title'=>'Poin Evaluasi'
                                    ),
                                )
                ),array(
                    'group' => 'vendor',
                    'title' => 'Penyedia Barang/Jasa',
                    'icon'  => 'user',
                    'url'   => site_url('vendor'),
                ),array(
                    'group' => 'csms',
                    'title' => 'CSMS',
                    'icon'  => 'user',
                    'url'   => site_url('k3/penilaian_csms'),
                ),
                array(
                    'group' => 'dpt',
                    'title' => 'DPT',
                    'icon'  => 'suitcase',
                    'url'   => site_url('vendor/dpt')
                ),
                array(
                    'group' => 'pengadaan',
                    'title' => 'Pengadaan',
                    'icon'  => 'briefcase',
                    'url'   => site_url('pengadaan')
                )
            );

            switch ($admin['id_role']) {
                case 1 :
                    $__menu = array(0,1,2,4);
                    break;
                
                case 2 :
                     $__menu = array(0,3);
                    break;
                case 9 :
                     $__menu = array(0,5);
                    break;
            }
            foreach($__menu as $key => $val){
                $this->_sideMenu[$key] = $menu[$val];
            }

        }else if($this->session->userdata('user')){
             $this->_sideMenu = array(
                array(
                    'group' => 'dashboard',
                    'title' => 'Dashboard',
                    'icon'  => 'dashboard',
                    'url'   => site_url()
                ),
                array(
                    'group' => 'administrasi',
                    'title' => 'Administrasi',
                    'icon'  => 'file-text-o',
                    'url'   => site_url('administrasi')
                ),
                array(
                    'group' => 'akta',
                    'title' => 'Akta',
                    'icon'  => 'file-text-o',
                    'url'   => site_url('akta')
                ),
                array(
                    'group' => 'situ',
                    'title' => 'SITU / SKDP',
                    'icon'  => 'file-text-o',
                    'url'   => site_url('situ')
                ),
                array(
                    'group' => 'tdp',
                    'title' => 'TDP',
                    'icon'  => 'file-text-o',
                    'url'   => site_url('tdp')
                ),
                array(
                    'group' => 'pengurus',
                    'title' => 'Pengurus',
                    'icon'  => 'file-text-o',
                    'url'   => site_url('pengurus')
                ),
                array(
                    'group' => 'pemilik',
                    'title' => 'Pemilik',
                    'icon'  => 'file-text-o',
                    'url'   => site_url('pemilik')
                ),
                array(
                    'group' => 'izin',
                    'title' => 'Izin',
                    'icon'  => 'file-text-o',
                    'url'   => site_url('izin')
                ),
                array(
                    'group' => 'pengalaman',
                    'title' => 'Pengalaman',
                    'icon'  => 'file-text-o',
                    'url'   => site_url('pengalaman')
                ),
                array(
                    'group' => 'agen',
                    'title' => 'Pabrikan/Keagenan/Distributor',
                    'icon'  => 'file-text-o',
                    'url'   => site_url('agen')
                )
            );

            if(is_k3()){
                 $this->_sideMenu[] = array(
                    'group' => 'k3',
                    'title' => 'Aspek K3',
                    'icon'  => 'life-buoy',
                    'url'   => site_url('k3')
                );
            }
            if(is_perseorangan()){
                $this->_sideMenu = array(
                    array(
                        'group' => 'administrasi',
                        'title' => 'Administrasi',
                        'icon'  => 'file-text-o',
                        'url'   => site_url('administrasi')
                    ),
                    array(
                        'group' => 'pengalaman',
                        'title' => 'Pengalaman',
                        'icon'  => 'file-text-o',
                        'url'   => site_url('pengalaman')
                    )
                );
            }
        }
       
        
              
           
    }
    public function validation($form=null){

        $_r = false;
        if($form==null) $form = $this->form['form'];
       
        if($this->form_validation->run()==FALSE){  
            $return['status'] = 'error';

            foreach($form as $value){

                if($value['type']=='file'){
                    $return['file'][$value['field']] = $this->session->userdata($value['field']);
                } 
                if($value['type']=='date_range'){
                    // print_r($value);
                    // foreach($value['field'] as $_value){
                    $return['form'][$value['field'][0]] = form_error($value['field'][0].'_start');
                    $return['form'][$value['field'][1]] = form_error($value['field'][1].'_start');
                    // }
                    
                }else{
                    $return['form'][$value['field']] = form_error($value['field']);
                }

            }

            $_r = false;
        }else{
            $return['status'] = 'success'; 
            $_r = true;          
        }
        echo json_encode($return);
        return $_r;
    }
    
    public function getData(){
        $config['query']    = $this->getData;
        
        $return = $this->tablegenerator->initialize($config);
        echo json_encode($return);
    }
    public function insert(){
        $this->form['url'] = $this->insertUrl;
        $this->form['button'] = array(
                array(
                    'type'=>'submit',
                    'label'=>'Simpan',
                ),
                array(
                    'type'=>'cancel',
                    'label'=>'Batal'
                )
            );
        echo json_encode($this->form);
    }
    public function save($data=null){
        $modelAlias = $this->modelAlias;

        if($this->validation()){
            $save = $this->input->post();
            $save['entry_stamp'] = timestamp();

            if($this->$modelAlias->insert($save)){
                 $this->deleteTemp($save);
                return true;
            }
        }
    }

    public function edit($id){
        $modelAlias = $this->modelAlias;
        $data   = $this->$modelAlias->selectData($id);

        foreach($this->form['form'] as $key => $element){
    
            $this->form['form'][$key]['value'] = $data[$element['field']];
        }

        $this->form['url'] = site_url($this->updateUrl .'/'.$id);
        $this->form['button'] = array(
                array(
                    'type'=>'submit',
                    'label'=>'Ubah'
                ),
                array(
                    'type'=>'cancel',
                    'label'=>'Batal'
                )
            );
        echo json_encode($this->form);
    }

    public function update($id){
        $modelAlias = $this->modelAlias;
        if($this->validation()){

            $save = $this->input->post();
            $lastData = $this->$modelAlias->selectData($id);

            if($this->$modelAlias->update($id, $save)){
                $this->session->set_userdata('alert', $this->form['successAlert']);
                $this->deleteTemp($save, $lastData);
            }
        }
    }

    public function approveOvertimeUser($id){
        $modelAlias = $this->modelAlias;
        
        $save = $this->input->post();
        $save['edit_stamp'] = timestamp();

        return $this->$modelAlias->update($id, $save);
        
    }

    public function delete($id){
        $modelAlias = $this->modelAlias;
        if($this->$modelAlias->delete($id)){
            $return['status'] = 'success';  
        }else{
            $return['status'] = 'error';  
        }
        echo json_encode($return);
    }

    public function remove($id){
        $this->formDelete['url'] = site_url($this->deleteUrl.$id);
        $this->formDelete['button'] = array(
                array(
                    'type'=>'delete',
                    'label'=>'Hapus'
                ),
                array(
                    'type'=>'cancel',
                    'label'=>'Batal'
                )
            );
        echo json_encode($this->formDelete);
    }

    public function upload_lampiran(){
        foreach($_FILES as $key => $row){
            $file_name = $_FILES[$key]['name'] = $key.'_'.name_generator($_FILES[$key]['name']);
            $config['upload_path'] = './assets/lampiran/temp/';
            $config['allowed_types'] = $_POST['allowed_types'];
            $this->load->library('upload');
            if(!is_dir($config['upload_path'])){
                mkdir($config['upload_path']);
            }  

            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload($key)){
                $return['status'] = 'error';
                $return['message'] = $this->upload->display_errors('','');
            }else{
                $return['status'] = 'success';
                $return['upload_path']= base_url('assets/lampiran/temp/'.$file_name);
                $return['file_name'] = $file_name;
            }
            echo json_encode($return);
        }
    }

    public function do_upload($field, $db_name = ''){   
        
        $file_name = $_FILES[$db_name]['name'] = $db_name.'_'.name_generator($_FILES[$db_name]['name']);
        
        $config['upload_path'] = './assets/lampiran/'.$db_name.'/';
        $config['allowed_types'] = 'pdf|jpeg|jpg|png|gif';
        
        $this->load->library('upload');
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload($db_name)){
            $_POST[$db_name] = $file_name;
            $this->form_validation->set_message('do_upload', $this->upload->display_errors('',''));
            return false;
        }else{

            $this->session->set_userdata($db_name, $file_name);
            $_POST[$db_name] = $file_name; 
            return true;
        }
    }

    public function deleteTemp($save, $lastData=null){

        foreach ($this->form['form'] as $key => $value) {
            if($value['type']=='file'){

                if($lastData!=null&&( $save[$value['field']] != $lastData[$value['field']])){
                    if($lastData[$value['field']]!=''){
                        unlink('./assets/lampiran/'.$value['field'].'/'.$lastData[$value['field']]);
                    }
                }
                if($save[$value['field']]!=''){
                    if(file_exists('./assets/lampiran/temp/'.$save[$value['field']])){
                        rename('./assets/lampiran/temp/'.$save[$value['field']], './assets/lampiran/'.$value['field'].'/'.$save[$value['field']]);
                    }
                }
               
                
            }
        }
    }
}

include('VMS.php'); 
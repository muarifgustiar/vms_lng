<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Izin_model extends MY_Model{
	public $table = 'ms_ijin_usaha';
	function __construct(){
		parent::__construct();

	}
	function getData($form, $id=null){
		if($id==null){
			$user = $this->session->userdata('user');
		}else{
			$user['id_user'] = $id;
		}
		$query = "	SELECT 	a.id_dpt_type,
							type, 
							no,
							issue_date,
							grade,
							
							qualification,
							authorize_by,
							expiry_date,
							izin_file, 
							
							a.id,
							a.data_status
					FROM ".$this->table." a
					WHERE a.del = 0 AND id_vendor = ".$user['id_user']."";
		if($this->input->post('filter')){
			$query .= $this->filter($form, $this->input->post('filter'), false);
		}
		
		return $query;
	}
	
	function selectData($id){
		$query = "SELECT 	id_dpt_type,
							type, 
							no,
							issue_date,
							grade,
							qualification,
							authorize_by,
							expiry_date,
							izin_file, 
							id,
							id_vendor
							 FROM ms_ijin_usaha WHERE id = ? AND del = 0";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function getVendorBidangSubBidang($ijin){
		$result = array();
    	$query = "SELECT * FROM ms_iu_bsb WHERE id_ijin_usaha = ?";
    	$query = $this->db->query($query, array($ijin));
    	foreach($query->result_array() as $key => $value){
			$result[$value['id_bidang']][$value['id_sub_bidang']] = 1;	
    	}
    	return $result;
	}
	function getBidangSubBidang($ijin){
    	$result = array();
    	$query = "SELECT * FROM tb_bidang WHERE id_dpt_type = ?";
    	$query = $this->db->query($query, array($ijin));
    	// echo print_r(expression)
    	foreach($query->result_array() as $key => $value){
    		$queryBSB = "SELECT * FROM tb_sub_bidang WHERE id_bidang = ?";
    		$queryBSB = $this->db->query($queryBSB, array($value['id']));
    		foreach($queryBSB->result_array() as $keyBSB=>$valueBSB){
    			$result[$value['id']]['label'] = $value['name'];
    			$result[$value['id']]['data'][$valueBSB['id']]['label'] = $valueBSB['name'];
    		}
    	}
    	return $result;
    }
	function insert_izin($data){
		$user = $this->session->userdata('user');
		$bsb = $data['bsb'];
		unset($data['bsb']);
		$data['id_vendor'] = $user['id_user'];
		$data['entry_stamp'] = timestamp();
		$this->insert($data);

		$id = $this->db->insert_id();
		foreach ($bsb as $key => $value) {
			foreach($value as $keybsb){
				
				$databsb['id_vendor'] = $data['id_vendor'];
				$databsb['id_ijin_usaha'] = $id;
				$databsb['id_bidang'] = $key;
				$databsb['id_sub_bidang'] = $keybsb;
				$databsb['entry_stamp'] = timestamp();
				$this->insert_bsb($databsb);
			}
		}
		return $id;
	}
	public function insert_bsb($databsb){
		$this->table = 'ms_iu_bsb';
		$this->insert($databsb);
	}
	public function getDPTType(){
		$query = "SELECT * FROM tb_dpt_type WHERE del = 0";
		$query = $this->db->query($query);
		$return = array();
		foreach ($query->result_array() as $key => $value) {
			$return[$value['id']] = $value['name'];
		}

		return $return;
	}

	public function getBSBList($id){

    	$result = array();
    	$bidang = $this->getBidangList();
    	$sub_bidang = $this->getSubBidangList();
    	$query = "SELECT * FROM ms_iu_bsb WHERE id_ijin_usaha = ?";
    	$query = $this->db->query($query, array($id));
    	foreach($query->result_array() as $key => $value){
    		$result[$bidang[$value['id_bidang']]][] = $sub_bidang[$value['id_sub_bidang']];
    	}
    	return $result;
    }
    public function getBidangList(){
    	$result = array();
    	$query = "SELECT * FROM tb_bidang WHERE del=0";
    	$query = $this->db->query($query);
    	foreach($query->result_array() as $key => $row){
    		$result[$row['id']] = $row['name'];
    	}
    	return $result;
    }

    public function getSubBidangList(){
    	$result = array();
    	$query = "SELECT * FROM tb_sub_bidang WHERE del=0";
    	$query = $this->db->query($query);
    	foreach($query->result_array() as $key => $row){
    		$result[$row['id']] = $row['name'];
    	}
    	return $result;
    }
    function delete($id){
    	$data = $this->selectData($id);

    	$this->db->where('status', 0)->where('id_dpt_type', $data['id_dpt_type'])->delete('tr_dpt');
    	
		return $this->db->where('id', $id)
					->update($this->table, array(
											'del' => 1,
											'edit_stamp' => timestamp()
											)
					);
	}
}
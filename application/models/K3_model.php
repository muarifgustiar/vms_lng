<?php defined('BASEPATH') OR exit('No direct script access allowed');

class K3_model extends MY_Model{
	public $table = 'ms_csms';
	function __construct(){
		parent::__construct();
	}
	function save_csms_data($post){

		$post['id_csms_limit'] = $this->get_k3_limit($post['score']);
		$res = $this->db->insert('ms_csms',$post);
		$this->update_score($post);
		return $res;
		
	}
	public function get_k3_limit($poin){
		$query = 	$this->db->get('tb_csms_limit')->result_array();

		foreach($query as $key => $row){
			if( ( 	$poin>=$row['end_score'] && $poin<=$row['start_score'])||
				(	$row['start_score']==NULL&&$poin>=$row['end_score']) || 
				(	$row['end_score']==NULL&&$poin<=$row['start_score']) 
			)

				return $row['id'];
		}
		return false;
	}
	public function update_score($post){

		// $num_rows = $this->db->where('id_vendor',$post['id_vendor'])->get('ms_score_k3')->num_rows();
		$this->table = 'ms_score_k3';
		$this->db->where('id_vendor', $post['id_vendor'])
					->update($this->table, array(
											'del' => 1,
											'edit_stamp' => timestamp()
											)
					);

			$res = $this->db->insert('ms_score_k3',array(
										'id_vendor'=>$post['id_vendor'],
										'data_status'=>$post['data_status'],
										'score'=>$post['score'],
										'entry_stamp'=>$post['entry_stamp'],
										'id_csms_limit'=>$post['id_csms_limit']
										)
									);

		
		return $res;
	}

	public function cek_csms($id=null){
		if($id==null){
			$user = $this->session->userdata('user');
		}else{
			$user['id_user'] = $id;
		}
		$query = "SELECT * FROM ms_csms WHERE id_vendor = ? AND del = 0";
		$query = $this->db->query($query, array($user['id_user']));
		return $query->num_rows();
	}

	public function getK3Data($id=null){
		if($id==null){
			$user = $this->session->userdata('user');
		}else{
			$user['id_user'] = $id;
		}
		$query = "	SELECT 
						csms_file,
						score,
						b.value,
						a.id
					FROM ms_csms a 
					LEFT JOIN tb_csms_limit b ON a.id_csms_limit = b.id 
					WHERE a.id_vendor = ? AND a.del = 0";
		$query = $this->db->query($query, array($user['id_user']));
		return $query->row_array();
	}
	function selectData($id){
		$query = "SELECT 
						csms_file,
						score,
						b.value,
						a.id
					FROM ms_csms a 
					LEFT JOIN tb_csms_limit b ON a.id_csms_limit = b.id 
					WHERE a.id = ? AND a.del = 0";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}

	function getK3Answer($id_vendor){
		$query = "SELECT * FROM tr_answer_hse WHERE id_vendor = ? AND del=0";

		$query = $this->db->query($query, array($id_vendor));
		$result = array();
		foreach ($query->result_array() as $key => $value) {
			$result[$value['id_answer']] = $value;
		}

		return $result;
	}

	function get_k3_all_data($id){
		$result = array();
		$getK3Data = $this->getK3Data($id);
		if(!empty($getK3Data)){
			$result['csms_file'] = $getK3Data;
		}
		$getK3Answer = $this->getK3Answer($id);
		if(!empty($getK3Answer)){
			$result['answer'] = $getK3Answer;
		}
		return $result;
	}
	function get_field_quest(){
		$query = "	SELECT *, a.id
					FROM tr_question_csms a 
					JOIN tb_quest b ON b.id=a.id_question
					WHERE a.del = 0
					AND b.del = 0";
		$query = $this->db->query($query);

		foreach($query->result_array() as $id => $quest){
			$result[$quest['id']] = $quest;
		}
		
		return $result;
	}
	function get_quest(){
		$res = $this->db->where('del',0)->get('tb_quest')->result_array();
		$result = array();

		foreach($res as $key =>$row){
			$quest = $this->db->select('*')->where('id_question',$row['id'])->get('tr_question_csms')->result_array();
			foreach($quest as $id => $quest){
				$result[$row['id_ms_header']][$row['id_sub_header']][$row['id']][$quest['id']] = $quest;
			}
		}


		return $result;
	}
	function get_evaluasi_list(){
		$result = array();
		$res = $this->db->where('del',0)->get('tb_evaluasi')->result_array();
		foreach($res as $key_sub => $value_sub){
			$result[$value_sub['id']] = $value_sub;
		}
		return $result;
	}
	function get_evaluasi(){
		$result = array();
		
		foreach($this->get_header_list() as $key_ms => $value_ms){

			$evaluasi = $this->db->where('id_ms_quest',$key_ms)->get('tb_evaluasi')->result_array();
			foreach($evaluasi as $key_ev => $data_ev){

				$quests = $this->db->where('id_evaluasi',$data_ev['id'])->where('del',0)->get('tb_quest')->result_array();
				// echo print_r($evaluasi);
				foreach($quests as $key_quest => $quest){

					$answer = $this->db->where('id_question',$quest['id'])->get('tr_question_csms')->result_array();
					$result[$quest['id_ms_header']][$quest['id_evaluasi']][$quest['id']] = $answer;
				}

			}
		}
		// echo print_r($result);
		return $result;
		
	}
	function get_question_data($id){
		$query = "SELECT * FROM tr_question_csms WHERE id = ?";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function get_header_list(){
		$query = "SELECT * FROM tb_ms_quest_k3 a WHERE a.del = 0";
		$query = $this->db->query($query);
		$result 	= array();
		foreach($query->result_array() as $key => $value){
			$result[$value['id']] = $value['question'];
		}
		return $result;
	}
	function get_sub_quest_list(){
		$query = "	SELECT 	a.id,
							a.id_header,
							a.question
					FROM 	tb_sub_quest_k3 a 
					WHERE 	del = 0";
		$query = $this->db->query($query);
		$result 	= array();
		foreach($query->result_array() as $keysq => $valuesq){
			$result[$valuesq['id_header']][$valuesq['id']] = $valuesq;
		}
		return $result;
	}
	function get_quest_list(){
		$query = "	SELECT 	id,
							id_ms_header,
							id_sub_header 
					FROM 	tb_quest 
					WHERE del = 0";

		$query = $this->db->query($query);
		$result = array();

		foreach($query->result_array() as $key =>$row){
			$result [$row['id']] = $row;
		}
		return $result;
	}
	public function get_csms($id_vendor){
		$res = array();
		$res = $this->db->select('ms_csms.*, tb_csms_limit.value')->where('id_vendor',$id_vendor)->where('ms_csms.del',0)->join('tb_csms_limit','tb_csms_limit.id=ms_csms.id_csms_limit','LEFT')->order_by('ms_csms.id','desc')->get('ms_csms')->row_array();
		return $res;

	}
	function get_data_field(){
		$query = "	SELECT 	*,
							a.id id 
					FROM 	tr_question_csms a
					JOIN 	tb_quest b ON b.id=a.id_question
					WHERE 	b.del = 0";
		$query = $this->db->query($query);
		foreach($query->result_array() as $id => $quest){
			$result[$quest['id']] = $quest;
		}		
		return $result;
	}
	function delete_answer_by_csms($id_csms){
		$this->db->where('id_csms', $id_csms)->update('tr_answer_hse', array('del'=>1));
	}
	function get_hse($id_vendor){
		return  $this->db->where('id_vendor',$id_vendor)->get('ms_hse')->row_array();

	}
	function get_penilaian_value($id_vendor,$id_csms=0){
		$get = $this->db->select('id_evaluasi,poin')->where('id_vendor',$id_vendor)->where('id_csms',$id_csms)->get('tr_evaluasi_poin')->result_array();
		$result = array();
		foreach ($get as $key => $value) {
			$result[$value['id_evaluasi']] = $value['poin'];
		}

		return $result;
	}
	function save_k3_data($post,$id_user){
		$this->db->where('id_vendor',$id_user)->update('tr_answer_hse',array('del'=>1));
		

		if($this->cek_csms()){
			$__dataCSMS = $this->getK3Data($id_user);
			$__post = array(
					'id_vendor'=>$id_user,
					'data_status'=>0,
					'edit_stamp'=>timestamp(),
				);
			$this->db->
			where('id_vendor', $id_user)->
			update('ms_csms', $__post);
			$id = $__dataCSMS['id'];

		}else{
			$__post = array(
					'id_vendor'=>$id_user,
					'data_status'=>0,
					'entry_stamp'=>timestamp(),
					'del'=>0
				);
			$this->db->insert('ms_csms', $__post);
			$id = $this->db->insert_id();
		}

		
		foreach($post as $id_question => $data){
			$__save = 	array(
							'id_answer'=>$id_question,
							'value'=>$data,
							'id_vendor'=>$id_user,
							'id_csms'=>$id,
							'entry_stamp'=>timestamp()
						);
			$res = $this->db->insert('tr_answer_hse',$__save);
			if(!$res){
				return false;
			}
		}
		return true;
	}
	function daftar_vendor_k3(){
		$query = "	SELECT d.name legal_name, a.name, c.score, a.id 
					FROM ms_vendor a
					LEFT JOIN ms_vendor_admistrasi b ON b.id_vendor=a.id
					JOIN ms_csms c ON c.id_vendor=a.id AND c.del = 0
					LEFT JOIN tb_legal d ON b.id_legal=d.id

					WHERE a.is_active = 1";
		return $query;
	}
	function save_evaluasi_poin($post, $id){
		$base_total = array();
		$evaluasiList = $this->get_evaluasi_list();
		foreach($post as $key => $row){
			
			$base_total[$evaluasiList[$key]['id_ms_quest']][] = $row;
			
			
		}
		$skor = 0;
		foreach ($base_total as $ms_quest => $value) {
			$total_row = count($value);
			$sum = array_sum($value);
			$skor += ($sum/$total_row);

		}
		$csms = $this->getK3Data($id);
		// $nr = $this->db->select('*')->where(array('id_vendor'=>$id))->get('ms_score_k3')->num_rows();
		$id_csms_limit = $this->get_k3_limit($skor);

		$this->db->where('id',$csms['id'])->update('ms_csms',array(
									// 'id_vendor'=>$id,
									'score'=>$skor,
									'edit_stamp'=>date('Y-m-d H:i:s'),
									'id_csms_limit'=>$id_csms_limit
									)
					);
		foreach($post as $key => $row){
			$res = $this->db->where('id_csms',$csms['id'])
							->where('id_evaluasi',$key)
							->where('id_vendor','$id')
							->update('tr_evaluasi_poin',array(
									'poin'=>$row,
									'edit_stamp'=>date('Y-m-d H:i:s')
									)
			);
			if(!$res){
				return false;
			}
		}
		
		
		$this->db->insert('ms_score_k3',array(
							'score'=>$skor,
							'data_status'=>0,
							'id_vendor'=>$id,
							'id_csms_limit'=>$id_csms_limit,
							'entry_stamp'=>date('Y-m-d H:i:s')
							));
			
		
		
		return $res;
	}
}

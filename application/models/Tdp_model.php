<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tdp_model extends MY_Model{
	public $table = 'ms_tdp';
	function __construct(){
		parent::__construct();

	}
	function getData($form, $id=null){
		if($id==null){
			$user = $this->session->userdata('user');
		}else{
			$user['id_user'] = $id;
		}
		$query = "	SELECT 	
							a.no,
							a.issue_date,
							a.expire_date,
							a.authorize_by,
							a.tdp_file,
							a.tdp_extension_file,
							
							a.id,
							a.data_status
					FROM ".$this->table." a
					WHERE a.del = 0 AND id_vendor = ".$user['id_user']."";
		if($this->input->post('filter')){
			$query .= $this->filter($form, $this->input->post('filter'), false);
		}
		
		return $query;
	}
	function save_data($data){
		$_param = array();
		$sql = "INSERT INTO ms_tdp (
							id_vendor,
							no,
							issue_date,
							expire_date,
							tdp_file,
							authorize_by,
							tdp_extension_file,
							entry_stamp,
							edit_stamp) 
				VALUES (?,?,?,?,?,?,?,?,?) ";
		
		
		foreach($this->field_master as $_param) $param[$_param] = $data[$_param];
		
		$this->db->query($sql, $param);
		$id = $this->db->insert_id();
		
		return $id;
	}

	function edit_data($data,$id){
				
		$this->db->where('id',$id);
		$result = $this->db->update('ms_tdp',$data);
		if($result)return $id;
	}
	function delete($id){
		$this->db->where('id',$id);
		
		return $this->db->update('ms_tdp',array('del'=>1));
	}
	function selectData($id){
		$query = "SELECT 	no,
							issue_date,
							expire_date,
							tdp_file,
							authorize_by,
							tdp_extension_file

						FROM ms_tdp WHERE id = ? AND del = 0";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
}
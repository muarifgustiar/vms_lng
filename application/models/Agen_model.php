<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Agen_model extends MY_Model{
	public $table = 'ms_agen';
	function __construct(){
		parent::__construct();

	}
	function getData($form, $id=null){
		if($id==null){
			$user = $this->session->userdata('user');
		}else{
			$user['id_user'] = $id;
		}
		$query = "	SELECT 	no,
							principal,
							issue_date,
							type,
							expiry_date,
							agen_file,
							a.id,
							a.data_status
					FROM ".$this->table." a
					WHERE a.del = 0 AND id_vendor = ".$user['id_user']."";
		if($this->input->post('filter')){
			$query .= $this->filter($form, $this->input->post('filter'), false);
		}
		
		return $query;

	}
	
	function selectData($id){
		$query = "SELECT 	no,
							principal,
							issue_date,
							type,
							expiry_date,
							agen_file
				FROM ms_agen WHERE id = ? AND del = 0";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	
}
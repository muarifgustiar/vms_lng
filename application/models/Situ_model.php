<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Situ_model extends MY_Model{
	public $table = 'ms_situ';
	function __construct(){
		parent::__construct();

	}
	function getData($form, $id=null){
		if($id==null){
			$user = $this->session->userdata('user');
		}else{
			$user['id_user'] = $id;
		}
		$query = "	SELECT 	a.type,
							a.no,
							a.address,
							a.issue_date,
							a.expire_date,
							a.situ_file,
							a.issue_by,
							a.situ_extension_file,
							a.id,
							a.data_status
					FROM ".$this->table." a
					WHERE a.del = 0 AND id_vendor = ".$user['id_user']."";
		if($this->input->post('filter')){
			$query .= $this->filter($form, $this->input->post('filter'), false);
		}
		
		return $query;
	}
	function save_data($data){
		$_param = array();
		$sql = "INSERT INTO ms_situ (
							id_vendor,
							type,
							no,
							address,
							issue_date,
							expire_date,
							situ_file,
							issue_by,
							situ_extension_file,
							entry_stamp,
							edit_stamp) 
				VALUES (?,?,?,?,?,?,?,?,?,?,?) ";
		
		
		foreach($this->field_master as $_param) $param[$_param] = $data[$_param];
		
		$this->db->query($sql, $param);
		$id = $this->db->insert_id();
		
		return $id;
	}

	function edit_data($data,$id){
				
		$this->db->where('id',$id);
		$result = $this->db->update('ms_situ',$data);
		if($result)return $id;
	}
	function delete($id){
		$this->db->where('id',$id);
		
		return $this->db->update('ms_situ',array('del'=>1));
	}
	function selectData($id){
		$query = "SELECT 	type,
							no,
							address,
							issue_date,
							expire_date,
							situ_file,
							issue_by,
							situ_extension_file

						FROM ms_situ WHERE id = ? AND del = 0";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}

}
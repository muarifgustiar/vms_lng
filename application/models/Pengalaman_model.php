<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pengalaman_model extends MY_Model{
	public $table = 'ms_pengalaman';
	function __construct(){
		parent::__construct();

	}
	function getData($form, $id=null){
		if($id==null){
			$user = $this->session->userdata('user');
		}else{
			$user['id_user'] = $id;
		}
		$query = "	SELECT 	
							job_name,
							job_location,
							job_giver,
							phone_no,
							contract_no,
							contract_start,
							contract_end,
							price_idr,
							currency,
							price_foreign,
							contract_file,
							a.id,
							a.data_status
					FROM ".$this->table." a 
					WHERE a.del = 0 AND a.id_vendor = ".$user['id_user']."";
		if($this->input->post('filter')){
			$query .= $this->filter($form, $this->input->post('filter'), false);
		}
		
		return $query;
	}
	
	function update($id, $data){
		$data['percentage'] = currency($data['percentage']);

		$this->db->where('id',$id);
		$result = $this->db->update('ms_pengalaman',$data);
		if($result)return $id;
	}
	function delete($id){
		$this->db->where('id',$id);
		return $this->db->update('ms_pengalaman',array('del'=>1));
	}
	function selectData($id){
		$query = "SELECT 	job_name,
							job_location,
							job_giver,
							phone_no,
							contract_no,
							contract_start,
							contract_end,
							price_idr,
							currency,
							price_foreign,
							contract_file,	
							id

						FROM ms_pengalaman WHERE id = ? AND del = 0";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
}
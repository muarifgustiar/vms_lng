<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pemilik_model extends MY_Model{
	public $table = 'ms_pemilik';
	function __construct(){
		parent::__construct();

	}
	function getData($form, $id=null){
		if($id==null){
			$user = $this->session->userdata('user');
		}else{
			$user['id_user'] = $id;
		}
		$query = "	SELECT 	
							b.no no_akta,
							a.name,
							a.shares,
							a.percentage,							
							a.id,
							a.data_status
					FROM ".$this->table." a 
					JOIN ms_akta b ON a.id_akta = b.id
					WHERE a.del = 0 AND a.id_vendor = ".$user['id_user']."";
		if($this->input->post('filter')){
			$query .= $this->filter($form, $this->input->post('filter'), false);
		}
		
		return $query;
	}
	
	function update($id, $data){
		$data['percentage'] = currency($data['percentage']);

		$this->db->where('id',$id);
		$result = $this->db->update('ms_pemilik',$data);
		if($result)return $id;
	}
	function delete($id){
		$this->db->where('id',$id);
		return $this->db->update('ms_pemilik',array('del'=>1));
	}
	function selectData($id){
		$query = "SELECT 	id_akta,
							name,
							shares,
							percentage

						FROM ms_pemilik WHERE id = ? AND del = 0";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
}
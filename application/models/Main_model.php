<?php
class Main_model extends CI_model{

	function check($data = array()){
		$username = $this->input->post('username');
		$_password = $this->input->post('password');
		$password = do_hash($this->input->post('password'),'sha1');

		$sql = "SELECT * FROM ms_login WHERE username = ? AND password = ?";
		$sql = $this->db->query($sql, array($username, $password));
		$sql_result = $sql->row_array();
		if($sql->num_rows() > 0){
			if($sql_result['type']=='admin'){
				$admin = "SELECT * FROM ms_admin WHERE id = ?";
				$admin = $this->db->query($admin, array($sql_result['id_user']));
				$admin = $admin->row_array();

				$set_session = array(
					'id_user' 		=> 	$admin['id'],
					'name'			=>	$admin['name'],
					'id_role'		=>	$admin['id_role']
				);
				$this->session->set_userdata('admin', $set_session);
				return true;
			}else{
				$admin = "SELECT * FROM ms_vendor WHERE id = ?";
				$admin = $this->db->query($admin, array($sql_result['id_user']));
				$admin = $admin->row_array();

				$set_session = array(
					'id_user' 		=> 	$admin['id'],
					'name'			=>	$admin['name']
				);
				$this->session->set_userdata('user', $set_session);
				return true;
			}
		}else{
			return false;
		}
	}

	function getKurs(){
		$return = array();
		$query = "SELECT * FROM tb_kurs WHERE del = 0";
		$query = $this->db->query($query);
		foreach ($query->result_array() as $key => $value) {
			$return[$value['id']] = $value['symbol'];
		}
		return $return;
	}
}
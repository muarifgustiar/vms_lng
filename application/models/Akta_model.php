<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Akta_model extends MY_Model{
	public $table = 'ms_akta';
	function __construct(){
		parent::__construct();

	}
	function getData($form, $id=null){
		if($id==null){
			$user = $this->session->userdata('user');
		}else{
			$user['id_user'] = $id;
		}
		
		$query = "	SELECT 	a.type,
							no,
							notaris,
							issue_date,
							akta_file,
							
							authorize_by,
							authorize_no,
							authorize_date,
							authorize_file,
							akta_extension_file,
							a.id,
							data_status
					FROM ".$this->table." a
					WHERE a.del = 0 AND id_vendor = ".$user['id_user']."";
		if($this->input->post('filter')){
			$query .= $this->filter($form, $this->input->post('filter'), false);
		}
		
		return $query;
	}
	function save_data($data){
		$_param = array();
		$sql = "INSERT INTO ms_akta (
							id_vendor,
							type,
							no,
							notaris,
							issue_date,
							akta_file,
							akta_extension_file,
							authorize_by,
							authorize_no,
							authorize_date,
							authorize_file,
							entry_stamp,
							edit_stamp) 
				VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) ";
		
		
		foreach($this->field_master as $_param) $param[$_param] = $data[$_param];
		
		$this->db->query($sql, $param);
		$id = $this->db->insert_id();
		
		return $id;
	}

	function edit_data($data,$id){
				
		$this->db->where('id',$id);
		

		$result = $this->db->update('ms_akta',$data);
		if($result)return $id;
	}
	function delete($id){
		$this->db->where('id',$id);
		
		return $this->db->update('ms_akta',array('del'=>1));
	}
	function selectData($id){
		$query = "SELECT type,
							no,
							notaris,
							issue_date,
							akta_file,
							akta_extension_file,
							authorize_by,
							authorize_no,
							authorize_date,
							authorize_file,
							data_status FROM ms_akta WHERE id = ? AND del = 0";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function getAktaOption($opt){
		$user = $this->session->userdata('user');
		$query = "	SELECT id, no 
					FROM ms_akta 
					WHERE id_vendor = ? AND type LIKE ?";
		$query = $this->db->query($query, array($user['id_user'], $opt));

		$return = array();
		foreach($query->result_array() as $key => $row){
			$return[$row['id']] = $row['no'];
		}
		return $return;
	}
}
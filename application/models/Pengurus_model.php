<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pengurus_model extends MY_Model{
	public $table = 'ms_pengurus';
	function __construct(){
		parent::__construct();

	}
	function getData($form, $id=null){
		if($id==null){
			$user = $this->session->userdata('user');
		}else{
			$user['id_user'] = $id;
		}
		$query = "	SELECT 	
							b.no no_akta,
							a.no,
							a.name,
							a.position,
							a.pengurus_file,
							a.expire_date,
							a.id,
							a.data_status
					FROM ".$this->table." a 
					JOIN ms_akta b ON a.id_akta = b.id
					WHERE a.del = 0 AND a.id_vendor = ".$user['id_user']."";
		if($this->input->post('filter')){
			$query .= $this->filter($form, $this->input->post('filter'), false);
		}
		
		return $query;
	}
	function save_data($data){
		$_param = array();
		$sql = "INSERT INTO ms_pengurus (
							id_vendor,
							id_akta,
							no,
							name,
							position,
							pengurus_file,
							expire_date,
							entry_stamp,
							edit_stamp) 
				VALUES (?,?,?,?,?,?,?,?,?) ";
		
		
		foreach($this->field_master as $_param) $param[$_param] = $data[$_param];
		
		$this->db->query($sql, $param);
		$id = $this->db->insert_id();
		
		return $id;
	}
	
	function edit_data($data,$id){
				
		$this->db->where('id',$id);
		$result = $this->db->update('ms_pengurus',$data);
		if($result)return $id;
	}
	function delete($id){
		$this->db->where('id',$id);
		
		return $this->db->update('ms_pengurus',array('del'=>1));
	}
	function selectData($id){
		$query = "SELECT 	id_akta,
							no,
							name,
							position,
							pengurus_file,
							expire_date

						FROM ms_pengurus WHERE id = ? AND del = 0";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
}
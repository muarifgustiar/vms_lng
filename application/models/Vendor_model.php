<?php
class Vendor_model extends MY_model{
	public $table = 'ms_vendor';
	public function daftar_vendor(){
		$query = "	SELECT 
						c.name badan_usaha, 
						a.name vendor_name,
						d.username,
						a.is_active,
						a.id
					FROM ms_vendor a 
					LEFT JOIN ms_vendor_admistrasi b ON a.id = b.id_vendor
					LEFT JOIN tb_legal c ON c.id = b.id_legal
					LEFT JOIN ms_login d ON d.id_user = a.id 
					WHERE 
						a.del = 0
					AND d.type = 'user'";
		return $query;
	}
	public function get_legal(){
		$query = "	SELECT 
						id, 
						name
					FROM tb_legal
					WHERE del = 0
				 ";
		$query = $this->db->query($query);
		foreach($query->result_array() as $key => $row){
			$return[$row['id']] = $row['name'];
		}
		return $return;
	}

	public function insert_vendor($data){
		$this->db->insert('ms_vendor',$data);
		return $this->db->insert_id();
	}

	public function insert_data_vendor($table, $data){
		return $this->db->insert($table,$data);
		
	}

	public function update($id,$data){
		$dataVendor['name']=$data['name'];
		$dataVendor['npwp_code']=$data['npwp_code'];
		$dataVendor['edit_stamp'] = timestamp();
		$return = $this->db->where('id', $id)->update('ms_vendor', $dataVendor);
		if($return){
			$dataAdministrasi['id_legal']=$data['id_legal'];
			$dataAdministrasi['npwp_code']=$data['npwp_code'];
			$dataAdministrasi['npwp_file']=$data['npwp_file'];
			$dataAdministrasi['nppkp_code']=$data['nppkp_code'];
			$dataAdministrasi['nppkp_file']=$data['nppkp_file'];
			$dataAdministrasi['vendor_office_status']=$data['vendor_office_status'];
			$dataAdministrasi['vendor_address']=$data['vendor_address'];
			$dataAdministrasi['vendor_country']=$data['vendor_country'];
			$dataAdministrasi['vendor_province']=$data['vendor_province'];
			$dataAdministrasi['vendor_city']=$data['vendor_city'];
			$dataAdministrasi['vendor_postal']=$data['vendor_postal'];
			$dataAdministrasi['vendor_fax']=$data['vendor_fax'];
			$dataAdministrasi['vendor_phone']=$data['vendor_phone'];
			$dataAdministrasi['vendor_website']=$data['vendor_website'];
			$dataAdministrasi['vendor_email']=$data['vendor_email'];

			$dataAdministrasi['edit_stamp'] = timestamp();
			$return = $this->db->where('id_vendor', $id)->update('ms_vendor_admistrasi', $dataAdministrasi);
			// echo $this->db->last_query();
		}
		
		return $return;
	}

	public function selectAdministrasiData($id=null){
		if($id!=null){
			$user['id_user'] = $id;
		}else{
			$user = $this->session->userdata('user');
		}
		
		$query = "	SELECT 
						c.name id_legal,
						a.name name,
						b.npwp_code,
						b.npwp_file,
						b.nppkp_code,
						b.nppkp_file,
						b.vendor_address,
						b.vendor_office_status,
						b.vendor_country,
						b.vendor_province,
						b.vendor_city,
						b.vendor_postal,
						b.vendor_fax,
						b.vendor_phone,
						b.vendor_email,
						b.vendor_website
					FROM ms_vendor a 
					LEFT JOIN ms_vendor_admistrasi b ON a.id = b.id_vendor 
					LEFT JOIN tb_legal c ON c.id = b.id_legal
					WHERE a.id = ?";
		$query = $this->db->query($query, array($user['id_user']));
		return $query->row_array();
	}

	public function selectData(){
		$user = $this->session->userdata('user');
		$query = "	SELECT 
						b.id_legal,
						a.name name,
						b.npwp_code,
						b.npwp_file,
						b.nppkp_code,
						b.nppkp_file,
						b.vendor_address,
						b.vendor_office_status,
						b.vendor_country,
						b.vendor_province,
						b.vendor_city,
						b.vendor_postal,
						b.vendor_fax,
						b.vendor_phone,
						b.vendor_email,
						b.vendor_website
					FROM ms_vendor a 
					LEFT JOIN ms_vendor_admistrasi b ON a.id = b.id_vendor 
					WHERE a.id = ?";
		$query = $this->db->query($query, array($user['id_user']));
		return $query->row_array();
	}

	function get_waiting_list(){

		$query = "	SELECT 	c.name legal_name,
							a.name name,
							a.id as id 
					FROM ms_vendor a
					LEFT JOIN ms_vendor_admistrasi b ON b.id_vendor = a.id
					LEFT JOIN tb_legal c ON c.id = b.id_legal
					WHERE vendor_status = 1";
		return $query;
	}


	public function get_data($id=0){
		if(!$id){
			$user = $this->session->userdata('user');
			$id = $user['id_user'];
		}
		
		$this->db->select('tr_dpt.start_date dpt_date, ms_vendor.certificate_no, ms_vendor.dpt_first_date as first_date, ms_vendor.id id_vendor, mva.id id,id_sbu,id_legal, ms_vendor.name as name, ms_vendor.npwp_code as npwp_code, npwp_file, npwp_date,nppkp_code, nppkp_file, nppkp_date, vendor_office_status, vendor_address, vendor_country, vendor_phone, vendor_province, vendor_fax, vendor_city, vendor_email, vendor_postal, vendor_website, tb_sbu.name as sbu_name, tb_legal.name as legal_name,data_status')
		->where('ms_vendor.id',$id)
		->join('ms_vendor_admistrasi as mva','mva.id_vendor=ms_vendor.id','LEFT')
		->join('tr_dpt','tr_dpt.id_vendor=ms_vendor.id','LEFT')
		->join('tb_sbu','tb_sbu.id=ms_vendor.id_sbu','LEFT')
		->join('tb_legal','tb_legal.id=mva.id_legal','LEFT')/*
		->join('ms_score_k3','ms_score_k3.id_vendor=ms_vendor.id','LEFT')*/
		->order_by('tr_dpt.id', 'DESC')
		->limit('1');
		$query = $this->db->get('ms_vendor');
		return $query->row_array();
	}
	public function get_pt(){
		$this->db->select('tb_legal.name legal_name,ms_vendor.name name, ms_vendor_admistrasi.vendor_email')
		->join('ms_vendor_admistrasi','ms_vendor_admistrasi.id_vendor = ms_vendor.id')
		->join('tb_legal','tb_legal.id=ms_vendor_admistrasi.id_legal')
		->where('vendor_status',1);
		$query = $this->db->get('ms_vendor');
		return $query->row_array();
	}
	function get_vendor_name($id){
		return $this->db->where('id',$id)->get('ms_vendor')->row_array();
	}

	public function get_dpt_list(){
	
		$query ="	SELECT 
						j.name legal_name,
						a.name,
						f.value category_name,
						b.npwp_code,
						b.nppkp_code,
						i.score,
						a.id id
					FROM ms_vendor a 
					LEFT JOIN ms_vendor_admistrasi b ON a.id = b.id_vendor
					LEFT JOIN ms_situ c ON c.id_vendor = a.id
					LEFT JOIN ms_ijin_usaha d ON d.id_vendor = a.id
					LEFT JOIN tr_assessment_point e ON e.id_vendor = a.id
					LEFT JOIN tb_blacklist_limit f ON f.id = e.category
					LEFT JOIN ms_pengalaman g ON g.id_vendor = a.id
					LEFT JOIN ms_agen h ON h.id_vendor = a.id
					LEFT JOIN ms_csms i ON i.id_vendor = a.id
					LEFT JOIN tb_legal j ON j.id = b.id_legal
					WHERE a.vendor_status = 2 
					AND a.is_active = 1
				";

		$query .="GROUP BY a.id";
		return $query;
	}
}
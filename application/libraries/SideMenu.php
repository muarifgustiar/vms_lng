<?php
class SideMenu{
	private $CI;

	private $menuList;

	public function __construct($menuList = array()){
		$this->menuList = $menuList;

		return $this->generate();
	}

	public function generate($active=''){

		$html = '<ul class="sidebar-wrapper">';

		foreach ($this->menuList as $keyGroup => $valueGroup) {
			$icon = (isset($valueGroup['icon'])) ? '<i class="fa fa-'.$valueGroup['icon'].' icon"></i>' : '';
			if(isset($valueGroup['title'])){
				$arrow = (isset($valueGroup['list'])) ? '<span class="pull-right">
								<i class="fa fa-angle-down text"></i>
								
							</span>' : '';

				$html .= '<li class="sidebar-list">
							<a href="'.(($valueGroup['url']!='')?$valueGroup['url']:'').'" class="sidebar-heading">
								'.$icon.'
								'.$arrow.'
								<span>'.$valueGroup['title'].'</span>
							</a>';
			}	
			if(isset($valueGroup['list'])){
				$html .= '<ul class="sidebar-menu">';
				foreach ($valueGroup['list'] as $key => $value) {
					$html .= '	<li class="sidebar-menu-item">
									<a href="'.$value['url'].'" class="sidebar-menu-button">

										<i class="fa fa-angle-right"></i>
										<span>'.$value['title'].'</span>
									</a>
								</li>
							';
				}
				$html .= '</ul>';
			}
			$html .='</li>';
		}
		$html .= '</ul>';
		return $html;
	}

}